package org.pixelgalaxy.mysql;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.pixelgalaxy.player_objects.PlayerLevel;

import java.sql.*;
import java.util.logging.Level;

public class MysqlConnection {
    private String insertPlayerXp = "INSERT INTO player_xp (uuid, total_xp)  VALUES (?, ?)";
    private String insertPlayerSkills = "INSERT INTO player_skills (uuid, strength, speed, crit, archery, hearts, defense, tospend) VALUES (?, 1, 1, 1, 1, 1, 1, 0)";
    private String insertPlayerEquipment = "INSERT INTO player_equipment (uuid, combat_boots, combat_leggings, combat_chestplate, combat_helmet, ring1, ring2, necklace, scroll, earring1, earring2, cosmetic_boots, cosmetic_leggings, cosmetic_chestplate, cosmetic_helmet) " +
            "VALUES (?, '', '', '', '', '', '', '', '', '', '', '', '', '', '')";
    private String insertPlayerBounties = "INSERT INTO player_bounties (uuid, bounty_types, bounty_progress) VALUES (?, '', '')";
    private String updatePlayerEquipment = "UPDATE player_equipment SET ? = ? WHERE uuid=?";

    public Connection getConnection(String host, String db, String user, String pw) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://" + host + ":3306/" + db, user, pw);
            return con;
        } catch (SQLException var2) {
            Bukkit.getLogger().log(Level.SEVERE, "Could not connect to MySQL server, error code: " + var2.getErrorCode());
        } catch (ClassNotFoundException var3) {
            Bukkit.getLogger().log(Level.SEVERE, "JDBC driver was not found in this machine.");
        }
        return null;
    }

    public void insertPlayerXp(Connection conn, Player player) {
        try (PreparedStatement insertStmt = conn.prepareStatement(insertPlayerXp)) {
            insertStmt.setString(1, player.getUniqueId().toString());
            insertStmt.setInt(2, PlayerLevel.getXpOfLevel(1));
            insertStmt.execute();
        } catch (SQLException e) {
            Bukkit.getLogger().log(Level.SEVERE, e.getMessage());
        }
    }

    public void insertPlayerSkills(Connection conn, Player player) {
        insertPlayerInfo(conn, player, insertPlayerSkills);
    }

    public void insertPlayerEquipment(Connection conn, Player player) {
        insertPlayerInfo(conn, player, insertPlayerEquipment);
    }

    public void insertPlayerBounties(Connection conn, Player player) {
        insertPlayerInfo(conn, player, insertPlayerBounties);
    }

    private void insertPlayerInfo(Connection conn, Player player, String sqlInsert) {
        try (PreparedStatement insertStmt = conn.prepareStatement(sqlInsert)) {
            insertStmt.setString(1, player.getUniqueId().toString());
            insertStmt.execute();
        } catch (SQLException e) {
            Bukkit.getLogger().log(Level.SEVERE, e.getMessage());
        }
    }

    public void updatePlayerEquipment(Connection conn, Player player, String equipment, String value) {
        try (PreparedStatement insertStmt = conn.prepareStatement(updatePlayerEquipment)) {
            insertStmt.setString(1, equipment);
            insertStmt.setString(2, value);
            insertStmt.setString(3, player.getUniqueId().toString());
            insertStmt.executeUpdate();
        } catch (SQLException e) {
            Bukkit.getLogger().log(Level.SEVERE, e.getMessage());
        }
    }

    public void updateQuery(Connection conn, String updateQuery) {
        try (Statement updateStmt = conn.createStatement()) {
            updateStmt.executeUpdate(updateQuery);
        } catch (SQLException e) {
            Bukkit.getLogger().log(Level.SEVERE, e.getMessage());
        }
    }
}

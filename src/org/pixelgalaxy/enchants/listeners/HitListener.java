package org.pixelgalaxy.enchants.listeners;

import org.apache.commons.lang.StringUtils;
import org.bukkit.*;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.pixelgalaxy.RPG;
import org.pixelgalaxy.RPGPlayer;
import org.pixelgalaxy.enchants.Enchant;
import org.pixelgalaxy.enchants.classes.WolfPack;
import org.pixelgalaxy.enchants.timers.BleedTimer;
import org.pixelgalaxy.enchants.types.EnchantType;
import org.pixelgalaxy.equipment.CustomEquipment;
import org.pixelgalaxy.equipment.Equipment;
import org.pixelgalaxy.player_objects.PlayerEquipment;
import org.pixelgalaxy.utils.Chance;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by robin on 19/18/2118.
 */
public class HitListener implements Listener {

    static final int SECOND = 21;
    private static Map<Player, Double> heavyhand_damage = new HashMap<>();

    public boolean hasAvaliableSlot(Player player) {
        Inventory inv = player.getInventory();
        for (ItemStack item : inv.getContents()) {
            if (item == null) {
                return true;
            }
        }
        return false;
    }


    @EventHandler(priority = EventPriority.HIGHEST)
    public void onhit(EntityDamageByEntityEvent e) {

        if (e.getDamager() instanceof Player && e.getEntity() instanceof LivingEntity) {

            Player p = (Player) e.getDamager();
            LivingEntity en = (LivingEntity) e.getEntity();
            ItemStack inhand = p.getInventory().getItemInMainHand();

            boolean pvpAllowed = true;

            if (en instanceof Player) {
                Player target = (Player) en;
                if (!RPG.isPvPAllowed(p) || !RPG.isPvPAllowed(target)) {
                    pvpAllowed = false;
                }

            }

            if (pvpAllowed) {

                if (inhand != null) {

                    if (Equipment.isMelee(inhand)) {

                        for (String loreLine : inhand.getItemMeta().getLore()) {

                            for (Enchant m : Enchant.values()) {

                                if (loreLine.toLowerCase().contains(m.getName().toLowerCase()) && m.getEnchantType().equals(EnchantType.MELEE)) {

                                    int level = Integer.valueOf(loreLine.split("\\s+")[StringUtils.countMatches(loreLine, " ")]);

                                    Location enLocation = en.getLocation();
                                    World w = Bukkit.getWorld(p.getWorld().getName());

                                    switch (m.toString().toUpperCase()) {

                                        case "FINAL_BLOW":

                                            if ((en.getHealth() / en.getMaxHealth()) <= 0.15) {
                                                if (Chance.chance(m.getChance() * level)) {

                                                    en.damage(e.getDamage());

                                                }
                                            }

                                            break;

                                        case "CONFUSION":

                                            if (Chance.chance(m.getChance() * level)) {
                                                en.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 5 * SECOND, 1, true, true));
                                            }

                                            break;

                                        case "SLOW_MELEE":

                                            if (Chance.chance(m.getChance() * level)) {
                                                en.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 5 * SECOND, 1, true, true));
                                            }

                                            break;

                                        case "BLIND":

                                            if (Chance.chance(m.getChance() * level)) {
                                                en.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 5 * SECOND, 1, true, true));
                                            }

                                            break;

                                        case "WITHER":

                                            if (Chance.chance(m.getChance() * level)) {
                                                en.addPotionEffect(new PotionEffect(PotionEffectType.WITHER, 10 * SECOND, 1, true, true));
                                            }

                                            break;

                                        case "POISON":

                                            if (Chance.chance(m.getChance() * level)) {
                                                en.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 5 * SECOND, 1, true, true));
                                            }

                                            break;

                                        case "LIGHTNING":

                                            if (Chance.chance(m.getChance() * level)) {
                                                w.strikeLightning(enLocation);
                                            }

                                            break;

                                        case "SPECTRAL":

                                            if (Chance.chance(m.getChance() * level)) {
                                                en.addPotionEffect(new PotionEffect(PotionEffectType.GLOWING, 5 * SECOND, 1, true, true));
                                            }

                                            break;

                                        case "LEVITATION":

                                            if (Chance.chance(m.getChance() * level)) {
                                                en.addPotionEffect(new PotionEffect(PotionEffectType.LEVITATION, (int) (m.getDuration() * SECOND), 1, true, true));
                                            }

                                            break;

                                        case "BERSERK":

                                            if (Chance.chance(m.getChance() * level)) {
                                                p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 5 * SECOND, 3, true, true));
                                                p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 5 * SECOND, 3, true, true));
                                            }

                                            break;

                                        case "CYNICAL":

                                            if (Chance.chance(m.getChance() * level)) {
                                                en.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 5 * SECOND, 3, true, true));
                                            }

                                            break;

                                        case "BLAST":

                                            if (Chance.chance(m.getChance() * level)) {

                                                if (p.getWorld().getName().equalsIgnoreCase("warzone")) {

                                                    w.createExplosion(enLocation, 2.0F);

                                                }

                                            }
                                            break;

                                        case "SNARE":

                                            if (Chance.chance(m.getChance() * level)) {

                                                en.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 3 * SECOND, 10, true, true));

                                            }
                                            break;

                                        case "LIFE_STEAL":

                                            p.setHealth(p.getHealth() + (e.getDamage() * (level * m.getIncrease())));

                                            break;


                                        case "BLEED":

                                            if (Chance.chance(m.getChance() * level)) {

                                                BleedTimer bleedTimer = new BleedTimer(en, m, level);
                                                bleedTimer.runTaskTimer(RPG.getPlugin(), 0, 10);

                                            }

                                            break;

                                        case "HEAVY_HAND":

                                            if (Chance.chance(m.getChance() * level)) {

                                                double current = 1.5;
                                                if (heavyhand_damage.containsKey(p)) {
                                                    current = heavyhand_damage.get(p);
                                                    current *= 1.5;
                                                    heavyhand_damage.remove(p);
                                                    heavyhand_damage.put(p, current);
                                                } else {
                                                    heavyhand_damage.put(p, current);
                                                }
                                                en.damage(Math.pow(e.getDamage(), current) - e.getDamage());

                                            } else {

                                                if (heavyhand_damage.containsKey(p)) {
                                                    if (Chance.chance(50)) {
                                                        heavyhand_damage.remove(p);
                                                    }
                                                }
                                            }

                                            break;

                                        case "DISARMOR":

                                            if (en instanceof Player) {

                                                Player target = (Player) en;

                                                if (Chance.chance(m.getChance() * level)) {

                                                    Random r = new Random();
                                                    int random = r.nextInt(3);

                                                    ItemStack is = target.getEquipment().getArmorContents()[random];

                                                    if (hasAvaliableSlot(target)) {
                                                        target.getInventory().addItem(is);
                                                    } else {
                                                        w.dropItem(enLocation, is);
                                                    }

                                                    PlayerEquipment pe = RPGPlayer.get(target).getPeq();

                                                    ItemStack newArmor = new ItemStack(Material.PAPER, 1);
                                                    ItemMeta newArmorM = newArmor.getItemMeta();

                                                    switch (random) {
                                                        case 0:
                                                            target.getEquipment().setBoots(null);

                                                            newArmorM.setDisplayName("§c" + CustomEquipment.slotWithItem.get(37).replaceAll("_", " ") + " slot");
                                                            newArmorM.setLore(Arrays.asList(" "));
                                                            newArmor.setItemMeta(newArmorM);
                                                            pe.setCombat_boots(newArmor);
                                                            break;

                                                        case 1:
                                                            target.getEquipment().setLeggings(null);
                                                            newArmorM.setDisplayName("§c" + CustomEquipment.slotWithItem.get(28).replaceAll("_", " ") + " slot");
                                                            newArmorM.setLore(Arrays.asList(" "));
                                                            newArmor.setItemMeta(newArmorM);
                                                            pe.setCombat_leggings(newArmor);
                                                            break;

                                                        case 2:
                                                            target.getEquipment().setChestplate(null);
                                                            newArmorM.setDisplayName("§c" + CustomEquipment.slotWithItem.get(19).replaceAll("_", " ") + " slot");
                                                            newArmorM.setLore(Arrays.asList(" "));
                                                            newArmor.setItemMeta(newArmorM);
                                                            pe.setCombat_chestplate(newArmor);
                                                            break;

                                                        case 3:
                                                            target.getEquipment().setHelmet(null);
                                                            newArmorM.setDisplayName("§c" + CustomEquipment.slotWithItem.get(10).replaceAll("_", " ") + " slot");
                                                            newArmorM.setLore(Arrays.asList(" "));
                                                            newArmor.setItemMeta(newArmorM);
                                                            pe.setCombat_helmet(newArmor);
                                                            break;

                                                    }

                                                    CustomEquipment.updatePlayerArmor(target);

                                                }

                                            }

                                            break;

                                        case "WOLF_PACK":

                                            if (Chance.chance(m.getChance() * level)) {

                                                WolfPack.summonWolf(p);

                                            }

                                            break;

                                        case "PICKPOCKET":

                                            if (Chance.chance(m.getChance() * level)) {

                                                if (en instanceof Player) {
                                                    Player target = (Player) en;
                                                    double amount = m.getIncrease() * level * RPG.getEco().getBalance(target);
                                                    RPG.getEco().withdrawPlayer(target, amount);
                                                    RPG.getEco().depositPlayer(p, amount);
                                                    target.sendMessage("§c$" + amount + " has been stolen from you!");
                                                    p.sendMessage("§aYou pickpocketed $" + amount + "!");

                                                }
                                            }

                                            break;

                                        case "GREATNESS":

                                            if (Chance.chance(m.getChance() * level)) {

                                                for (int i = 0; i < 2; i++) {

                                                    Firework fw2 = (Firework) p.getWorld().spawnEntity(en.getLocation(), EntityType.FIREWORK);
                                                    FireworkMeta fwm = fw2.getFireworkMeta();

                                                    fwm.setPower(0);
                                                    fwm.addEffect(FireworkEffect.builder().withColor(Color.GREEN, Color.BLUE, Color.RED, Color.BLACK).flicker(true).build());

                                                    fw2.setFireworkMeta(fwm);
                                                    fw2.detonate();
                                                }

                                            }

                                            break;

                                    }

                                }

                            }
                        }
                    }
                }
            }

        }

    }

}

package org.pixelgalaxy.enchants.classes;

import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wolf;
import org.pixelgalaxy.RPG;
import org.pixelgalaxy.enchants.timers.WolvesKillTimer;

import java.util.*;

/**
 * Created by robin on 19/08/2018.
 */
public class WolfPack {

    public static Map<Player, List<Wolf>> wolfList = new HashMap<>();

    public static void summonWolf(Player p) {
        // Spawn anywhere between 1 and 4 wolves.
        Random rand = new Random();
        int amount = rand.nextInt(4) + 1;

        List<Wolf> wolves = new ArrayList<>();

        for (int i = 0; i < amount; i++) {
            wolves.add(spawnWolf(p));
        }

        wolfList.put(p, wolves);
        WolvesKillTimer wolvesKillTimer = new WolvesKillTimer(p);
        wolvesKillTimer.runTaskLater(RPG.getPlugin(), 200);

    }

    public static Wolf spawnWolf(Player p) {
        Wolf wolf = (Wolf) p.getWorld().spawnEntity(p.getLocation(), EntityType.WOLF);

        // Just to make sure it's a normal wolf.
        wolf.setAdult();
        wolf.setTamed(true);
        wolf.setOwner(p);

        // We don't want extra wolves.
        wolf.setBreed(false);

        // Clarify the owner.
        wolf.setCustomName(ChatColor.GREEN + p.getName() + "'s Wolf");
        wolf.setCustomNameVisible(true);

        // Let's have a little bit of variation
        wolf.setCollarColor(DyeColor.RED);

        // Misc.
        wolf.setHealth(wolf.getMaxHealth());
        wolf.setCanPickupItems(false);

        return wolf;
    }

}

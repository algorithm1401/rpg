package org.pixelgalaxy.enchants.types;

/**
 * Created by robin on 3/09/2018.
 */
public enum EnchantType {

    ARMOR("Armor."),
    MELEE("Melee."),
    BOW("Bow.");

    private String configName;

    EnchantType(String configName) {
        this.configName = configName;
    }

    public String getConfigName() {
        return configName;
    }
}

package org.pixelgalaxy.enchants;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.pixelgalaxy.RPG;
import org.pixelgalaxy.enchants.types.EnchantType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by robin on 11/09/2018.
 */
public class InventoryList implements Listener {

    public static ItemStack getWikiStack() {

        ItemStack wiki = new ItemStack(Material.BOOK, 1);
        ItemMeta wikim = wiki.getItemMeta();
        wikim.setDisplayName("§9§lWiki");
        wikim.setLore(Arrays.asList("§6You can view all our",
                "§6enchantments and exact",
                "§6values on our wiki.",
                "§6CLICK ME!"));
        wiki.setItemMeta(wikim);
        return wiki;

    }

    public static ItemStack getMeleeStack() {

        ItemStack melee = new ItemStack(Material.DIAMOND_SWORD, 1);
        ItemMeta meleem = melee.getItemMeta();
        meleem.setDisplayName("§a§lMelee Enchantments");
        meleem.setLore(Arrays.asList("§7Click this to view all", "§7Melee enchantments!"));
        meleem.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meleem.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        meleem.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        meleem.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        melee.setItemMeta(meleem);

        return melee;
    }

    public static ItemStack getBowStack() {

        ItemStack bow = new ItemStack(Material.BOW, 1);
        ItemMeta bowm = bow.getItemMeta();
        bowm.setDisplayName("§a§lBow Enchantments");
        bowm.setLore(Arrays.asList("§7Click this to view all", "§7Bow enchantments!"));
        bowm.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        bowm.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        bowm.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        bowm.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        bow.setItemMeta(bowm);

        return bow;
    }

    public static ItemStack getArmorStack() {

        ItemStack armor = new ItemStack(Material.DIAMOND_CHESTPLATE, 1);
        ItemMeta armorm = armor.getItemMeta();
        armorm.setDisplayName("§a§lArmor Enchantments");
        armorm.setLore(Arrays.asList("§7Click this to view all", "§7Armor enchantments!"));
        armor.setItemMeta(armorm);

        return armor;
    }

    public static ItemStack getCosmeticStack() {

        ItemStack cosmetic = new ItemStack(Material.MAP, 1);
        ItemMeta cosmeticm = cosmetic.getItemMeta();
        cosmeticm.setDisplayName("§a§lCosmetic Enchantments");
        cosmeticm.setLore(Arrays.asList("§7Click this to view all", "§7Cosmetic enchantments!"));
        cosmetic.setItemMeta(cosmeticm);

        return cosmetic;
    }

    public static ItemStack getEnchantStack(Enchant enchant) {

        ItemStack book = new ItemStack(Material.ENCHANTED_BOOK, 1);
        ItemMeta bookm = book.getItemMeta();
        bookm.setDisplayName("§b§l" + enchant.getName());
        List<String> lore = new ArrayList<>();

        lore.add("§eDescription: §7" + ChatColor.translateAlternateColorCodes('&', enchant.getDescription().get(0)));

        for (int i = 1; i < enchant.getDescription().size(); i++) {
            lore.add("§7" + ChatColor.translateAlternateColorCodes('&', enchant.getDescription().get(i)));
        }

        bookm.setLore(lore);

        book.setItemMeta(bookm);

        return book;
    }

    public static void openEnchantTypes(Player p) {

        Inventory inv = Bukkit.createInventory(null, InventoryType.HOPPER, "§5§lCustom Enchantments");

        inv.setItem(0, getMeleeStack());
        inv.setItem(1, getBowStack());
        inv.setItem(2, getWikiStack());
        inv.setItem(3, getArmorStack());
        inv.setItem(4, getCosmeticStack());

        p.openInventory(inv);

    }

    public static void openEnchantList(EnchantType enchantType, Player p) {

        p.closeInventory();

        Inventory inv = Bukkit.createInventory(null, 54, "§9§l" + enchantType.getConfigName().replace(".", "") + " Enchantments");

        int slot = 0;
        for (Enchant enchant : Enchant.values()) {

            if (enchant.getEnchantType().equals(enchantType)) {

                inv.setItem(slot, getEnchantStack(enchant));

                slot++;
            }

        }

        p.openInventory(inv);

    }

    @EventHandler
    public void invClick(InventoryClickEvent e) {

        if (e.getClickedInventory().getTitle() != null) {

            if (e.getClickedInventory().getTitle().equalsIgnoreCase("§5§lCustom Enchantments")) {

                Player p = (Player) e.getWhoClicked();

                switch (e.getCurrentItem().getType()) {

                    case DIAMOND_SWORD:

                        openEnchantList(EnchantType.MELEE, p);

                        break;

                    case BOW:

                        openEnchantList(EnchantType.BOW, p);

                        break;

                    case BOOK:

                        p.sendMessage(RPG.getPrefix() + "§cWiki coming soon...");

                        break;

                    case DIAMOND_CHESTPLATE:

                        openEnchantList(EnchantType.ARMOR, p);

                        break;

                    case MAP:

                        p.sendMessage(RPG.getPrefix() + "§cAccessoires will come with a future update!");

                        break;

                }

                e.setCancelled(true);

            } else if (e.getClickedInventory().getTitle().contains("Enchantments")) {
                e.setCancelled(true);
            }
        }
    }

}

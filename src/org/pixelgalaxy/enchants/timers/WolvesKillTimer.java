package org.pixelgalaxy.enchants.timers;

import org.bukkit.entity.Player;
import org.bukkit.entity.Wolf;
import org.bukkit.scheduler.BukkitRunnable;
import org.pixelgalaxy.enchants.classes.WolfPack;

/**
 * Created by robin on 19/08/2018.
 */
public class WolvesKillTimer extends BukkitRunnable {

    Player p;

    public WolvesKillTimer(Player p) {
        this.p = p;
    }

    @Override
    public void run() {

        for(Wolf w : WolfPack.wolfList.get(p)){
            w.remove();
        }

    }
}

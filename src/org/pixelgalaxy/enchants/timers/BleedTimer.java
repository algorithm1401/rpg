package org.pixelgalaxy.enchants.timers;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Sound;
import org.bukkit.entity.LivingEntity;
import org.bukkit.scheduler.BukkitRunnable;
import org.pixelgalaxy.enchants.Enchant;

/**
 * Created by robin on 19/08/2018.
 */
public class BleedTimer extends BukkitRunnable {

    LivingEntity en;
    Enchant m;
    int level;

    int count = 0;

    public BleedTimer(LivingEntity en, Enchant m, int level) {
        this.en = en;
        this.m = m;
        this.level = level;
    }

    @Override
    public void run() {

        if (count < 10) {
            double toRemove = m.getIncrease() * level;
            if ((en.getHealth() - toRemove) <= 0) {
                en.setHealth(0);
                this.cancel();
            } else {
                en.setHealth(en.getHealth() - (toRemove));
            }
            en.getServer().getWorld(en.getWorld().getName()).playSound(en.getLocation(), Sound.ENTITY_PLAYER_ATTACK_STRONG, 1.0F, 1.0F);
            en.getServer().getWorld(en.getWorld().getName()).playEffect(en.getLocation(), Effect.COLOURED_DUST, 1);
            count++;
        } else {
            this.cancel();
        }
    }
}

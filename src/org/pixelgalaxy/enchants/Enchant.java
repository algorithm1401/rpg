package org.pixelgalaxy.enchants;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.pixelgalaxy.RPG;
import org.pixelgalaxy.enchants.types.EnchantType;
import org.pixelgalaxy.equipment.Equipment;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by robin on 19/08/2018.
 */
public enum Enchant {

    /*/
    Internal errors:
    x_bow
    shulkers_bane
    eternal_chestplate
    eternal_leggings

     */

    PROTECTION(EnchantType.ARMOR),
    KADABRA(EnchantType.ARMOR),
    IGNITE(EnchantType.ARMOR),
    VALOR(EnchantType.ARMOR),
    REINFORCED(EnchantType.ARMOR),
    BANE(EnchantType.ARMOR),
    CACTUS(EnchantType.ARMOR),
    ENLIGHTENED(EnchantType.ARMOR),
    LAST_STAND(EnchantType.ARMOR),
    NUTRITION(EnchantType.ARMOR),
    HARDEN(EnchantType.ARMOR),
    SNARE_ARMOR(EnchantType.ARMOR),
    SLOW_ARMOR(EnchantType.ARMOR),
    SELF_DESTRUCT(EnchantType.ARMOR),
    BLAST_OFF(EnchantType.ARMOR),
    ESCAPE(EnchantType.ARMOR),
    SAVIOR(EnchantType.ARMOR),
    SMOKE_SCREEN(EnchantType.ARMOR),
    TANK(EnchantType.ARMOR),
    WITHERING(EnchantType.ARMOR),
    ABSORBING(EnchantType.ARMOR),
    SATURATION(EnchantType.ARMOR),
    HASTENED(EnchantType.ARMOR),
    GHOSTLY(EnchantType.ARMOR),
    SNOWSTORM(EnchantType.ARMOR),
    LIGHTWEIGHT(EnchantType.ARMOR),

    BLIND_BOW(EnchantType.BOW),
    WEAKNESS(EnchantType.BOW),
    POISON_BOW(EnchantType.BOW),
    LEVITATION_BOW(EnchantType.BOW),
    PIERCE(EnchantType.BOW),
    LIGHTNING_BOW(EnchantType.BOW),
    SNARE_BOW(EnchantType.BOW),
    LIFE_STEAL_BOW(EnchantType.BOW),
    DISARM(EnchantType.BOW),
    SLOW_BOW(EnchantType.BOW),
    BLEED_BOW(EnchantType.BOW),
    HEAD_HUNTER_BOW(EnchantType.BOW),
    PARALYZE_BOW(EnchantType.BOW),
    BLAST_BOW(EnchantType.BOW),

    BLAST(EnchantType.MELEE),
    FEATHERWEIGHT(EnchantType.MELEE),
    SLOW_MELEE(EnchantType.MELEE),
    PICKPOCKET(EnchantType.MELEE),
    SNARE(EnchantType.MELEE),
    LIFE_STEAL(EnchantType.MELEE),
    BLEED(EnchantType.MELEE),
    CONFUSION(EnchantType.MELEE),
    HEAVY_HAND(EnchantType.MELEE),
    DISARMOR(EnchantType.MELEE),
    WOLF_PACK(EnchantType.MELEE),
    FIRE_ASPECT(EnchantType.MELEE),
    XP_BOOST(EnchantType.MELEE),
    BLIND(EnchantType.MELEE),
    WITHER(EnchantType.MELEE),
    POISON(EnchantType.MELEE),
    LIGHTNING(EnchantType.MELEE),
    SPECTRAL(EnchantType.MELEE),
    FACTION_HEAL(EnchantType.MELEE),
    DRAGONS_BREATH(EnchantType.MELEE),
    LEVITATION(EnchantType.MELEE),
    HEAD_HUNTER(EnchantType.MELEE),
    GREATNESS(EnchantType.MELEE),
    FINAL_BLOW(EnchantType.MELEE),
    PARALYZE(EnchantType.MELEE),
    BERSERK(EnchantType.MELEE),
    CYNICAL(EnchantType.MELEE),
    SWARM(EnchantType.MELEE);

    private String name;
    private List<String> description;
    private double duration;
    private double increase;
    private int chance;
    private EnchantType enchantType;

    Enchant(EnchantType enchantType) {
        this.name = "";
        this.description = new ArrayList<>();
        this.duration = -1;
        this.increase = -1;
        this.chance = -1;
        this.enchantType = enchantType;

    }

    private static String parseLoreString(String s) {
        s = s.replaceAll("\\s+", "_");
        s = s.replaceAll("-", "_");
        s = ChatColor.stripColor(s);
        s = s.toUpperCase();

        return s;
    }

    public EnchantType getEnchantType() {
        return enchantType;
    }

    public String getName() {
        return name;
    }

    public List<String> getDescription() {
        return description;
    }

    public double getDuration() {
        return duration;
    }

    public double getIncrease() {
        return increase;
    }

    public int getChance() {
        return chance;
    }

    public static String getEnchantPath() {
        return enchantPath;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(List<String> description) {
        this.description = description;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }

    public void setIncrease(double increase) {
        this.increase = increase;
    }

    public void setChance(int chance) {
        this.chance = chance;
    }

    public static File file;
    public static FileConfiguration enchantFile;
    private static String enchantPath = "enchants.yml";

    public static FileConfiguration getEnchantFile() {
        if (enchantFile == null) enchantFile = YamlConfiguration.loadConfiguration(getFile());
        return enchantFile;
    }

    private static File getFile() {
        if (file == null) {
            RPG.getPlugin().saveResource(enchantPath, false);
            file = new File(RPG.getPlugin().getDataFolder(), enchantPath);
        }
        return file;
    }

    public static String getTypeString(Enchant enchant) {
        return enchant.getEnchantType().getConfigName();
    }

    public static void initializeValues() {

        for (Enchant enchant : Enchant.values()) {
            RPG.getPlugin().getLogger().info("RPG ENCHANT PATH: " + getTypeString(enchant) + enchant.toString() + ".name");
            RPG.getPlugin().getLogger().info("RPG ENCHANT LOADED: " + Enchant.getEnchantFile().getString(getTypeString(enchant) + enchant.toString() + ".name"));
            enchant.setName(Enchant.getEnchantFile().getString(getTypeString(enchant) + enchant.toString() + ".name"));
            enchant.setDescription(Enchant.getEnchantFile().getStringList(getTypeString(enchant) + enchant.toString() + ".description"));
            if (Enchant.getEnchantFile().get(getTypeString(enchant) + enchant.toString() + ".chance") != null) {
                enchant.setChance(Enchant.getEnchantFile().getInt(getTypeString(enchant) + enchant.toString() + ".chance"));
            }

            if (Enchant.getEnchantFile().get(getTypeString(enchant) + enchant.toString() + ".duration") != null) {
                enchant.setDuration(Enchant.getEnchantFile().getDouble(getTypeString(enchant) + enchant.toString() + ".duration"));
            }

            if (Enchant.getEnchantFile().get(getTypeString(enchant) + enchant.toString() + ".increase") != null) {

                enchant.setIncrease(Enchant.getEnchantFile().getDouble(getTypeString(enchant) + enchant.toString() + ".increase"));

            }

        }

    }

}

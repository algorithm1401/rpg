package org.pixelgalaxy.volcano;

import org.bukkit.inventory.ItemStack;

public enum VolcanoType {

    VOLCANO_STANDARD(27, "§c§lVolcano §8§lStone"),
    VOLCANO_GREAT(27, "§c§lGreat Volcano §8§lStone"),
    VOLCANO_SUPER(27, "§c§l§nSuper Volcano§r §8§lStone"),
    VOLCANO_GODLY(27, "§c§l§nGodly Volcano§r §8§lStone");

    private int amountOfItems;
    private String itemName;

    VolcanoType(int amountOfItems, String itemName) {
        this.amountOfItems = amountOfItems;
        this.itemName = itemName;
    }

    public int getAmountOfItems() {
        return this.amountOfItems;
    }

    public String getItemName() {
        return this.itemName;
    }

    public static VolcanoType getVolcanoTypeFromitem(ItemStack is) {

        for (VolcanoType volcanoType : VolcanoType.values()) {

            if (volcanoType.getItemName().equalsIgnoreCase(is.getItemMeta().getDisplayName())) {
                return volcanoType;
            }

        }
        return null;
    }

}

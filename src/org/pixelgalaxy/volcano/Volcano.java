package org.pixelgalaxy.volcano;

import com.boydti.fawe.FaweAPI;
import com.boydti.fawe.object.schematic.Schematic;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.bukkit.BukkitUtil;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.pixelgalaxy.RPG;
import org.pixelgalaxy.utils.schematics.BuildTask;
import org.pixelgalaxy.utils.schematics.CustomScheduler;
import org.pixelgalaxy.utils.schematics.CustomSchematic;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class Volcano implements Listener {

    public static ItemStack getVolcanoStack(VolcanoType volcanoType) {

        ItemStack toAdd = null;
        switch (volcanoType) {
            case VOLCANO_STANDARD:
                toAdd = Volcano.getVolcanoStone();
                break;
            case VOLCANO_GREAT:
                toAdd = Volcano.getGreatVolcanoStone();
                break;
            case VOLCANO_SUPER:
                toAdd = Volcano.getSuperVolcanoStone();
                break;
            case VOLCANO_GODLY:
                toAdd = Volcano.getGodlyVolcanoStone();
                break;
        }

        return toAdd;
    }

    public static ItemStack getVolcanoStone() {

        ItemStack volcanoStone = new ItemStack(Material.COAL);
        volcanoStone.addUnsafeEnchantment(Enchantment.FIRE_ASPECT, 1);
        ItemMeta volcanoStonem = volcanoStone.getItemMeta();
        volcanoStonem.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        volcanoStonem.setDisplayName("§c§lVolcano §8§lStone");
        volcanoStonem.setLore(Arrays.asList(

                "§7Right click this item to see",
                "§7a preview of the volcano that",
                "§7will shoot out items!"

        ));
        volcanoStone.setItemMeta(volcanoStonem);
        return volcanoStone;
    }

    public static ItemStack getGreatVolcanoStone() {
        ItemStack stone = getVolcanoStone();
        ItemMeta im = stone.getItemMeta();
        im.setDisplayName(VolcanoType.VOLCANO_GREAT.getItemName());
        stone.setItemMeta(im);
        return stone;
    }

    public static ItemStack getSuperVolcanoStone() {
        ItemStack stone = getVolcanoStone();
        ItemMeta im = stone.getItemMeta();
        im.setDisplayName(VolcanoType.VOLCANO_SUPER.getItemName());
        stone.setItemMeta(im);
        return stone;
    }

    public static ItemStack getGodlyVolcanoStone() {
        ItemStack stone = getVolcanoStone();
        ItemMeta im = stone.getItemMeta();
        im.setDisplayName(VolcanoType.VOLCANO_GODLY.getItemName());
        stone.setItemMeta(im);
        return stone;
    }

    public static List<ItemStack> getAllVolcanoStones() {
        List<ItemStack> volcanoStones = Arrays.asList(getVolcanoStone(), getGreatVolcanoStone(), getSuperVolcanoStone(), getGodlyVolcanoStone());
        return volcanoStones;
    }

    @EventHandler
    public void onItemUse(PlayerInteractEvent e) {

        Action a = e.getAction();
        Player p = e.getPlayer();

        if (e.getItem() != null) {

            ItemStack itemCopy = e.getItem().clone();
            itemCopy.setAmount(1);

            if (getAllVolcanoStones().contains(itemCopy)) {

                if (a.equals(Action.LEFT_CLICK_BLOCK)) {

                    VolcanoType volcanoType = VolcanoType.getVolcanoTypeFromitem(e.getItem());

                    p.getInventory().removeItem(getVolcanoStack(volcanoType));

                    Location targetLocation = BuildTask.getTargetBlock(p, 10).getLocation();

                    Map<Location, Material> resetBlocks = new HashMap<>();

                    for (int y = targetLocation.getBlockY(); y <= targetLocation.getBlockY() + 6; y++) {

                        for (int x = targetLocation.getBlockX() - 3; x <= targetLocation.getBlockX() + 3; x++) {

                            for (int z = targetLocation.getBlockZ() - 3; z <= targetLocation.getBlockZ() + 3; z++) {

                                Location saveLocation = new Location(targetLocation.getWorld(), x, y, z);
                                resetBlocks.put(saveLocation, targetLocation.getWorld().getBlockAt(saveLocation).getType());

                            }

                        }
                    }

                    CustomSchematic schematic = RPG.getPlugin().getPlayerManagement().getBuilding(p.getUniqueId());
                    List<Location> locations = schematic.pasteSchematic(targetLocation, p, false);
                    if (locations != null) {

                        CustomScheduler scheduler = new CustomScheduler();

                        scheduler.setTask(Bukkit.getScheduler().scheduleSyncRepeatingTask(RPG.getPlugin(), () -> {
                            for (Location loc : locations) {
                                if (loc.getBlock().getType() == Material.AIR)
                                    loc.getWorld().spawnParticle(Particle.VILLAGER_HAPPY, loc.getX() + 0.5, loc.getY(), loc.getZ() + 0.5, 2);
                            }

                            if (schematic.isPasted()) {
                                scheduler.cancel();
                                schematic.setPasted(false);
                                shootItems shootItems = new shootItems(targetLocation.add(0, 5, 0), volcanoType, resetBlocks);
                                shootItems.runTaskTimer(RPG.getPlugin(), 0, 10);

                            }

                        }, 0, 3));
                        p.sendMessage("§aNow building volcano!");
                        RPG.getPlugin().getPlayerManagement().setBuilding(p.getUniqueId(), false, "null");
                    } else {
                        p.sendMessage("§cYou can't build that here!");
                    }
                }

            }
        }
    }


}

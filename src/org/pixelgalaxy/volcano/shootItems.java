package org.pixelgalaxy.volcano;

import jdk.nashorn.internal.ir.Block;
import org.apache.commons.lang.StringUtils;
import org.bukkit.*;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;
import org.pixelgalaxy.RPG;
import org.pixelgalaxy.crystals.Opener;
import org.pixelgalaxy.equipment.Equipment;
import org.pixelgalaxy.equipment.Rarity;
import org.pixelgalaxy.level.XP;
import org.pixelgalaxy.utils.Chance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class shootItems extends BukkitRunnable {

    private static double MAX_COMMON_NUMBER;
    private static double MAX_RARE_NUMBER;
    private static double MAX_EPIC_NUMBER;
    private static double MAX_LEGENDARY_NUMBER;

    private Location loc;
    private int maxAmountOfItems;
    private Map<Location, Material> resetBlocks = new HashMap<>();
    private List<ItemStack> rewards;

    public shootItems(Location loc, VolcanoType volcanoType, Map<Location, Material> resetBlocks) {
        this.loc = loc;
        this.maxAmountOfItems = volcanoType.getAmountOfItems();
        this.resetBlocks = resetBlocks;
        rewards = getRewardsForVolcano(volcanoType);
    }

    public static void initMaxValues() {

        double common = 0;
        double rare = 0;
        double epic = 0;
        double legendary = 0;

        for (String s : Opener.getCrystalFile().getKeys(true)) {

            Bukkit.getServer().getLogger().info("S: " + s);

            for (String loreLine : Opener.getCrystalFile().getStringList(s)) {

                double parsed = Double.valueOf(StringUtils.substringAfterLast(loreLine, " "));

                if (loreLine.contains("rpg") && !loreLine.contains("xp")) {

                    if (s.contains("COMMON")) {

                        common += parsed;

                    } else if (s.contains("RARE")) {

                        rare += parsed;

                    } else if (s.contains("EPIC")) {

                        epic += parsed;

                    } else if (s.contains("LEGENDARY")) {

                        legendary += parsed;

                    }

                }
            }

        }

        MAX_COMMON_NUMBER = common;
        MAX_RARE_NUMBER = rare;
        MAX_EPIC_NUMBER = epic;
        MAX_LEGENDARY_NUMBER = legendary;

    }

    private static double getMaxValueForRarity(Rarity r) {

        switch (r) {

            case COMMON:
                return MAX_COMMON_NUMBER;

            case RARE:
                return MAX_RARE_NUMBER;

            case EPIC:
                return MAX_EPIC_NUMBER;

            case LEGENDARY:
                return MAX_LEGENDARY_NUMBER;

        }
        return 0.0;
    }

    public static ItemStack getRandomReward(Rarity r, int level) {

        double randomChance = Chance.getRandom(0, (int) (getMaxValueForRarity(r) * 100)) / 100;

        for (String s : Opener.getCrystalFile().getKeys(true)) {

            for (String item : Opener.getCrystalFile().getStringList(s)) {

                if (s.equalsIgnoreCase(ChatColor.stripColor(r.getName())) && item.contains("rpg") && !item.contains("xp")) {

                    double chance = Double.valueOf(StringUtils.substringAfterLast(item, " "));
                    randomChance -= chance;
                    if (randomChance <= 0) {

                        String itemName = StringUtils.substringBetween(item, ";", ",");
                        itemName = itemName.toLowerCase();
                        ItemStack toAdd = new Equipment(itemName, Equipment.getItemsOfLevel(level)).build();
                        return toAdd;

                    }
                }

            }

        }
        return null;
    }

    private static List<ItemStack> getRewardsForVolcano(VolcanoType volcanoType) {

        List<ItemStack> rewards = new ArrayList<>();

        switch (volcanoType) {

            case VOLCANO_STANDARD:

                for (int i = 0; i < 19; i++) {
                    rewards.add(getRandomReward(Rarity.RARE, 1));
                }

                for (int i = 0; i < 6; i++) {
                    rewards.add(getRandomReward(Rarity.EPIC, 1));
                }

                for (int i = 0; i < 2; i++) {
                    rewards.add(getRandomReward(Rarity.LEGENDARY, 1));
                }

                break;

            case VOLCANO_GREAT:

                for (int i = 0; i < 11; i++) {
                    rewards.add(getRandomReward(Rarity.RARE, 1));
                }

                for (int i = 0; i < 8; i++) {
                    rewards.add(getRandomReward(Rarity.EPIC, 1));
                }

                for (int i = 0; i < 8; i++) {
                    rewards.add(getRandomReward(Rarity.LEGENDARY, 1));
                }

                break;

            case VOLCANO_SUPER:

                for (int i = 0; i < 9; i++) {
                    rewards.add(getRandomReward(Rarity.EPIC, 1));
                }

                for (int i = 0; i < 18; i++) {
                    rewards.add(getRandomReward(Rarity.LEGENDARY, 1));
                }

                break;

            case VOLCANO_GODLY:

                for (int i = 0; i < 27; i++) {
                    rewards.add(getRandomReward(Rarity.LEGENDARY, 2));
                }

                break;

        }
        return rewards;
    }

    @Override
    public void run() {

        if (maxAmountOfItems == 0) {

            loc.getWorld().playSound(loc, Sound.ENTITY_GENERIC_EXPLODE, 1.0F, 1.0F);
            for (Location loc : resetBlocks.keySet()) {

                loc.getWorld().spawnParticle(Particle.EXPLOSION_LARGE, loc, 5, 0.5F, 0.5F, 0.5F, 0.01);
                loc.getWorld().getBlockAt(loc).setType(resetBlocks.get(loc));

            }
            this.cancel();

        } else {
            loc.getWorld().spawnParticle(Particle.SMOKE_LARGE, new Location(loc.getWorld(), loc.getBlockX(), loc.getBlockY(), loc.getBlockZ()), 30, 0.5F, 0.5F, 0.5F, 0.01);
            loc.getWorld().spawnParticle(Particle.LAVA, new Location(loc.getWorld(), loc.getBlockX(), loc.getBlockY(), loc.getBlockZ()), 10, 0.5F, 0.5F, 0.5F, 0.01);

            // Make the items only pickup-able by the player who placed the volcano

            ItemStack reward = rewards.get(27 - maxAmountOfItems);
            Item shotItem = loc.getWorld().dropItem(new Location(loc.getWorld(), loc.getBlockX(), loc.getBlockY() + 1, loc.getBlockZ()), reward);

            double randomX = Chance.getRandom(0, 200);
            double randomY = Chance.getRandom(0, 200);
            if (randomX > 100) {
                randomX = -randomX + 100;
            }

            if (randomY > 100) {
                randomY = -randomY + 100;
            }
            randomX /= 100;
            randomY /= 100;
            shotItem.setVelocity(new Vector(randomX, 1.5, randomY).multiply(0.5));

            loc.getWorld().playSound(loc, Sound.BLOCK_LAVA_EXTINGUISH, 1.0F, 1.0F);

            maxAmountOfItems--;

        }

    }

}

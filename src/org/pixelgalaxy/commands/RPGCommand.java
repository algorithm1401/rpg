package org.pixelgalaxy.commands;/**
 * Created by robin on 29/06/2018.
 */

import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.bukkit.selections.Selection;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.pixelgalaxy.RPG;
import org.pixelgalaxy.RPGPlayer;
import org.pixelgalaxy.boosters.Booster;
import org.pixelgalaxy.boosters.BoosterType;
import org.pixelgalaxy.crystals.Crystal;
import org.pixelgalaxy.disparity.ChestUpdater;
import org.pixelgalaxy.disparity.Disparity;
import org.pixelgalaxy.ekits.EKitType;
import org.pixelgalaxy.ekits.Ekit;
import org.pixelgalaxy.enchants.InventoryList;
import org.pixelgalaxy.equipment.CustomEquipment;
import org.pixelgalaxy.equipment.Equipment;
import org.pixelgalaxy.equipment.Rarity;
import org.pixelgalaxy.events.onPlayerDeath;
import org.pixelgalaxy.insurances.Insurance;
import org.pixelgalaxy.level.XP;
import org.pixelgalaxy.npcs.Bounties;
import org.pixelgalaxy.npcs.Salvage;
import org.pixelgalaxy.npcs.Shopkeeper;
import org.pixelgalaxy.player_objects.PlayerLevel;
import org.pixelgalaxy.skills.Skill_Inventory;
import org.pixelgalaxy.tags.Tags;
import org.pixelgalaxy.utils.configSavers;
import org.pixelgalaxy.volcano.Volcano;
import org.pixelgalaxy.volcano.VolcanoType;

import java.util.*;

/**
 * Created by robin on 13/06/2018.
 */
public class RPGCommand implements CommandExecutor {

    RPG plugin = RPG.getPlugin();
    private static Map<Player, Long> dpcooldown = new HashMap<>();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            Player p = (Player) sender;

            RPGPlayer rpgPlayer = RPGPlayer.get(p);

            if (RPG.getPlugin().checkValidCommand(cmd, "rpg", "rpg.use", p)) {

                if (args.length == 0 && p.hasPermission("rpg.use.help")) {

                    List<String> helpmessages = plugin.getConfig().getStringList("helpmessage");

                    helpmessages.forEach(s -> {

                        p.sendMessage(ChatColor.translateAlternateColorCodes('&', s));

                    });

                } else if ((args.length == 1 || args.length == 2) && args[0].equalsIgnoreCase("skills") && p.hasPermission("rpg.use.skills")) {

                    if (args.length == 2) {
                        if (Bukkit.getServer().getOfflinePlayer(args[1]) != null) {

                            Player target = (Player) Bukkit.getOfflinePlayer(args[1]);

                            Skill_Inventory.openSkillsMenu(target, p);

                        } else {
                            p.sendMessage(RPG.getPrefix() + "§cUnknown player!");
                        }
                    } else {

                        Skill_Inventory.openSkillsMenu(p);

                    }

                } else if ((args.length >= 1 && args.length <= 3) && args[0].equalsIgnoreCase("level")) {

                    if (args.length == 1) {

                        p.sendMessage(RPG.getPrefix() + "§aYour current level is: §6" + rpgPlayer.getPl().getPlayerLevel());

                    } else if (args.length == 2 && p.hasPermission("rpg.admin")) {

                        if (Integer.valueOf(args[1]) != null) {

                            int level = Integer.valueOf(args[1]);
                            RPGPlayer.get(p).getPl().setLevel(level);

                            PlayerLevel.setPlayerXPBar(p);
                            p.sendMessage(RPG.getPrefix() + "§aYou have set your level to: §6" + args[1]);

                        } else {
                            p.sendMessage(RPG.getPrefix() + "§cYou need to enter a valid value as level!");
                        }

                    } else if (args.length == 3 && p.hasPermission("rpg.admin")) {

                        if (Integer.valueOf(args[1]) != null) {

                            if (Bukkit.getServer().getOfflinePlayer(args[2]) != null) {

                                Player target = (Player) Bukkit.getServer().getOfflinePlayer(args[2]);

                                int level = Integer.valueOf(args[1]);
                                RPGPlayer.get(p).getPl().setLevel(level);

                                PlayerLevel.setPlayerXPBar(target);
                                p.sendMessage(RPG.getPrefix() + "§aYou have set §6" + args[2] + "'s §alevel to: §6" + args[1]);

                            } else {
                                p.sendMessage(RPG.getPrefix() + "§cInvalid player!");
                            }

                        } else {
                            p.sendMessage(RPG.getPrefix() + "§cYou need to enter a valid value as level!");
                        }

                    }

                } else if ((args.length >= 1 && args.length <= 3) && args[0].equalsIgnoreCase("xp")) {

                    if (args.length == 1) {

                        p.sendMessage(RPG.getPrefix() + "§aYour current experience is: §6" + rpgPlayer.getPl().getExperience());

                    } else if (args.length == 2 && p.hasPermission("rpg.admin")) {

                        if (Integer.valueOf(args[1]) != null) {

                            int experience = Integer.valueOf(args[1]);
                            RPGPlayer.get(p).getPl().setExperience(experience);

                            PlayerLevel.setPlayerXPBar(p);
                            p.sendMessage(RPG.getPrefix() + "§aYou have set your experience to: §6" + args[1]);

                        } else {
                            p.sendMessage(RPG.getPrefix() + "§cYou need to enter a valid value as level!");
                        }

                    } else if (args.length == 3 || args.length == 4 && p.hasPermission("rpg.admin")) {

                        if (args[1].equalsIgnoreCase("bottle")) {

                            if (Integer.valueOf(args[2]) != null) {

                                if (args.length == 3) {

                                    int amount = Integer.valueOf(args[2]);
                                    ItemStack bottle = XP.getBottle(amount);
                                    p.getInventory().addItem(bottle);
                                    p.sendMessage(RPG.getPrefix() + "§aYou have received a bottle of " + bottle.getItemMeta().getDisplayName() + "§a!");
                                } else {

                                    if (Bukkit.getServer().getOfflinePlayer(args[3]) != null) {
                                        Player target = (Player) Bukkit.getServer().getOfflinePlayer(args[3]);
                                        int amount = Integer.valueOf(args[2]);
                                        ItemStack bottle = XP.getBottle(amount);
                                        target.getInventory().addItem(bottle);
                                        target.sendMessage(RPG.getPrefix() + "§aYou have received a bottle of " + bottle.getItemMeta().getDisplayName() + "§a!");

                                    }

                                }

                            } else if (Integer.valueOf(args[1]) != null) {

                                if (Bukkit.getServer().getOfflinePlayer(args[2]) != null) {

                                    Player target = (Player) Bukkit.getServer().getOfflinePlayer(args[2]);

                                    PlayerLevel.setPlayerXPBar(target);
                                    p.sendMessage(RPG.getPrefix() + "§aYou have set §6" + args[2] + "'s §aExperience to: §6" + args[1]);

                                } else {
                                    p.sendMessage(RPG.getPrefix() + "§cInvalid player!");
                                }

                            }

                        }

                    }

                } else if ((args.length >= 2 && args.length <= 4) && args[0].equalsIgnoreCase("equipment") && p.hasPermission("rpg.admin")) {

                    if (args.length == 2) {

                        if (args[1].equalsIgnoreCase("list")) {

                            p.sendMessage(Equipment.getWeaponListString());

                        } else if ((new Equipment(args[1], 1).build()) != null) {

                            p.getInventory().addItem(new Equipment(args[1], 1).build());

                        } else {
                            p.sendMessage(RPG.getPrefix() + "§cUnknown custom weapon/armor!");
                        }

                    } else if (args.length == 3) {

                        if (Bukkit.getOfflinePlayer(args[1]) != null) {

                            Player target = (Player) Bukkit.getServer().getOfflinePlayer(args[1]);

                            if (target.isOnline()) {

                                if (new Equipment(args[2].toLowerCase(), 1).build() != null) {

                                    target.getInventory().addItem(new Equipment(args[2].toLowerCase(), 1).build());

                                } else {

                                    if (Integer.valueOf(args[2]) != null) {

                                        int level = Integer.valueOf(args[3]);

                                        target.getInventory().addItem(new Equipment(args[2].toLowerCase(), Equipment.getItemsForLevel(level)).build());

                                    }

                                }

                            }

                        }
                    } else if (args.length == 4) {

                        if (Bukkit.getOfflinePlayer(args[1]) != null) {

                            Player target = (Player) Bukkit.getServer().getOfflinePlayer(args[1]);

                            if (target.isOnline()) {

                                if (new Equipment(args[2].toLowerCase(), 1) != null) {

                                    if (Integer.valueOf(args[3]) != null) {

                                        int level = Integer.valueOf(args[3]);

                                        target.getInventory().addItem(new Equipment(args[2].toLowerCase(), Equipment.getItemsOfLevel(level)).build());

                                    }

                                }

                            }

                        }


                    }

                } else if (args.length == 1 && args[0].equalsIgnoreCase("portalLocation") && p.hasPermission("rpg.admin")) {

                    WorldEditPlugin worldEdit = (WorldEditPlugin) Bukkit.getServer().getPluginManager().getPlugin("WorldEdit");
                    Selection selection = worldEdit.getSelection(p);

                    List<Location> toSave = new ArrayList<>();

                    if (selection != null) {
                        Location min = selection.getMinimumPoint();
                        Location max = selection.getMaximumPoint();

                        for (int y = (int) min.getY(); y < max.getY() + 1; y++) {

                            for (int z = (int) min.getZ(); z < max.getZ() + 1; z++) {

                                toSave.add(new Location(p.getWorld(), min.getX(), y, z));

                            }

                        }

                        for (int i = 1; i < toSave.size(); i++) {
                            configSavers.saveLocation("portal_location." + i, toSave.get(i - 1));
                        }

                        p.sendMessage(RPG.getPrefix() + "§aYou saved your selection as portal locations!");

                    } else {
                        p.sendMessage(RPG.getPrefix() + "§cYou need to make a selection first!");
                    }

                } else if (args.length == 1 && args[0].equalsIgnoreCase("shop") && p.hasPermission("rpg.use.shop")) {

                    Shopkeeper.openStartMenu(p);

                } else if (args.length == 1 && args[0].equalsIgnoreCase("enchants") && p.hasPermission("rpg.use.enchants")) {

                    InventoryList.openEnchantTypes(p);

                } else if (args.length == 1 && args[0].equalsIgnoreCase("salvage") && p.hasPermission("rpg.use.salvage")) {

                    Salvage.open(p);

                } else if (args.length == 1 && args[0].equalsIgnoreCase("bounties") && p.hasPermission("rpg.use.bounties")) {

                    Bounties.open(p);

                } else if (args.length == 1 && args[0].equalsIgnoreCase("bountyForceUpdate") && p.hasPermission("rpg.admin")) {
                    Bounties.update(new Date());
                } else if (args.length == 1 && args[0].equalsIgnoreCase("setDisparityLocation") && p.hasPermission("rpg.admin")) {
                    configSavers.saveLocation("spawn_location", p.getLocation());
                    p.sendMessage(RPG.getPrefix() + "§aYou have succesfully set the spawn location.");
                } else if (args.length >= 1 && args.length <= 3 && args[0].equalsIgnoreCase("chest") && p.hasPermission("rpg.admin")) {

                    if (args.length == 2) {

                        if (args[1].equalsIgnoreCase("toggle")) {

                            Disparity.setToggleMode(p);

                        } else if (args[1].equalsIgnoreCase("rewards")) {

                            Disparity.openRewardsEditor(p);

                        } else if (args[1].equalsIgnoreCase("forceupdate")) {
                            ChestUpdater.forceUpdate();

                        } else if (args[1].equalsIgnoreCase("addReward")) {

                            Disparity.addReward(p.getInventory().getItemInMainHand(), p);

                        }

                    }

                } else if (args.length == 1 && args[0].equalsIgnoreCase("dp") && p.hasPermission("rpg.use.dp")) {

                    if (onPlayerDeath.dpLocations.containsKey(p) && (!dpcooldown.containsKey(p) || System.currentTimeMillis() - dpcooldown.get(p) > 60000) && onPlayerDeath.playerDeathTime.containsKey(p)) {
                        if (System.currentTimeMillis() - onPlayerDeath.playerDeathTime.get(p) > 5000) {
                            p.teleport(onPlayerDeath.dpLocations.get(p));
                            onPlayerDeath.dpLocations.remove(p);
                            if (dpcooldown.containsKey(p)) {
                                dpcooldown.remove(p);
                            }
                            if (onPlayerDeath.playerDeathTime.containsKey(p)) {
                                onPlayerDeath.playerDeathTime.remove(p);
                            }
                            dpcooldown.put(p, System.currentTimeMillis());
                        } else {
                            p.sendMessage(RPG.getPrefix() + "§cYou can't teleport this quickly to your deathpoint!");
                        }
                    } else {
                        p.sendMessage(RPG.getPrefix() + "§cTeleporting to your deathpoint is on cooldown!");
                    }

                }
            }
            return false;
        } else {

            if (args.length == 3 && args[0].equalsIgnoreCase("ekit")) {

                Ekit.giveRandom((Player) Bukkit.getServer().getOfflinePlayer(args[2]), EKitType.valueOf(args[1].toUpperCase()));

            } else if (args.length == 2 && args[0].equalsIgnoreCase("insurance")) {

                if (Bukkit.getServer().getOfflinePlayer(args[1]) != null) {

                    Player target = (Player) Bukkit.getServer().getOfflinePlayer(args[1]);

                    target.getInventory().addItem(Insurance.getInsuranceStack(1));
                    target.sendMessage(RPG.getPrefix() + "§aYou have received: §6an Item Insurance");

                }

            } else if (args.length == 2 && args[0].equalsIgnoreCase("default")) {

                if (Bukkit.getOfflinePlayer(args[1]) != null) {
                    Player target = (Player) Bukkit.getOfflinePlayer(args[1]);
                    if (target.isOnline()) {
                        target.sendMessage(RPG.getPrefix() + "§aYou have reseted all your stats to default!");

                        RPGPlayer rpgPlayer = RPGPlayer.get(target);

                        rpgPlayer.getPs().defaultSkills();
                        int level = rpgPlayer.getPl().getPlayerLevel();
                        if (level > 1) {
                            rpgPlayer.getPs().setToSpend(rpgPlayer.getPl().getPlayerLevel() * 2 - 2);
                        }
                        CustomEquipment.updatePlayerArmor(target);
                        Tags.update(target);
                    }
                }
            } else if ((args.length == 3 || args.length == 4) && args[0].equalsIgnoreCase("crystal")) {

                if (Bukkit.getOfflinePlayer(args[1]) != null) {

                    Player target = (Player) Bukkit.getOfflinePlayer(args[1]);

                    if (target.isOnline()) {

                        if (Rarity.valueOf(args[2].toUpperCase()) != null) {

                            Rarity r = Rarity.valueOf(args[2].toUpperCase());

                            if (args.length == 4) {

                                if (Integer.valueOf(args[3]) != null) {

                                    int amount = Integer.valueOf(args[3]);
                                    Crystal.giveCrystal(target, r, amount);

                                }

                            } else if (args.length == 3) {

                                Crystal.giveCrystal(target, r, 1);

                            }

                        }
                    }

                } else if (args[1].equalsIgnoreCase("giveall")) {

                    if (Rarity.valueOf(args[2].toUpperCase()) != null) {

                        Rarity r = Rarity.valueOf(args[2].toUpperCase());

                        if (Integer.valueOf(args[3]) == null) {

                            for (Player online : Bukkit.getServer().getOnlinePlayers()) {

                                Crystal.giveCrystal(online, r, 1);

                            }
                        } else {

                            Integer amount = Integer.valueOf(args[3]);

                            for (Player online : Bukkit.getServer().getOnlinePlayers()) {

                                Crystal.giveCrystal(online, r, amount);

                            }
                        }

                    }
                }


            } else if (args.length == 3 && args[0].equalsIgnoreCase("volcanostone")) {

                if (VolcanoType.valueOf(args[1].toUpperCase()) != null) {

                    VolcanoType volcanoStoneType = VolcanoType.valueOf(args[1].toUpperCase());

                    if (Bukkit.getOfflinePlayer(args[2]) != null) {

                        Player target = (Player) Bukkit.getOfflinePlayer(args[2]);

                        ItemStack toAdd = Volcano.getVolcanoStack(volcanoStoneType);

                        target.getInventory().addItem(toAdd);
                        target.sendMessage(RPG.getPrefix() + "§aYou have received a volcano stone!");

                    }
                }
            } else if (args.length == 3 && args[0].equalsIgnoreCase("booster")) {

                BoosterType boosterType = BoosterType.valueOf(args[1].toUpperCase());

                if (boosterType != null) {

                    if (Bukkit.getOfflinePlayer(args[2]) != null) {

                        Player p = (Player) Bukkit.getOfflinePlayer(args[2]);

                        Booster.addBooster(p, boosterType);

                    }

                }

            }

        }
        return true;
    }
}
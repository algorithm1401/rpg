package org.pixelgalaxy.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.pixelgalaxy.RPG;
import org.pixelgalaxy.equipment.CustomEquipment;

/**
 * Created by robin on 26/08/2018.
 */
public class EquipmentCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if(sender instanceof Player){

            Player p = (Player) sender;

            if (RPG.checkValidCommand(cmd, "equipment", "rpg.use.equipmentinv", p)) {

                if(args.length == 0){

                    CustomEquipment.openPlayerEquipment(p, p);

                }else if(args.length == 1){

                    if(Bukkit.getServer().getOfflinePlayer(args[0]) != null){

                        Player target = (Player) Bukkit.getServer().getOfflinePlayer(args[0]);
                        CustomEquipment.openPlayerEquipment(p, target);

                    }

                }

            }

        }

        return false;
    }
}

package org.pixelgalaxy;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.pixelgalaxy.equipment.Equipment;
import org.pixelgalaxy.player_objects.*;
import org.pixelgalaxy.mysql.MysqlConnection;
import org.pixelgalaxy.utils.ItemStackBase;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

/**
 * Created by robin on 2/09/2018.
 */
public class RPGPlayer {

    private static Map<Player, RPGPlayer> rpgPlayerMap = new HashMap<>();

    private PlayerEquipment peq;
    private PlayerEnchantments pen;
    private PlayerSkills ps;
    private PlayerCombat pc;
    private PlayerLevel pl;
    private PlayerBounties pb;


    public RPGPlayer(PlayerEquipment peq, PlayerEnchantments pen, PlayerSkills ps, PlayerCombat pc, PlayerLevel pl, PlayerBounties pb) {
        this.peq = peq;
        this.pen = pen;
        this.ps = ps;
        this.pc = pc;
        this.pl = pl;
        this.pb = pb;
    }

    public static RPGPlayer get(Player p) {
        return rpgPlayerMap.get(p);
    }

    private static void remove(Player p) {
        rpgPlayerMap.remove(p);
    }

    public PlayerLevel getPl() {
        return pl;
    }

    public PlayerBounties getPb() {
        return pb;
    }

    public PlayerCombat getPc() {
        return pc;
    }

    public PlayerEquipment getPeq() {
        return peq;
    }

    public PlayerEnchantments getPen() {
        return pen;
    }

    public PlayerSkills getPs() {
        return ps;
    }

    public void setPen(PlayerEnchantments pen) {
        this.pen = pen;
    }

    public void setPeq(PlayerEquipment peq) {
        this.peq = peq;
    }

    public static Map<Player, RPGPlayer> all() {
        return rpgPlayerMap;
    }

    public static void loadRPGPlayer(Player p) {

        PlayerEquipment peq = loadPlayerEquipment(p);
        PlayerEnchantments pen = loadPlayerEnchantments(peq, p);
        PlayerCombat pc = loadPlayerCombat(p);
        PlayerBounties pb = loadPlayerBounties(p);
        PlayerSkills ps;
        PlayerLevel pl;

        if (p.hasPlayedBefore()) {

            ps = loadPlayerSkills(p);
            pl = loadPlayerLevel(p);

        } else {

            ps = new PlayerSkills(1, 1, 1, 1, 1, 1, 0);
            pl = new PlayerLevel(PlayerLevel.getXpOfLevel(1));
            pb = new PlayerBounties(new ArrayList<>(), new ArrayList<>());
            pc = new PlayerCombat(false, false);

        }

        rpgPlayerMap.put(p, new RPGPlayer(peq, pen, ps, pc, pl, pb));
    }

    public static PlayerCombat loadPlayerCombat(Player p) {
        return new PlayerCombat(false, false);
    }

    public static PlayerBounties loadPlayerBounties(Player p) {

        List<Integer> bountyTypes;
        List<Integer> bountyProgress;
        String bountyTypesS = new String("");
        String bountyProgressS = new String("");
        ResultSet rs = RPG.SQL.mysql.query("SELECT * FROM `player_bounties` WHERE uuid ='" + p.getUniqueId().toString() + "'");
        try {
            while (rs.next()) {

                bountyTypesS = rs.getString("bounty_types");
                bountyProgressS = rs.getString("bounty_progress");

            }
        } catch (SQLException ex) {
            RPG.getPlugin().getLogger().info("Error executing a query: " + ex.getErrorCode());
        }

        bountyTypes = parseStringToIntList(bountyTypesS);
        bountyProgress = parseStringToIntList(bountyProgressS);

        return new PlayerBounties(bountyTypes, bountyProgress);
    }

    private static List<Integer> parseStringToIntList(String bountyS) {
        List<Integer> intList = new ArrayList<>();
        if (bountyS.contains(",")) {
            for (String s : bountyS.split(",")) {

                intList.add(Integer.valueOf(s));

            }
        } else {
            if (bountyS != "" && bountyS != null && bountyS.length() > 0 && !bountyS.contains("\\s+")) {
                intList.add(Integer.valueOf(bountyS));
            } else {
                return new ArrayList<>();
            }
        }
        return intList;
    }

    public static PlayerEnchantments loadPlayerEnchantments(PlayerEquipment peq, Player p) {

        PlayerEnchantments pen = new PlayerEnchantments(peq);

        return pen;
    }

    public static PlayerEquipment loadPlayerEquipment(Player p) {

        PlayerEquipment pe = new PlayerEquipment(

                Equipment.getItem(p, "combat_boots", 37),
                Equipment.getItem(p, "combat_leggings", 28),
                Equipment.getItem(p, "combat_chestplate", 19),
                Equipment.getItem(p, "combat_helmet", 10),
                Equipment.getItem(p, "earring1", 12),
                Equipment.getItem(p, "necklace", 13),
                Equipment.getItem(p, "earring2", 14),
                Equipment.getItem(p, "ring1", 30),
                Equipment.getItem(p, "ring2", 32),
                Equipment.getItem(p, "scroll", 31),
                Equipment.getItem(p, "cosmetic_boots", 43),
                Equipment.getItem(p, "cosmetic_leggings", 34),
                Equipment.getItem(p, "cosmetic_chestplate", 25),
                Equipment.getItem(p, "cosmetic_helmet", 16),
                p.getInventory().getItemInOffHand()

        );

        return pe;
    }

    public static PlayerSkills loadPlayerSkills(Player p) {

        int strengthLevel = new Integer(0);
        int attackSpeedLevel = new Integer(0);
        int critLevel = new Integer(0);
        int archerLevel = new Integer(0);
        int hearthLevel = new Integer(0);
        int defenseLevel = new Integer(0);
        int toSpend = new Integer(0);

        ResultSet rs = RPG.SQL.mysql.query("SELECT * FROM `player_skills` WHERE uuid ='" + p.getUniqueId().toString() + "'");
        try {
            while (rs.next()) {

                strengthLevel = rs.getInt("strength");
                attackSpeedLevel = rs.getInt("speed");
                critLevel = rs.getInt("crit");
                archerLevel = rs.getInt("archery");
                hearthLevel = rs.getInt("hearts");
                defenseLevel = rs.getInt("defense");
                toSpend = rs.getInt("tospend");

            }
        } catch (SQLException ex) {
            RPG.getPlugin().getLogger().info("Error executing a query: " + ex.getErrorCode());
        }


        return new PlayerSkills(strengthLevel, attackSpeedLevel, critLevel, archerLevel, hearthLevel, defenseLevel, toSpend);
    }

    public static PlayerLevel loadPlayerLevel(Player p) {

        int experience = 43;

        ResultSet rs = RPG.SQL.mysql.query("SELECT * FROM `player_xp` WHERE uuid ='" + p.getUniqueId().toString() + "'");
        try {
            while (rs.next()) {

                experience = rs.getInt("total_xp");

            }
        } catch (SQLException ex) {
            RPG.getPlugin().getLogger().info("Error executing a query: " + ex.getErrorCode());
        }

        return new PlayerLevel(experience);
    }

    private static void savePlayerSkillsToDB(Player p) {

        PlayerSkills ps = RPGPlayer.get(p).getPs();
        String pUUID = p.getUniqueId().toString();
        RPG.SQL.mysql.update("UPDATE player_skills SET strength = " + ps.getStrengthLevel() + " WHERE uuid='" + pUUID + "';");
        RPG.SQL.mysql.update("UPDATE player_skills SET speed = " + ps.getSpeedLevel() + " WHERE uuid='" + pUUID + "';");
        RPG.SQL.mysql.update("UPDATE player_skills SET crit = " + ps.getCritLevel() + " WHERE uuid='" + pUUID + "';");
        RPG.SQL.mysql.update("UPDATE player_skills SET archery = " + ps.getBowLevel() + " WHERE uuid='" + pUUID + "';");
        RPG.SQL.mysql.update("UPDATE player_skills SET hearts = " + ps.getHealthLevel() + " WHERE uuid='" + pUUID + "';");
        RPG.SQL.mysql.update("UPDATE player_skills SET defense = " + ps.getDefenseLevel() + " WHERE uuid='" + pUUID + "';");
        RPG.SQL.mysql.update("UPDATE player_skills SET tospend = " + ps.getToSpend() + " WHERE uuid='" + pUUID + "';");

    }

    private static void savePlayerLevelToDB(Player p) {
        PlayerLevel pl = RPGPlayer.get(p).getPl();
        RPG.SQL.mysql.update("UPDATE player_xp SET total_xp = " + pl.getExperience() + " WHERE uuid='" + p.getUniqueId().toString() + "';");
    }

    private static String itemStackToString(ItemStack item) {


        ItemStack[] isArray = new ItemStack[1];
        isArray[0] = item;

        return ItemStackBase.itemStackArrayToBase64(isArray);
    }

    private static void savePlayerEquipmentToDB(Player p) {
        PlayerEquipment pe = RPGPlayer.get(p).getPeq();
        StringBuilder updateQuery = new StringBuilder("UPDATE  player_equipment SET ");
        updateQuery.append(" combat_boots = '").append(itemStackToString(pe.getCombat_boots())).append("'");
        updateQuery.append(", combat_leggings='").append(itemStackToString(pe.getCombat_leggings())).append("'");
        updateQuery.append(", combat_chestplate='").append(itemStackToString(pe.getCombat_chestplate())).append("'");
        updateQuery.append(", combat_helmet='").append(itemStackToString(pe.getCombat_helmet())).append("'");

        updateQuery.append(", earring1='").append(itemStackToString(pe.getEarring1())).append("'");
        updateQuery.append(", necklace='").append(itemStackToString(pe.getNecklace())).append("'");
        updateQuery.append(", earring2='").append(itemStackToString(pe.getEarring2())).append("'");

        updateQuery.append(", ring1='").append(itemStackToString(pe.getRing1())).append("'");
        updateQuery.append(", scroll='").append(itemStackToString(pe.getScroll())).append("'");
        updateQuery.append(", ring2='").append(itemStackToString(pe.getRing2())).append("'");

        updateQuery.append(", cosmetic_boots='").append(itemStackToString(pe.getCosmetic_boots())).append("'");
        updateQuery.append(", cosmetic_leggings='").append(itemStackToString(pe.getCosmetic_leggings())).append("'");
        updateQuery.append(", cosmetic_chestplate='").append(itemStackToString(pe.getCosmetic_chestplate())).append("'");
        updateQuery.append(", cosmetic_helmet='").append(itemStackToString(pe.getCosmetic_helmet())).append("'");

        updateQuery.append(" WHERE uuid='").append(p.getUniqueId().toString()).append("'");

        MysqlConnection mysqlConnection = new MysqlConnection();
        Connection conn = mysqlConnection.getConnection("localhost", "dev_server?autoReconnect=true&useSSL=false", "robin", "TXGmysqlTXG1");
        mysqlConnection.updateQuery(conn, updateQuery.toString());

    }

    private static void savePlayerBountiesToDB(Player p) {
        PlayerBounties pb = RPGPlayer.get(p).getPb();
        String bountyTypes = new String("");
        for (int bountyID : pb.getBountyIDS()) {
            bountyTypes += bountyID + ",";
        }
        bountyTypes = StringUtils.substringBeforeLast(bountyTypes, ",");
        String bountyProgress = new String("");
        for (int progress : pb.getBountyProgress()) {
            bountyProgress += progress + ",";
        }
        bountyProgress = StringUtils.substringBeforeLast(bountyProgress, ",");

        RPG.SQL.mysql.update("UPDATE player_bounties SET bounty_types = '" + bountyTypes + "' WHERE uuid='" + p.getUniqueId().toString() + "';");
        RPG.SQL.mysql.update("UPDATE player_bounties SET bounty_progress = '" + bountyProgress + "' WHERE uuid='" + p.getUniqueId().toString() + "';");
    }

    public static void saveRPGPlayerToDB(Player p) {
        savePlayerSkillsToDB(p);
        savePlayerLevelToDB(p);
        savePlayerEquipmentToDB(p);
        savePlayerBountiesToDB(p);
        RPGPlayer.remove(p);
    }

    public static void saveAllRPGPlayerToDB() {
        all().keySet().forEach(p -> {
            saveRPGPlayerToDB(p);
        });
    }

    public static void createDefaultStats(Player p) {

        if (!p.hasPlayedBefore()) {
            long systime = System.currentTimeMillis();
  /*          MysqlConnection mysqlConnection = new MysqlConnection();
            Connection conn = mysqlConnection.getConnection("localhost", "dev_server?autoReconnect=true&useSSL=false","robin", "TXGmysqlTXG1");
            mysqlConnection.insertPlayerXp(conn, p);
            mysqlConnection.insertPlayerSkills(conn, p);
            mysqlConnection.insertPlayerEquipment(conn, p);
            mysqlConnection.insertPlayerBounties(conn, p); */

            RPG.SQL.mysql.update("INSERT INTO player_xp (uuid, total_xp)" +
                    "VALUES ('" + p.getUniqueId().toString() + "', " + PlayerLevel.getXpOfLevel(1) + ")");

            RPG.SQL.mysql.update("INSERT INTO player_skills (uuid, strength, speed, crit, archery, hearts, defense, tospend) " +
                    "VALUES ('" + p.getUniqueId().toString() + "', 1, 1, 1, 1, 1, 1, 0)");

            RPG.SQL.mysql.update("INSERT INTO player_equipment (uuid, combat_boots, combat_leggings, combat_chestplate, combat_helmet, ring1, ring2, necklace, scroll, earring1, earring2, cosmetic_boots, cosmetic_leggings, cosmetic_chestplate, cosmetic_helmet) " +
                    "VALUES ('" + p.getUniqueId().toString() + "', '', '', '', '', '', '', '', '', '', '', '', '', '', '')");

            RPG.SQL.mysql.update("INSERT INTO player_bounties (uuid, bounty_types, bounty_progress) VALUES ('" + p.getUniqueId().toString() + "', '', '')");

            Bukkit.getLogger().log(Level.INFO, "Execution time" + (System.currentTimeMillis() - systime));
        }


    }

}

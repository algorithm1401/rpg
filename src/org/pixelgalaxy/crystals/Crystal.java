package org.pixelgalaxy.crystals;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.pixelgalaxy.RPG;
import org.pixelgalaxy.equipment.Rarity;

import java.io.File;
import java.util.Arrays;

/**
 * Created by robin on 17/08/2018.
 */
public class Crystal {

    public static ItemStack createCrystal(Rarity r, int amount){

        ItemStack crystal = new ItemStack(Material.NETHER_STAR, amount);
        ItemMeta crystalM = crystal.getItemMeta();
        crystalM.setDisplayName(r.getName() + " Crystal");
        crystalM.setLore(Arrays.asList(

                "§7Rarity: " + r.getName(),
                "§6Bring your Crystals",
                "§6to Novis who is",
                "§6found at spawn"

        ));
        crystal.setItemMeta(crystalM);

        return crystal;

    }

    public static void giveCrystal(Player p, Rarity r, int amount){

        p.getInventory().addItem(createCrystal(r, amount));
        p.sendMessage(RPG.getPrefix() + "§aYou have received §6" + amount + " " + r.getName() + " Crystals§a!");

    }

}

package org.pixelgalaxy.crystals;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.pixelgalaxy.RPG;
import org.pixelgalaxy.equipment.Equipment;
import org.pixelgalaxy.equipment.Rarity;
import org.pixelgalaxy.level.XP;
import org.pixelgalaxy.utils.Chance;
import org.pixelgalaxy.utils.FloatingItem;
import org.pixelgalaxy.utils.configSavers;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by robin on 25/08/2018.
 */
public class Opener implements Listener {

    public static final int PORTAL_IN_RADIUS = RPG.getPlugin().getConfig().getInt("portal_in_radius");
    public static List<ItemStack> crystalItemstacks = Arrays.asList(Crystal.createCrystal(Rarity.COMMON, 1), Crystal.createCrystal(Rarity.RARE, 1), Crystal.createCrystal(Rarity.EPIC, 1), Crystal.createCrystal(Rarity.LEGENDARY, 1));

    public static List<Player> openingCrystal = new ArrayList<>();

    private static double MAX_COMMON_NUMBER;
    private static double MAX_RARE_NUMBER;
    private static double MAX_EPIC_NUMBER;
    private static double MAX_LEGENDARY_NUMBER;

    public static File file;
    public static FileConfiguration crystalFile;
    private static String crystalPath = "rewards.yml";

    public static FileConfiguration getCrystalFile() {
        if (crystalFile == null) crystalFile = YamlConfiguration.loadConfiguration(getFile());
        return crystalFile;
    }

    private static File getFile() {
        if (file == null) {
            RPG.getPlugin().saveResource(crystalPath, false);
            file = new File(RPG.getPlugin().getDataFolder(), crystalPath);
        }
        return file;
    }

    public static void initMaxValues() {

        double common = 0;
        double rare = 0;
        double epic = 0;
        double legendary = 0;

        for (String s : getCrystalFile().getKeys(true)) {

            Bukkit.getServer().getLogger().info("S: " + s);

            for (String loreLine : getCrystalFile().getStringList(s)) {

                Bukkit.getServer().getLogger().info("Loreline: " + loreLine);

                double parsed = Double.valueOf(StringUtils.substringAfterLast(loreLine, " "));

                if (s.contains("COMMON")) {

                    common += parsed;

                } else if (s.contains("RARE")) {

                    rare += parsed;

                } else if (s.contains("EPIC")) {

                    epic += parsed;

                } else if (s.contains("LEGENDARY")) {

                    legendary += parsed;

                }

            }

        }

        MAX_COMMON_NUMBER = common;
        MAX_RARE_NUMBER = rare;
        MAX_EPIC_NUMBER = epic;
        MAX_LEGENDARY_NUMBER = legendary;

        Bukkit.getServer().getLogger().info("Max values opener " + MAX_COMMON_NUMBER + ", " + MAX_RARE_NUMBER + ", " + MAX_EPIC_NUMBER + ", " + MAX_LEGENDARY_NUMBER);

    }

    private static double getMaxValueForRarity(Rarity r) {

        switch (r) {

            case COMMON:
                return MAX_COMMON_NUMBER;

            case RARE:
                return MAX_RARE_NUMBER;

            case EPIC:
                return MAX_EPIC_NUMBER;

            case LEGENDARY:
                return MAX_LEGENDARY_NUMBER;

        }
        return 0.0;
    }

    public static void giveRandomReward(Player p, Rarity r) {

        double randomChance = Chance.getRandom(0, (int) (getMaxValueForRarity(r) * 100)) / 100;

        for (String s : getCrystalFile().getKeys(true)) {

            for (String item : getCrystalFile().getStringList(s)) {

                if (s.equalsIgnoreCase(ChatColor.stripColor(r.getName()))) {

                    double chance = Double.valueOf(StringUtils.substringAfterLast(item, " "));
                    randomChance -= chance;
                    if (randomChance <= 0) {

                        String type = StringUtils.substringBefore(item, ";");
                        switch (type) {

                            case "rpg":

                                String itemName = StringUtils.substringBetween(item, ";", ",");
                                itemName = itemName.toLowerCase();
                                ItemStack toAdd;
                                if (!itemName.contains("xp")) {
                                    toAdd = new Equipment(itemName, 1).build();
                                } else {
                                    int xpAmount = Integer.valueOf(StringUtils.substringBefore(itemName, "xp"));
                                    toAdd = XP.getBottle(xpAmount);
                                }
                                p.getInventory().addItem(toAdd);
                                p.sendMessage(RPG.getPrefix() + "§aYou have received a: " + toAdd.getItemMeta().getDisplayName());

                                break;

                            case "vault":

                                double amount = Double.valueOf(StringUtils.substringBetween(item, ";", ","));
                                RPG.getEco().depositPlayer(p, amount);
                                p.sendMessage(RPG.getPrefix() + "§aYou have received " + amount + "$ from opening a crystal!");

                                break;

                            case "pex":

                                Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), "/voucher give challenger 1 " + p.getName());
                                p.sendMessage(RPG.getPrefix() + "§aYou have received a challenger rank from opening a crystal!");
                                Bukkit.broadcastMessage(RPG.getPrefix() + "§6§lCongratulations to §a§l" + p.getCustomName() + " §6§lfor getting a §e§lChallenger §6§lrank out of a legendary crystal!");

                                break;

                            case "crystal":

                                String crystalR = StringUtils.substringBetween(item, ";", ",");
                                int crystalAmount = Integer.valueOf(StringUtils.substringBetween(item, crystalR.toLowerCase() + ", ", ","));
                                Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "rpg crystal " + p.getName() + " " + crystalR + " " + crystalAmount);
                                p.sendMessage(RPG.getPrefix() + "§aYou have received §6" + crystalAmount + " " + Rarity.valueOf(crystalR).getName() + " §acrystals!");

                                break;

                        }
                        break;
                    }
                }

            }

        }

    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {

        Player p = e.getPlayer();
        ItemStack inhand = p.getInventory().getItemInMainHand();

        boolean isCrystal = false;
        ItemStack toRemove = null;

        for (ItemStack crystal : crystalItemstacks) {

            crystal.setAmount(inhand.getAmount());
            if (crystal.equals(inhand)) {
                isCrystal = true;
                crystal.setAmount(1);
                toRemove = crystal;
            }

        }

        if (isCrystal) {

            if (!openingCrystal.contains(p)) {

                if (p.getWorld().getNearbyEntities(configSavers.getLocation("portal_location.5"), PORTAL_IN_RADIUS, PORTAL_IN_RADIUS, PORTAL_IN_RADIUS).contains(p)) {

                    String loreline = inhand.getItemMeta().getLore().get(0);
                    Rarity r = Rarity.valueOf(StringUtils.substringAfter(ChatColor.stripColor(loreline), "Rarity: ").toUpperCase());

                    p.getInventory().removeItem(toRemove);

                    openingCrystal.add(p);

                    FloatingItem.create(p.getLocation(), toRemove, p, r);

                }
            } else {

                p.sendMessage(RPG.getPrefix() + "§cYou can only open 1 crystal at a time!");

            }
        }

    }

}

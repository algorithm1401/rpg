package org.pixelgalaxy.abilities;

import org.bukkit.Material;
import org.pixelgalaxy.skills.SkillType;

import java.util.Arrays;
import java.util.List;

/**
 * Created by robin on 13/08/2018.
 */
public enum AbilityType {

    DECIMATE(25, Material.IRON_SWORD, "Decimate", "§e", Arrays.asList("§7Deal 1.5X damage when", "§7you right-left click"), SkillType.STRENGTH, 19, "§aYou §a§lDECIMATED §e"),
    SPRINTER(25, Material.FEATHER, "Sprinter", "§a", Arrays.asList("§7Increases walking speed by 20%"), SkillType.ATTACK_SPEED, 20, ""),
    SUPERCRITICAL(25, Material.BLAZE_POWDER, "Supercritical", "§9", Arrays.asList("§7If you do a critical", "§7your hit you do next will have", "§7double the chance to crit also!"), SkillType.CRITICAL_CHANCE, 21, ""),
    PIERCING_ARROW(25, Material.BOW, "Piercing Arrow", "§6", Arrays.asList("§7Shoot an arrow with double", "§7the damage when", "§7you left-right click"), SkillType.ARCHERY, 23, "§ePiercing shot!"),
    ABSORBTION(25, Material.APPLE, "Absorbtion", "§d", Arrays.asList("§7When your health reaches 10%", "§7you will steal health on", "§7each hit for 5 seconds"), SkillType.HEARTS, 24, ""),
    BLOCKAGE(25, Material.IRON_CHESTPLATE, "Blockage", "§b", Arrays.asList("§710% chance to block hits"), SkillType.DEFENCE, 25, "§eBlocked!");

    private int unlockLevel;
    private Material material;
    private String name;
    private String color;
    private List<String> description;
    private SkillType skillType;
    private int slot;
    private String messageOnUse;

    AbilityType(int unlockLevel, Material material, String name, String color, List<String> description, SkillType skillType, int slot, String messageOnUse) {
        this.unlockLevel = unlockLevel;
        this.material = material;
        this.name = name;
        this.color = color;
        this.description = description;
        this.skillType = skillType;
        this.slot = slot;
        this.messageOnUse = messageOnUse;
    }

    public int getUnlockLevel() {
        return unlockLevel;
    }

    public Material getMaterial() {
        return material;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

    public List<String> getDescription() {
        return description;
    }

    public SkillType getSkillType() {
        return skillType;
    }

    public int getSlot() {
        return slot;
    }

    public String getMessageOnUse() {
        return messageOnUse;
    }

}

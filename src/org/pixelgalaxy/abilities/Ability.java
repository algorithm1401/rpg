package org.pixelgalaxy.abilities;

import org.bukkit.entity.Player;
import org.pixelgalaxy.RPGPlayer;
import org.pixelgalaxy.player_objects.PlayerSkills;
import org.pixelgalaxy.utils.Chance;

/**
 * Created by robin on 13/08/2018.
 */
public class Ability {

    public static boolean ifBlockNotApplies(Player target) {

        if (Chance.chance(90)) {
            return true;
        } else {
            target.sendMessage(AbilityType.BLOCKAGE.getMessageOnUse());
        }

        return false;
    }

    public static boolean isUnlocked(Player p, AbilityType abilityType){

        RPGPlayer rpgPlayer = RPGPlayer.get(p);
        PlayerSkills ps = rpgPlayer.getPs();

        switch(abilityType){

            case DECIMATE:

                if (ps.getStrengthLevel() >= abilityType.getUnlockLevel()) {
                    return true;
                }

                return false;

            case SPRINTER:

                if (ps.getSpeedLevel() >= abilityType.getUnlockLevel()) {
                    return true;
                }

                return false;

            case SUPERCRITICAL:

                if (ps.getCritLevel() >= abilityType.getUnlockLevel()) {
                    return true;
                }

                return false;

            case PIERCING_ARROW:

                if (ps.getBowLevel() >= abilityType.getUnlockLevel()) {
                    return true;
                }

                return false;

            case ABSORBTION:

                if (ps.getHealthLevel() >= abilityType.getUnlockLevel()) {
                    return true;
                }

                return false;

            case BLOCKAGE:

                if (ps.getDefenseLevel() >= abilityType.getUnlockLevel()) {
                    return true;
                }

                return false;


        }

        return false;
    }

}

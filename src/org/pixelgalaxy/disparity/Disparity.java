package org.pixelgalaxy.disparity;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.pixelgalaxy.RPG;
import org.pixelgalaxy.player_objects.PlayerLevel;
import org.pixelgalaxy.utils.Chance;
import org.pixelgalaxy.utils.WGRegion;
import org.pixelgalaxy.utils.configSavers;

import java.io.File;
import java.util.*;

/**
 * Created by robin on 21/09/2018.
 */
public class Disparity implements Listener {

    private static Map<Player, BossBar> playerDisparityBarMap = new HashMap<>();
    private static List<Player> inToggleMode = new ArrayList<>();

    public static File file;
    public static FileConfiguration chestsFile;
    private static String chestPath = "chests.yml";

    private static int maxItemReward;

    public static void initRewardChances() {

        maxItemReward = 0;

        for (String s : getChestsFile().getKeys(true)) {

            if (s.contains("rewards") && s.contains("chance") && StringUtils.countMatches(s, ".") == 2) {

                double chance = Double.valueOf(getChestsFile().getDouble(s));
                maxItemReward += chance;

            }

        }

        ChestUpdater.forceUpdate();

    }

    public static FileConfiguration getChestsFile() {
        if (chestsFile == null) chestsFile = YamlConfiguration.loadConfiguration(getFile());
        return chestsFile;
    }

    public static File getFile() {
        if (file == null) {
            RPG.getPlugin().saveResource(chestPath, false);
            file = new File(RPG.getPlugin().getDataFolder(), chestPath);
        }
        return file;
    }

    public static void setToggleMode(Player p) {

        if (inToggleMode.contains(p)) {
            inToggleMode.remove(p);
            p.sendMessage(RPG.getPrefix() + "§aTogglemode has been disabled");
        } else {
            inToggleMode.add(p);
            p.sendMessage(RPG.getPrefix() + "§aTogglemode has been enabled");
        }

    }

    public static int getDisparity(Location playerLoc) {

        Location spawnLoc = configSavers.getLocation("spawn_location");
        double distance = getDistanceBetweenLocs(spawnLoc, playerLoc);

        int disparity = (int) (distance / 16.0);
        disparity -= (int) (130.0 / 16.0);

        if (disparity > PlayerLevel.MAX_LEVEL) {
            disparity = PlayerLevel.MAX_LEVEL;
        } else if (disparity == 0) {
            disparity = 1;
        }

        return disparity;
    }

    public static int getDistanceBetweenLocs(Location startLoc, Location secondLoc) {

        int distance = (int) startLoc.distance(secondLoc);

        return distance;
    }

    public static BarColor getBarColorFromDisparity(int disparity) {

        if (disparity < 20) {
            return BarColor.GREEN;
        } else if (disparity >= 20 && disparity < 40) {
            return BarColor.YELLOW;
        } else if (disparity >= 40 && disparity < 50) {
            return BarColor.RED;
        } else {
            return BarColor.PURPLE;
        }

    }

    public static void updateDisparityBar(Player p) {
        int disparity = getDisparity(p.getLocation());

        double progress = disparity / 65.0;

        if (!playerDisparityBarMap.containsKey(p) && progress >= 0 && progress <= 1 && !WGRegion.playerInRegion(p, "spawn")) {
            BossBar bossBar = Bukkit.createBossBar("§7Level Disparity: §e" + disparity, getBarColorFromDisparity(disparity), BarStyle.SOLID
            );
            bossBar.setProgress(progress);
            bossBar.addPlayer(p);
            playerDisparityBarMap.put(p, bossBar);
        }

        if (disparity > 0 && !WGRegion.playerInRegion(p, "spawn")) {

            playerDisparityBarMap.get(p).setTitle("§7Level Disparity: §e" + disparity);
            playerDisparityBarMap.get(p).setColor(getBarColorFromDisparity(disparity));
            playerDisparityBarMap.get(p).setProgress(progress);

        } else if (playerDisparityBarMap.containsKey(p)) {
            playerDisparityBarMap.get(p).removePlayer(p);
            playerDisparityBarMap.remove(p);
        }

    }

    @EventHandler
    public void onMove(PlayerMoveEvent e) {

        Player p = e.getPlayer();
        updateDisparityBar(p);

    }

    public static List<ItemStack> getChestRewards(Location loc) {

        List<ItemStack> chestRewards = new ArrayList<>();

        int amountOfItems = Chance.getRandom(0, 100);
        int disparityOfChest = getDisparity(loc);
        double dispPercentage = disparityOfChest / 65.0;

        double firstThreshold = 90 - (20 * dispPercentage);
        double secondThreshold = 60 - (20 * dispPercentage);

        if (amountOfItems >= firstThreshold) {
            amountOfItems = 5;
        } else if (amountOfItems >= secondThreshold) {
            amountOfItems = 4;
        } else {
            amountOfItems = 3;
        }

        int maxReward = getMaxRewardNumber();

        for (int i = 0; i < amountOfItems; i++) {

            double randomItemChance = (double) Chance.getRandom(0, maxItemReward);

            for (int rewardID = 1; rewardID < maxReward + 1; rewardID++) {

                String path = "rewards." + rewardID + ".chance";
                double toRemove = getChestsFile().getDouble(path);
                randomItemChance -= toRemove;

                if (randomItemChance <= 0) {

                    chestRewards.add(getChestsFile().getItemStack("rewards." + rewardID + ".item"));
                    break;
                }

            }

        }

        return chestRewards;
    }

    public static int getMaxRewardNumber() {
        int reward = new Integer(0);
        for (String s : getChestsFile().getKeys(true)) {

            if (StringUtils.countMatches(s, ".") == 1 && s.contains("rewards")) {

                int maxReward = Integer.valueOf(StringUtils.substringAfter(s, "."));
                if (reward < maxReward) {
                    reward = maxReward;
                }

            }

        }
        return reward;
    }

    public static int getMaxChestNumber() {
        int max = new Integer(0);
        for (String s : getChestsFile().getKeys(true)) {

            if (StringUtils.countMatches(s, ".") == 1 && s.contains("chests")) {

                int maxChest = Integer.valueOf(StringUtils.substringAfter(s, "."));
                if (max < maxChest) {
                    max = maxChest;
                }

            }

        }
        return max;
    }

    private static void saveChest(int chest, Location loc) {

        String path = "chests." + (chest + 1);
        getChestsFile().set(path + ".x", loc.getX());
        getChestsFile().set(path + ".y", loc.getY());
        getChestsFile().set(path + ".z", loc.getZ());
        try {
            getChestsFile().save(getFile());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public static Location getChestLocation(int number) {

        String path = "chests." + number;
        double x = getChestsFile().getDouble(path + ".x");
        double y = getChestsFile().getDouble(path + ".y");
        double z = getChestsFile().getDouble(path + ".z");
        Location loc = new Location(Bukkit.getServer().getWorld(getChestsFile().getString("world")), x, y, z);

        return loc;
    }

    private static Inventory rewardInventory;
    private static Inventory chanceInventory;
    private static int currentItem;
    private static ItemStack currentItemStack;

    public static void openRewardsEditor(Player p) {

        rewardInventory = Bukkit.createInventory(null, 54, "§6§lChest rewards editor");
        for (int i = 1; i < Disparity.getMaxRewardNumber() + 1; i++) {

            rewardInventory.setItem(i - 1, Disparity.getChestsFile().getItemStack("rewards." + i + ".item"));

        }

        p.openInventory(rewardInventory);

    }

    private static ItemStack getChanceChangerStack(int amount) {

        ItemStack pane = new ItemStack(Material.STAINED_GLASS_PANE);
        String display = "§6Change chance by: ";
        if (amount > 0) {
            pane.setDurability((short) 5);
            display += "§a" + amount;
        } else {
            pane.setDurability((short) 14);
            display += "§c" + amount;
        }
        ItemMeta panem = pane.getItemMeta();
        panem.setDisplayName(display);
        pane.setItemMeta(panem);

        return pane;
    }

    private static ItemStack getBackStack() {
        ItemStack is = new ItemStack(Material.BARRIER);
        ItemMeta ism = is.getItemMeta();
        ism.setDisplayName("§cGo back to the menu");
        is.setItemMeta(ism);
        return is;
    }

    private static ItemStack noStack() {

        ItemStack nostack = new ItemStack(Material.STAINED_GLASS_PANE);
        nostack.setDurability((short) 15);
        ItemMeta nostackm = nostack.getItemMeta();
        nostackm.setDisplayName("§7Change item chance");
        nostack.setItemMeta(nostackm);

        return nostack;
    }

    public static Map<Integer, Integer> chanceWithSlot;

    static {

        chanceWithSlot = new HashMap<Integer, Integer>();

        chanceWithSlot.put(10, 1);
        chanceWithSlot.put(11, 5);
        chanceWithSlot.put(12, 10);
        chanceWithSlot.put(14, -10);
        chanceWithSlot.put(15, -5);
        chanceWithSlot.put(16, -1);
    }

    public static void openChanceInventory(Player p, ItemStack is) {

        chanceInventory = Bukkit.createInventory(null, 27, "§6§lRewards chance editor");

        List<Integer> addSlots = Arrays.asList(10, 11, 12, 14, 15, 16);

        ItemMeta ism = is.getItemMeta();
        ism.setLore(Arrays.asList("§7Current chance: §6" + Disparity.getChestsFile().getInt("rewards." + currentItem + ".chance")));
        is.setItemMeta(ism);

        for (int i = 0; i < chanceInventory.getSize(); i++) {

            if (addSlots.contains(i)) {
                chanceInventory.setItem(i, getChanceChangerStack(chanceWithSlot.get(i)));
            } else if (i == 22) {
                chanceInventory.setItem(i, getBackStack());
            } else if (i == 13) {
                chanceInventory.setItem(i, is);
            } else {
                chanceInventory.setItem(i, noStack());
            }

        }

        currentItemStack = is;

        p.openInventory(chanceInventory);
    }

    public static void addReward(ItemStack placed, Player p) {
        String path = "rewards." + (Disparity.getMaxRewardNumber() + 1);
        Disparity.getChestsFile().set(path + ".item", placed);
        Disparity.getChestsFile().set(path + ".chance", 50);
        try {
            Disparity.getChestsFile().save(Disparity.getFile());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        p.sendMessage(RPG.getPrefix() + "§aYou have succesfully added " + placed.getItemMeta().getDisplayName() + " §ato the chest rewards!");
    }

    @EventHandler
    public void invClick(InventoryClickEvent e) {

        Player p = (Player) e.getWhoClicked();

        if (e.getClickedInventory().equals(rewardInventory)) {

            if (e.getAction().equals(InventoryAction.PICKUP_ALL)) {

                currentItem = e.getSlot() + 1;
                openChanceInventory(p, e.getCurrentItem());
                e.setCancelled(true);

            } else if (e.getAction().equals(InventoryAction.PLACE_ONE) || (e.getAction().equals(InventoryAction.PLACE_ALL) || (e.getAction().equals(InventoryAction.PLACE_SOME)))) {

                ItemStack cursor = p.getItemOnCursor();
                e.setCancelled(true);
                e.getInventory().setItem(e.getSlot(), cursor);
                addReward(cursor, p);
                p.setItemOnCursor(null);

            } else {
                e.setCancelled(true);
            }

        } else if (e.getClickedInventory().equals(chanceInventory)) {

            if (e.getCurrentItem().getType() == Material.BARRIER) {
                e.setCancelled(true);
                p.closeInventory();
                openRewardsEditor(p);

            } else if (e.getCurrentItem().getType() == Material.STAINED_GLASS_PANE) {

                double chance = Disparity.getChestsFile().getInt("rewards." + currentItem + ".chance");
                int toAdd = chanceWithSlot.get(e.getSlot());
                chance += toAdd;
                Disparity.getChestsFile().set("rewards." + currentItem + ".chance", chance);
                try {
                    Disparity.getChestsFile().save(Disparity.getFile());
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                e.setCancelled(true);
                openChanceInventory(p, currentItemStack);
            } else {
                e.setCancelled(true);
            }

        }

    }

    @EventHandler
    public void onClick(PlayerInteractEvent e) {

        Player p = e.getPlayer();

        if (e.getClickedBlock() != null) {

            if (e.getClickedBlock().getType().equals(Material.CHEST) && inToggleMode.contains(p)) {

                e.setCancelled(true);
                saveChest(getMaxChestNumber(), e.getClickedBlock().getLocation());
                p.sendMessage(RPG.getPrefix() + "§aYou have succesfully added the clicked chest to the config!");

            }

        }
    }

}

package org.pixelgalaxy.disparity;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.pixelgalaxy.RPG;
import org.pixelgalaxy.npcs.Bounties;
import org.pixelgalaxy.npcs.BountyTriggers;
import org.pixelgalaxy.utils.Chance;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Created by robin on 23/09/2018.
 */
public class ChestUpdater extends BukkitRunnable {

    private static List<ChestUpdater> active = new ArrayList<>();

    public static List<ChestUpdater> getActive() {
        return active;
    }

    public static void forceUpdate() {

        for (ChestUpdater chestUpdater : active) {
            chestUpdater.cancel();
            active.remove(chestUpdater);
        }

        for (int i = 1; i < Disparity.getMaxChestNumber() + 1; i++) {

            ChestUpdater chestUpdater = new ChestUpdater(Disparity.getChestLocation(i), Chance.getRandom(Disparity.getChestsFile().getInt("minRefresh"), Disparity.getChestsFile().getInt("maxRefresh")));
            chestUpdater.runTaskTimer(RPG.getPlugin(), 0, 20);
            ChestUpdater.getActive().add(chestUpdater);

        }

    }

    public Location getLoc() {
        return loc;
    }

    private Location loc;
    private int updateTime;

    public ChestUpdater(Location loc, int updateTime) {
        this.loc = loc;
        this.updateTime = updateTime;
    }

    @Override
    public void run() {

        if (updateTime != 0) {
            updateTime--;
            World w = Bukkit.getServer().getWorld(loc.getWorld().getName());
            Block bl = w.getBlockAt(loc);
            Chest c = (Chest) bl.getState();

            c.setCustomName("§c§lLoot chest: §6§l" + updateTime + " sec left");

        } else {

            World w = Bukkit.getServer().getWorld(loc.getWorld().getName());
            Block bl = w.getBlockAt(loc);
            Chest c = (Chest) bl.getState();

            c.setCustomName("§c§lLoot chest");
            c.getBlockInventory().clear();

            List<Integer> alreadyTaken = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8,
                    9, 10, 11, 12, 13, 14, 15, 16, 17,
                    18, 19, 20, 21, 22, 23, 24, 25, 26);

            List<Integer> hasTaken = new ArrayList<>();
            for (int i : alreadyTaken) {
                hasTaken.add(i);
            }

            List<ItemStack> chestRewards = Disparity.getChestRewards(loc);

            for (int i = 0; i < chestRewards.size(); i++) {

                int takenPlace = Chance.getRandom(0, hasTaken.size() - 1);
                int slot = hasTaken.get(takenPlace);
                hasTaken.remove(takenPlace);
                c.getBlockInventory().setItem(slot, chestRewards.get(i));
            }

            c.update();

            active.remove(this);
            ChestUpdater chestUpdater = new ChestUpdater(loc, Chance.getRandom(Disparity.getChestsFile().getInt("minRefresh"), Disparity.getChestsFile().getInt("maxRefresh")));
            chestUpdater.runTaskTimer(RPG.getPlugin(), 0, 20);
            active.add(chestUpdater);

            BountyTriggers.lootedChests.remove(loc);

            this.cancel();
        }

    }
}

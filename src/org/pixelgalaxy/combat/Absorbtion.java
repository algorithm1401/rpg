package org.pixelgalaxy.combat;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.pixelgalaxy.abilities.Ability;
import org.pixelgalaxy.abilities.AbilityType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by robin on 15/08/2018.
 */
public class Absorbtion implements Listener {

    public static Map<Player, Long> lifeStealing = new HashMap<>();

    public static List<Player> under10 = new ArrayList<>();

    public static double roundToHalf(double d) {
        return Math.round(d * 2) / 2.0;
    }

    @EventHandler (priority = EventPriority.HIGH)
    public void onHit(EntityDamageByEntityEvent e){

        if(e.getEntity() instanceof Player){

            Player p = (Player) e.getEntity();

            if(Ability.isUnlocked(p, AbilityType.ABSORBTION)){

                if(p.getHealth() <= (p.getMaxHealth() / 10)){

                    if(!lifeStealing.containsKey(p) && !under10.contains(p)) {

                        lifeStealing.put(p, System.currentTimeMillis());
                        under10.add(p);

                    }

                }else{
                    under10.remove(p);
                }

            }

        }

        if(e.getDamager() instanceof Player){

            Player p = (Player) e.getDamager();

            if(lifeStealing.containsKey(p)){

                Long diff = System.currentTimeMillis() - lifeStealing.get(p);

                if(diff <= 5000){

                    p.setHealth(p.getHealth() + (e.getDamage() / 2));
                    p.sendMessage("§aYou stole " + roundToHalf(e.getDamage()/2) + "§c❤§a!");

                }else{

                    lifeStealing.remove(p);

                }

            }

        }

    }

}

package org.pixelgalaxy.combat.melee;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.pixelgalaxy.RPG;
import org.pixelgalaxy.RPGPlayer;
import org.pixelgalaxy.abilities.Ability;
import org.pixelgalaxy.abilities.AbilityType;
import org.pixelgalaxy.combat.Speed;
import org.pixelgalaxy.disparity.Disparity;
import org.pixelgalaxy.equipment.Equipment;
import org.pixelgalaxy.events.onInteract;
import org.pixelgalaxy.player_objects.PlayerCombat;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by robin on 4/09/2018.
 */
public class PlayerMeleeHit implements Listener {

    private static Map<Player, String> lastHit = new HashMap<>();

    public static void removeLastHit(Player p) {
        if (lastHit.containsKey(p)) {
            lastHit.remove(p);
        }
    }

    public static Player getPlayerFromHitEntity(LivingEntity en) {

        for (Player p : lastHit.keySet()) {

            if (lastHit.get(p).equals(en.getUniqueId().toString())) {

                return p;

            }

        }
        return null;
    }

    private static void damageTarget(Player p, LivingEntity len, double damageToDo) {
        removeLastHit(p);
        lastHit.put(p, len.getUniqueId().toString());

        len.damage(damageToDo);
        PlayerCombat.applyKnockBack(p, len, Speed.getPercentCharged(p));
        Speed.startCharge(p.getInventory().getItemInMainHand(), p);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onHit(EntityDamageByEntityEvent e) {

        Entity en = e.getEntity();

        if (e.getDamager() instanceof Player && en instanceof LivingEntity) {

            LivingEntity len = (LivingEntity) en;
            Player p = (Player) e.getDamager();

            if (p.getInventory().getItemInMainHand() != null) {

                ItemStack inhand = p.getInventory().getItemInMainHand();

                if (Equipment.isMelee(inhand)) {

                    e.setCancelled(true);

                    if (RPGPlayer.get(p).getPl().getPlayerLevel() >= onInteract.getRequiredLevel(inhand)) {

                        double damageToDo = PlayerCombat.getTotalRawMeleeDMG(p, inhand, len);

                        if (en instanceof Player) {

                            Player target = (Player) len;

                            int disparity = Disparity.getDisparity(p.getLocation());
                            int levelDifference = Math.abs(RPGPlayer.get(p).getPl().getPlayerLevel() - RPGPlayer.get(target).getPl().getPlayerLevel());

                            if (!(levelDifference > disparity)) {

                                if (RPG.isPvPAllowed(p) && RPG.isPvPAllowed(target)) {

                                    double armorMultiplier = (1 - PlayerCombat.getTotalArmorReduction(target));
                                    armorMultiplier -= RPGPlayer.get(target).getPen().getProtectionReduction();

                                    damageToDo *= armorMultiplier;

                                    if (Ability.isUnlocked(target, AbilityType.BLOCKAGE)) {

                                        if (Ability.ifBlockNotApplies(target)) {
                                            damageTarget(p, target, damageToDo);

                                        }

                                    } else {

                                        damageTarget(p, target, damageToDo);

                                    }

                                } else {

                                    removeLastHit(p);
                                    lastHit.put(p, len.getUniqueId().toString());
                                    len.damage(damageToDo);
                                    Speed.startCharge(p.getInventory().getItemInMainHand(), p);

                                }
                            } else {
                                p.sendMessage(RPG.getPrefix() + "§7Can't hit this player, level difference is higher than the disparity!");
                            }

                        } else if (en instanceof LivingEntity) {

                            damageTarget(p, len, damageToDo);

                        }
                    } else {
                        p.sendMessage(RPG.getPrefix() + "§cYou need to be atleast level §6" + onInteract.getRequiredLevel(inhand) + " §cto use this item!");
                    }

                } else {
                    e.setCancelled(true);
                }
            } else {
                e.setCancelled(true);
            }
        } else if (e.getDamager() instanceof Fireball && e.getEntity() instanceof Player) {

            Player target = (Player) e.getEntity();
            Player p = Decimate.playerFireballs.get(e.getDamager());

            int disparity = Disparity.getDisparity(p.getLocation());
            int levelDifference = Math.abs(RPGPlayer.get(p).getPl().getPlayerLevel() - RPGPlayer.get(target).getPl().getPlayerLevel());

            if (!(levelDifference > disparity)) {

                if (Ability.isUnlocked(target, AbilityType.BLOCKAGE)) {

                    if (!Ability.ifBlockNotApplies(target)) {
                        e.setCancelled(true);

                    }

                }

            } else {
                e.setCancelled(true);
            }
        }
    }
}

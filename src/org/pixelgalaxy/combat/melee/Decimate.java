package org.pixelgalaxy.combat.melee;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.BlockIterator;
import org.pixelgalaxy.RPGPlayer;
import org.pixelgalaxy.abilities.Ability;
import org.pixelgalaxy.abilities.AbilityType;
import org.pixelgalaxy.equipment.Equipment;
import org.pixelgalaxy.player_objects.PlayerCombat;

import java.util.*;

/**
 * Created by robin on 15/08/2018.
 */
public class Decimate implements Listener {

    private static Map<Player, Long> dbCooldown = new HashMap<>();
    public static Map<Entity, Player> playerFireballs = new HashMap<>();

    public static void shootFireball(Player p) {
        Fireball f = p.launchProjectile(Fireball.class);
        f.setDirection(p.getEyeLocation().getDirection());
        f.setVelocity(f.getVelocity().multiply(0.0000001));
        f.setYield(2.0F);
        f.setBounce(false);
        f.setIsIncendiary(false);
        for (Entity en : playerFireballs.keySet()) {
            if (playerFireballs.get(en).equals(p)) {
                playerFireballs.remove(en);
            }
        }
        playerFireballs.put(f, p);
    }

    @EventHandler
    public void onDecimateClick(PlayerInteractEvent e) {

        if (e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {

            Player p = e.getPlayer();

            if (p.getInventory().getItemInMainHand() != null) {

                ItemStack inhand = p.getInventory().getItemInMainHand();

                if (Equipment.meleeMaterials.contains(inhand.getType())) {

                    if (Equipment.isMelee(inhand)) {

                        PlayerCombat pc = RPGPlayer.get(p).getPc();

                        // Check for dragons breath right click

                        if (inhand.getType().equals(Material.BLAZE_ROD)) {

                            if (dbCooldown.containsKey(p)) {
                                if (System.currentTimeMillis() - dbCooldown.get(p) > 20000) {

                                    dbCooldown.remove(p);

                                    shootFireball(p);

                                    dbCooldown.put(p, System.currentTimeMillis());
                                }
                            } else {

                                shootFireball(p);
                                dbCooldown.put(p, System.currentTimeMillis());
                            }
                        }

                        if (Ability.isUnlocked(p, AbilityType.DECIMATE)) {

                            if (!pc.hasDecimateCooldown()) {

                                pc.setDecimating(true);

                            }
                        }
                    }
                }
            }

        }

    }

}

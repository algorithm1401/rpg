package org.pixelgalaxy.combat;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.pixelgalaxy.abilities.Ability;
import org.pixelgalaxy.abilities.AbilityType;

/**
 * Created by robin on 15/08/2018.
 */
public class Sprinter implements Listener {

    public static void updateWalkSpeed(Player p){
        if (Ability.isUnlocked(p, AbilityType.SPRINTER)) {
            p.setWalkSpeed(0.2F * 1.2F);
        }
    }

}

package org.pixelgalaxy.combat;

import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.inventory.ItemStack;
import org.pixelgalaxy.RPG;
import org.pixelgalaxy.RPGPlayer;
import org.pixelgalaxy.equipment.Equipment;
import org.pixelgalaxy.player_objects.PlayerSkills;
import org.pixelgalaxy.utils.EquipmentUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by robin on 13/08/2018.
 */
public class Speed implements Listener {

    public static Map<Player, Long> playerHit = new HashMap<>();
    public static Map<Player, Long> playerChargeDelay = new HashMap<>();
    public static Map<Player, ChargeDelayDisplayer> playerChargeDelayDisplayerMap = new HashMap<>();

    public static void saveChargeDelay(Player p, ItemStack is) {

        Long timeForCharge = new Long(0);

        ItemStack inhand = is;
        if (inhand.hasItemMeta()) {
            if (Equipment.isBow(is) || Equipment.isMelee(is)) {

                PlayerSkills ps = RPGPlayer.get(p).getPs();

                double skillMultiplier = (1.0 + ((double) ps.getSpeedLevel() / (double) 50));
                timeForCharge = (long) ((2 / (EquipmentUtil.getAttackSpeed(inhand) * skillMultiplier)) * 500);

            }
        }

        playerChargeDelay.put(p, timeForCharge);

    }

    public static double getPercentCharged(Player p) {

        double percent = 0.0;

        if (playerHit.containsKey(p) && playerChargeDelay.containsKey(p)) {

            Long timeBetween = System.currentTimeMillis() - playerHit.get(p);
            percent = (double) timeBetween / (double) playerChargeDelay.get(p);

            if (percent > 1) {
                percent = 1;
            }

        }

        return percent;

    }

    public static void removeIfContains(Player p) {
        if (playerChargeDelayDisplayerMap.containsKey(p)) {
            while (playerChargeDelayDisplayerMap.containsKey(p)) {
                playerChargeDelayDisplayerMap.get(p).cancel();
                playerChargeDelayDisplayerMap.remove(p);
            }
        }
    }

    public static void startCharge(ItemStack inhand, Player p) {
        if (inhand != null) {

            if (Equipment.meleeMaterials.contains(inhand.getType())) {

                if (Equipment.isMelee(inhand)) {

                    removeIfContains(p);

                    if (playerHit.containsKey(p)) {
                        playerHit.remove(p);
                    }

                    if (playerChargeDelayDisplayerMap.containsKey(p)) {
                        playerChargeDelayDisplayerMap.get(p).cancel();
                        playerChargeDelayDisplayerMap.remove(p);
                    }

                    if (playerChargeDelay.containsKey(p)) {
                        playerChargeDelay.remove(p);
                    }

                    playerHit.put(p, System.currentTimeMillis());
                    saveChargeDelay(p, inhand);
                    ChargeDelayDisplayer chargeDelayDisplayer = new ChargeDelayDisplayer(p);
                    chargeDelayDisplayer.runTaskTimer(RPG.getPlugin(), 1, 0);
                    playerChargeDelayDisplayerMap.put(p, chargeDelayDisplayer);


                } else {

                    removeIfContains(p);
                    p.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText(""));
                }
            } else {
                removeIfContains(p);
                p.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText(""));
            }
        } else {
            removeIfContains(p);
            p.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText(""));
        }
    }

    @EventHandler
    public void itemSwitch(PlayerItemHeldEvent e) {

        Player p = e.getPlayer();

        ItemStack inhand = p.getInventory().getItem(e.getNewSlot());

        startCharge(inhand, p);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onSwing(PlayerInteractEvent e) {

        Player p = e.getPlayer();

        if (e.getAction().equals(Action.LEFT_CLICK_BLOCK) || e.getAction().equals(Action.LEFT_CLICK_AIR)) {

            if (p.getInventory().getItemInMainHand() != null && !p.getInventory().getItemInMainHand().getType().equals(Material.AIR)) {
                ItemStack inhand = p.getInventory().getItemInMainHand();

                if (Equipment.isMelee(inhand)) {

                    removeIfContains(p);

                    if (playerHit.containsKey(p)) {
                        playerHit.remove(p);
                    }

                    if (playerChargeDelay.containsKey(p)) {
                        playerChargeDelay.remove(p);
                    }

                    playerHit.put(p, System.currentTimeMillis());
                    saveChargeDelay(p, p.getInventory().getItemInMainHand());
                    ChargeDelayDisplayer chargeDelayDisplayer = new ChargeDelayDisplayer(p);
                    chargeDelayDisplayer.runTaskTimer(RPG.getPlugin(), 0, 1);
                    playerChargeDelayDisplayerMap.put(p, chargeDelayDisplayer);

                }
            }
        }
    }

}

package org.pixelgalaxy.combat;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;
import org.pixelgalaxy.RPGPlayer;
import org.pixelgalaxy.armor.ArmorEquipEvent;
import org.pixelgalaxy.enchants.Enchant;
import org.pixelgalaxy.enchants.types.EnchantType;
import org.pixelgalaxy.player_objects.PlayerEnchantments;
import org.pixelgalaxy.utils.Chance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by robin on 16/08/2018.
 */
public class Armor implements Listener {

    private static final int SECOND = 20;

    @EventHandler
    public void EntityHItByEntity(EntityDamageByEntityEvent e) {

        if (e.getEntity() instanceof Player && e.getDamager() instanceof LivingEntity) {

            Player p = (Player) e.getEntity();
            LivingEntity en = (LivingEntity) e.getDamager();

            PlayerEnchantments pen = RPGPlayer.get(p).getPen();

            for (Enchant enchant : pen.getEnchantLevels().keySet()) {

                if (enchant.getEnchantType().equals(EnchantType.ARMOR)) {

                    switch (enchant) {

                        case KADABRA:

                            if (Chance.chance((int) pen.getKadabraChance())) {

                                en.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, (int) (SECOND * pen.getKadabraIncrease()), 1, true, true), true);

                            }

                            break;

                        case IGNITE:

                            if (Chance.chance((int) pen.getIgniteChance())) {

                                en.setFireTicks((int) (SECOND * pen.getIgniteIncrease()));

                            }

                            break;

                        case BANE:

                            if (Chance.chance((int) pen.getBaneChance())) {

                                // Slow down attack speed

                            }

                            break;

                        case CACTUS:

                            if (Chance.chance((int) pen.getCactusChance())) {

                                en.setHealth(en.getHealth() - (pen.getCactusIncrease()));

                            }

                            break;

                        case LAST_STAND:

                            if ((p.getHealth() / p.getMaxHealth()) < 0.15)

                                if (Chance.chance((int) pen.getLastStandChance())) {

                                    p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, (int) pen.getLastStandIncrease() * SECOND, 1, true, true));

                                }

                            break;

                        case HARDEN:

                            if ((p.getHealth() / p.getMaxHealth()) < 0.15) {

                                p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, (int) pen.getHardenIncrease() * SECOND, 1, true, true));

                            }

                            break;

                        case SNARE_ARMOR:

                            if (Chance.chance((int) pen.getSnareChance())) {

                                en.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, (int) (pen.getSnareIncrease() * SECOND), 1, true, true));
                                en.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, (int) (pen.getSnareIncrease() * SECOND), 1, true, true));

                            }

                            break;

                        case SLOW_ARMOR:

                            if (Chance.chance((int) pen.getSlowChance())) {

                                en.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, (int) pen.getSlowIncrease() * SECOND, 1, true, true));

                            }

                            break;

                        case SELF_DESTRUCT:

                            if (Chance.chance((int) pen.getSelfDestructChance())) {

                                Entity TNT1 = p.getWorld().spawn(p.getLocation(), TNTPrimed.class);
                                ((TNTPrimed) TNT1).setFuseTicks(20);
                            }

                            break;

                        case BLAST_OFF:

                            if ((p.getHealth() / p.getMaxHealth()) < 0.15) {

                                if (Chance.chance((int) pen.getBlastOffChance())) {

                                    p.setVelocity(new Vector(0, 10, 0));

                                }
                            }
                            break;

                        case ESCAPE:

                            if ((p.getHealth() / p.getMaxHealth()) < 0.15) {

                                p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, (int) (pen.getEscapeMultiplier() * SECOND), 2, true, true));
                                p.setHealth(p.getHealth() * 1.2);

                            }

                            break;

                        case SAVIOR:

                            if ((p.getHealth() / p.getMaxHealth()) < 0.15) {

                                if (Chance.chance((int) pen.getSaviorChance())) {

                                    p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, (int) (pen.getSaviorIncrease() * SECOND), 1, true, true));

                                }

                            }

                            break;

                        case SMOKE_SCREEN:

                            if ((p.getHealth() / p.getMaxHealth()) < 0.15) {

                                if (Chance.chance((int) pen.getSmokeScreenChance())) {

                                    en.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, (int) (pen.getSmokeScreenIncrease() * SECOND), 1, true, true));
                                    en.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, (int) (pen.getSmokeScreenIncrease() * SECOND), 1, true, true));

                                }

                            }

                            break;

                        case TANK:

                            if (Chance.chance((int) pen.getTankChance())) {

                                p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, (int) (pen.getTankIncrease() * SECOND), 1, true, true));
                                p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, (int) (pen.getTankIncrease() * SECOND), 1, true, true));
                                p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, (int) (pen.getTankIncrease() * SECOND), 1, true, true));

                            }

                            break;

                        case WITHERING:

                            if (Chance.chance((int) pen.getWitheringChance())) {

                                en.addPotionEffect(new PotionEffect(PotionEffectType.WITHER, (int) (pen.getWitheringIncrease() * SECOND), 1, true, true));

                            }

                            break;

                        case ABSORBING:

                            if (Chance.chance((int) pen.getAbsorbingChance())) {

                                p.setHealth(p.getHealth() + (p.getMaxHealth() * pen.getAbsorbingIncrease()));

                            }

                            break;

                        case SATURATION:

                            if (Chance.chance((int) pen.getSaturationChance())) {

                                p.addPotionEffect(new PotionEffect(PotionEffectType.SATURATION, (int) (SECOND * pen.getSaturationIncrease()), 1, false, false));

                            }

                            break;

                        case HASTENED:

                            if (Chance.chance((int) pen.getHastenedChance())) {


                            }

                            break;

                        case GHOSTLY:

                            if (Chance.chance((int) pen.getGhostlyChance())) {

                                p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, (int) (pen.getGhostlyIncrease() * SECOND), 1, true, true));

                            }

                            break;

                        case SNOWSTORM:

                            if (Chance.chance((int) pen.getSnowstormChance())) {

                                for (Entity nearEn : p.getWorld().getNearbyEntities(p.getLocation(), 3, 3, 3)) {

                                    if (nearEn instanceof LivingEntity) {
                                        LivingEntity nearLen = (LivingEntity) nearEn;

                                        Vector direction = nearLen.getLocation().toVector().subtract(p.getLocation().toVector()).normalize();
                                        nearLen.setVelocity(direction);
                                        nearLen.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, (int) (pen.getSnowstormIncrease() * SECOND), 1, true, true));

                                    }

                                }

                            }

                            break;

                    }

                }

            }

        }

    }

}

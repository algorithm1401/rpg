package org.pixelgalaxy.combat.bow;

import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.apache.commons.lang.StringUtils;
import org.bukkit.*;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;
import org.pixelgalaxy.RPG;
import org.pixelgalaxy.RPGPlayer;
import org.pixelgalaxy.abilities.Ability;
import org.pixelgalaxy.abilities.AbilityType;
import org.pixelgalaxy.combat.Absorbtion;
import org.pixelgalaxy.combat.Armor;
import org.pixelgalaxy.combat.ChargeDelayDisplayer;
import org.pixelgalaxy.combat.Speed;
import org.pixelgalaxy.combat.bow.PiercingArrow;
import org.pixelgalaxy.disparity.Disparity;
import org.pixelgalaxy.enchants.Enchant;
import org.pixelgalaxy.enchants.types.EnchantType;
import org.pixelgalaxy.equipment.Equipment;
import org.pixelgalaxy.player_objects.PlayerCombat;
import org.pixelgalaxy.skills.SkillType;
import org.pixelgalaxy.utils.Chance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by robin on 13/08/2018.
 */
public class BowDamage implements Listener {

    public static Map<Entity, Player> arrowShots = new HashMap<>();
    public static Map<Player, ItemStack> playerDamageMap = new HashMap<>();
    public static Map<Player, Double> playerLifeStealmap = new HashMap<>();
    public static Map<Entity, Double> percentArrow = new HashMap<>();

    public static Map<Player, Long> playerDrawing = new HashMap<>();
    public static Map<Player, ChargeDelayDisplayer> chargeDelayDisplayerMap = new HashMap<>();


    public static final int SECOND = 20;

    public static double getDrawedPercent(Player p) {

        Long difference = System.currentTimeMillis() - playerDrawing.get(p);
        double percent = (double) difference / (double) 1100;
        if (percent > 1) {
            percent = 1;
        }
        return percent;
    }

    public static void removePiercing(Player p) {

        PlayerCombat pc = RPGPlayer.get(p).getPc();
        pc.setPiercing(false);

    }

    @EventHandler
    public void changeslot(PlayerItemHeldEvent e) {
        removePiercing(e.getPlayer());
        if (chargeDelayDisplayerMap.containsKey(e.getPlayer())) {
            chargeDelayDisplayerMap.get(e.getPlayer()).cancel();
            chargeDelayDisplayerMap.remove(e.getPlayer());
        }
    }

    @EventHandler
    public void changeSlot(PlayerSwapHandItemsEvent e) {
        removePiercing(e.getPlayer());
    }


    @EventHandler(priority = EventPriority.LOWEST)
    public void bowInteract(PlayerInteractEvent e) {

        if (e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {

            if (e.getItem() != null && e.getItem().getType().equals(Material.BOW)) {

                if (Equipment.isBow(e.getItem())) {

                    playerDrawing.put(e.getPlayer(), System.currentTimeMillis());
                    ChargeDelayDisplayer chargeDelayDisplayer = new ChargeDelayDisplayer(e.getPlayer(), true);
                    chargeDelayDisplayer.runTaskTimer(RPG.getPlugin(), 1, 0);
                    chargeDelayDisplayerMap.put(e.getPlayer(), chargeDelayDisplayer);

                } else {
                    Speed.removeIfContains(e.getPlayer());
                    e.getPlayer().spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText(""));
                }

            }

        }

    }

    @EventHandler()
    public void bowShoot(EntityShootBowEvent e) {

        Entity projectile = e.getProjectile();
        double magnitude = Math.sqrt(Math.pow(projectile.getVelocity().getX(), 2) + Math.pow(projectile.getVelocity().getY(), 2) + Math.pow(projectile.getVelocity().getZ(), 2));

        double percent = magnitude / 3;

        if (e.getEntity() instanceof Player) {

            Player p = (Player) e.getEntity();

            if (chargeDelayDisplayerMap.containsKey(p)) {
                chargeDelayDisplayerMap.get(p).cancel();
                chargeDelayDisplayerMap.remove(p);
            }

            p.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText(""));

            arrowShots.put(e.getProjectile(), p);
            percentArrow.put(e.getProjectile(), percent);
            playerDamageMap.put(p, p.getInventory().getItemInMainHand());
        }

    }

    @EventHandler
    public void EntityHit(EntityDamageByEntityEvent e) {

        // If the damager is arrow and hit entity is a living entity

        if (e.getDamager() instanceof Arrow && e.getEntity() instanceof LivingEntity) {

            e.getDamager().remove();

            if (arrowShots.containsKey(e.getDamager())) {
                Player p = arrowShots.get(e.getDamager());
                LivingEntity en = (LivingEntity) e.getEntity();

                ItemStack inhand = p.getInventory().getItemInMainHand();
                String name = Equipment.convertDisplayToPath(inhand.getItemMeta().getDisplayName());

                boolean pvpAllowed = true;
                boolean canHit = false;
                if (en instanceof Player) {
                    Player target = (Player) en;
                    if (!RPG.isPvPAllowed(p) || !RPG.isPvPAllowed(target)) {
                        pvpAllowed = false;
                    }

                    int disparity = Disparity.getDisparity(p.getLocation());
                    int levelDifference = Math.abs(RPGPlayer.get(p).getPl().getPlayerLevel() - RPGPlayer.get(target).getPl().getPlayerLevel());

                    if (!(levelDifference > disparity)) {
                        canHit = true;
                    }
                }

                boolean pierceEnch = false;

                if (canHit) {

                    if (pvpAllowed) {

                        if (Equipment.isBow(inhand)) {

                            if (e.getEntity() != e.getDamager()) {

                                for (String loreLine : inhand.getItemMeta().getLore()) {

                                    for (Enchant m : Enchant.values()) {

                                        if (m.getEnchantType().equals(EnchantType.BOW)) {

                                            if (loreLine.toLowerCase().contains(m.getName().toLowerCase())) {

                                                int level = Integer.valueOf(loreLine.split("\\s+")[1]);
                                                loreLine = loreLine.split("\\s+")[StringUtils.countMatches(loreLine, "\\s+")];
                                                loreLine = ChatColor.stripColor(loreLine);

                                                Location enLocation = en.getLocation();
                                                World w = Bukkit.getWorld(p.getWorld().getName());

                                                switch (m.toString().toUpperCase()) {

                                                    case "BLIND_BOW":

                                                        if (Chance.chance(m.getChance() * level)) {
                                                            en.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 5 * SECOND, 1, true, true));
                                                        }

                                                        break;

                                                    case "WEAKNESS":

                                                        if (Chance.chance(m.getChance() * level)) {
                                                            en.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 5 * SECOND, 1, true, true));
                                                        }

                                                        break;

                                                    case "POISON_BOW":

                                                        if (Chance.chance(m.getChance() * level)) {
                                                            en.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 5 * SECOND, 1, true, true));
                                                        }

                                                        break;

                                                    case "LEVITATION":

                                                        if (Chance.chance(m.getChance() * level)) {
                                                            en.addPotionEffect(new PotionEffect(PotionEffectType.LEVITATION, (int) (m.getDuration() * level) * SECOND, 1, true, true));
                                                        }

                                                        break;

                                                    case "PIERCE":

                                                        if (Chance.chance(m.getChance() * level)) {
                                                            pierceEnch = true;
                                                        }

                                                        break;

                                                    case "SLOW_BOW":

                                                        if (Chance.chance(m.getChance() * level)) {
                                                            en.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, SECOND * 5, 1, true, true));
                                                        }

                                                        break;

                                                    case "LIGHTNING_BOW":

                                                        if (Chance.chance(m.getChance() * level)) {
                                                            w.strikeLightning(enLocation);
                                                        }

                                                        break;

                                                    case "SNARE_BOW":

                                                        if (Chance.chance(m.getChance() * level)) {
                                                            en.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 5 * SECOND, 10, true, true));
                                                        }

                                                        break;

                                                    case "LIFE_STEAL_BOW":

                                                        playerLifeStealmap.put(p, m.getIncrease() * level);

                                                        break;

                                                    case "PARALYZE_BOW":

                                                        if (Chance.chance(m.getChance() * level)) {
                                                            w.strikeLightning(enLocation);
                                                            en.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 5 * SECOND, 1, true, true));
                                                            en.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 3 * SECOND, 10, true, true));
                                                        }

                                                        break;

                                                    case "BLAST_BOW":
                                                        if (Chance.chance(m.getChance() * level)) {
                                                            if (p.getWorld().getName().equalsIgnoreCase("warzone")) {

                                                                w.createExplosion(enLocation, 2.0F);

                                                            }
                                                        }
                                                        break;

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        Player shooter = arrowShots.get(e.getDamager());
                        double damage = PlayerCombat.getTotalRawBowDMG(p, inhand);
                        e.setCancelled(true);

                        // If the living target entity is a player

                        if (e.getEntity() instanceof Player) {

                            Player target = (Player) e.getEntity();

                            double armorMultiplier = (1 - PlayerCombat.getTotalArmorReduction(target));
                            armorMultiplier -= RPGPlayer.get(target).getPen().getProtectionReduction();

                            if (pierceEnch == false) {
                                damage *= armorMultiplier;
                            }

                            if (target != shooter) {

                                if (Ability.isUnlocked(target, AbilityType.BLOCKAGE)) {

                                    if (!Chance.chance(90)) {

                                        e.setCancelled(true);
                                        target.sendMessage("§eBlocked!");
                                        arrowShots.remove(e.getDamager());
                                        playerDamageMap.remove(shooter);
                                        e.getDamager().remove();

                                    } else {

                                        target.damage(damage);
                                        PlayerCombat.applyKnockBack(p, target, getDrawedPercent(p));

                                    }

                                } else {

                                    target.damage(damage);
                                    PlayerCombat.applyKnockBack(p, target, getDrawedPercent(p));

                                }
                                e.setCancelled(true);
                            }

                        } else {

                            ((LivingEntity) e.getEntity()).damage(damage);

                        }

                        if (Absorbtion.lifeStealing.containsKey(shooter)) {

                            Long diff = System.currentTimeMillis() - Absorbtion.lifeStealing.get(shooter);

                            if (diff <= 5000) {

                                shooter.setHealth(shooter.getHealth() + (e.getDamage() / 2));
                                shooter.sendMessage("§aYou stole " + Absorbtion.roundToHalf(e.getDamage() / 2) + "§c❤§a!");

                            } else {

                                Absorbtion.lifeStealing.remove(shooter);

                            }
                        }

                    } else {
                        p.sendMessage(RPG.getPrefix() + "§cPvP is not allowed in this region!");
                    }
                } else {
                    p.sendMessage(RPG.getPrefix() + "§7Can't hit this player, level difference is higher than the disparity!");
                    e.setCancelled(true);
                }

                if (playerDrawing.containsKey(p)) {
                    playerDrawing.remove(p);

                }
            }

        }

    }

}


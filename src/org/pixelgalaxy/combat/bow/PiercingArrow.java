package org.pixelgalaxy.combat.bow;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.pixelgalaxy.RPGPlayer;
import org.pixelgalaxy.abilities.Ability;
import org.pixelgalaxy.abilities.AbilityType;
import org.pixelgalaxy.player_objects.PlayerCombat;

/**
 * Created by robin on 15/08/2018.
 */
public class PiercingArrow implements Listener {

    @EventHandler
    public void piercingShot(PlayerInteractEvent e) {

        Player p = e.getPlayer();

        if(e.getAction().equals(Action.LEFT_CLICK_BLOCK) || e.getAction().equals(Action.LEFT_CLICK_AIR)){

            ItemStack mainHand = p.getInventory().getItemInMainHand();
            ItemStack offHand = p.getInventory().getItemInOffHand();

            if(mainHand.getType().equals(Material.BOW) || offHand.equals(Material.BOW)){

                if(Ability.isUnlocked(p, AbilityType.PIERCING_ARROW)){

                    PlayerCombat pc = RPGPlayer.get(p).getPc();

                    if ((System.currentTimeMillis() - pc.getPiercingCooldown()) > 20000) {
                        pc.setPiercing(true);
                    }

                }

            }

        }

    }

}

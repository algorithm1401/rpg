package org.pixelgalaxy.combat;

import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.pixelgalaxy.combat.bow.BowDamage;

/**
 * Created by robin on 29/08/2018.
 */
public class ChargeDelayDisplayer extends BukkitRunnable {

    Player p;

    public static final int TOTAL_LINES = 40;
    boolean bow = false;

    public ChargeDelayDisplayer(Player p) {
        this.p = p;
    }

    public ChargeDelayDisplayer(Player p, boolean isBow) {
        this.p = p;
        this.bow = isBow;
    }

    @Override
    public void run() {

        if (!bow) {

            String display = "§7[";
            int greenLines = (int) (Speed.getPercentCharged(p) * TOTAL_LINES);

            if (greenLines != 0) {
                for (int i = 1; i < greenLines; i++) {

                    if (greenLines != TOTAL_LINES) {

                        display += "§a|";

                    } else {

                        display += "§6|";

                    }

                }
            }

            if (greenLines < TOTAL_LINES) {

                for (int i = greenLines + 1; i < TOTAL_LINES; i++) {

                    display += "§7|";

                }
            }

            display += "§7]";

            p.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText(display));
        } else {

            String display = "§7[";
            int greenLines = (int) (BowDamage.getDrawedPercent(p) * TOTAL_LINES);

            if (greenLines != 0) {
                for (int i = 1; i < greenLines; i++) {

                    if (greenLines != TOTAL_LINES) {

                        display += "§a|";

                    } else {

                        display += "§6|";

                    }

                }
            }

            if (greenLines < TOTAL_LINES) {

                for (int i = greenLines + 1; i < TOTAL_LINES; i++) {

                    display += "§7|";

                }
            }

            display += "§7]";

            p.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText(display));

        }
    }
}

package org.pixelgalaxy.skills;

import org.bukkit.Material;

/**
 * Created by robin on 11/08/2018.
 */
public enum SkillType {

    STRENGTH("Strength", Material.IRON_SWORD, 10, "Damage"),
    ATTACK_SPEED("Attack Speed", Material.FEATHER, 11, "Attack Speed"),
    CRITICAL_CHANCE("Critical Chance", Material.BLAZE_POWDER, 12, "Crit Chance"),
    ARCHERY("Archery", Material.BOW, 14, "Damage"),
    HEARTS("Hearts", Material.APPLE, 15, "Hearts"),
    DEFENCE("Defense", Material.IRON_CHESTPLATE, 16, "Armour");

    SkillType(String name, Material material, int slot, String description) {
        this.name = name;
        this.material = material;
        this.slot = slot;
        this.max_level = 50;
        this.description = description;
    }

    private String name;
    private String description;
    private int max_level;
    private Material material;
    private int slot;

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getMax_level() {
        return max_level;
    }

    public Material getMaterial() {
        return material;
    }

    public int getSlot() {
        return slot;
    }
}

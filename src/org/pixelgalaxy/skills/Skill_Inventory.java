package org.pixelgalaxy.skills;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.pixelgalaxy.RPGPlayer;
import org.pixelgalaxy.abilities.Ability;
import org.pixelgalaxy.abilities.AbilityType;
import org.pixelgalaxy.player_objects.PlayerLevel;
import org.pixelgalaxy.player_objects.PlayerSkills;

import java.text.DecimalFormat;
import java.util.Arrays;

/**
 * Created by robin on 11/08/2018.
 */
public class Skill_Inventory {

    static DecimalFormat df = new DecimalFormat("#.##");
    private static final int size = 27;

    private static ItemStack createUpgradeIS(SkillType skillType, Player p){

        int skillLevel = RPGPlayer.get(p).getPs().getPlayerSkillLevel(skillType, p);
        ItemStack is = new ItemStack(skillType.getMaterial(), skillLevel);
        ItemMeta ism = is.getItemMeta();
        ism.setDisplayName("§aUpgrade your " + skillType.getName() + " §e[" + skillLevel + " / " + skillType.getMax_level() + "]");
        int currentPercentage = RPGPlayer.get(p).getPc().getSkillPercentage(skillType, skillLevel);
        int nextPercenatge = RPGPlayer.get(p).getPc().getSkillPercentage(skillType, skillLevel + 1);

        if(skillType.equals(SkillType.ATTACK_SPEED)){

            ism.setLore(Arrays.asList(

                    "                                       ",
                    "       §7§lNow                §6§lNext     ",
                    "      §6§l+" + currentPercentage + "%       §2>§a>        §6§l+" + nextPercenatge + "%",
                    "  §7" + skillType.getDescription() + "          §7" + skillType.getDescription() + "    ",
                    "                                       "

            ));

        }else if(skillType.equals(SkillType.CRITICAL_CHANCE)) {

            ism.setLore(Arrays.asList(

                    "                                       ",
                    "       §7§lNow                §6§lNext     ",
                    "      §6§l+" + currentPercentage + "%       §2>§a>        §6§l+" + nextPercenatge + "%",
                    "   §7" + skillType.getDescription() + "           §7" + skillType.getDescription() + "    ",
                    "                                       "

            ));

        }else{

            ism.setLore(Arrays.asList(

                    "                                       ",
                    "       §7§lNow                §6§lNext     ",
                    "      §6§l+" + currentPercentage + "%       §2>§a>        §6§l+" + nextPercenatge + "%",
                    "      §7" + skillType.getDescription() + "                §7" + skillType.getDescription() + "    ",
                    "                                       "

            ));
        }
        is.setItemMeta(ism);

        return is;

    }

    private static ItemStack createHeadIS(Player p){

        RPGPlayer rpgPlayer = RPGPlayer.get(p);
        PlayerLevel pl = rpgPlayer.getPl();

        ItemStack head = getPlayerHead(p);
        ItemMeta headm = head.getItemMeta();
        headm.setDisplayName("§a" + p.getName() + "'s stats");
        headm.setLore(Arrays.asList(

                "§7Level: §e" + pl.getPlayerLevel(),
                "§7Next level: §d" + pl.getXpToNextLevel() + " XP",
                pl.getXPPercentageBar() + " §6" + df.format(pl.getXPPercentage() * 100) + "%",
                    "",
                "§7" + SkillType.STRENGTH.getName() + ": §6" + RPGPlayer.get(p).getPs().getPlayerSkillLevel(SkillType.STRENGTH, p) + " §7(§e+" +
                        RPGPlayer.get(p).getPc().getSkillPercentage(SkillType.STRENGTH, RPGPlayer.get(p).getPs().getPlayerSkillLevel(SkillType.STRENGTH, p)) + "% §7" + SkillType.STRENGTH.getDescription() + ")",

                "§7" + SkillType.ATTACK_SPEED.getName() + ": §6" + RPGPlayer.get(p).getPs().getPlayerSkillLevel(SkillType.ATTACK_SPEED, p) + " §7(§e+" +
                        RPGPlayer.get(p).getPc().getSkillPercentage(SkillType.ATTACK_SPEED, RPGPlayer.get(p).getPs().getPlayerSkillLevel(SkillType.ATTACK_SPEED, p)) + "% §7" + SkillType.ATTACK_SPEED.getDescription() + ")",

                "§7" + SkillType.CRITICAL_CHANCE.getName() + ": §6" + RPGPlayer.get(p).getPs().getPlayerSkillLevel(SkillType.CRITICAL_CHANCE, p) + " §7(§e+" +
                        RPGPlayer.get(p).getPc().getSkillPercentage(SkillType.CRITICAL_CHANCE, RPGPlayer.get(p).getPs().getPlayerSkillLevel(SkillType.CRITICAL_CHANCE, p)) + "% §7" + SkillType.CRITICAL_CHANCE.getDescription() + ")",

                "§7" + SkillType.ARCHERY.getName() + ": §6" + RPGPlayer.get(p).getPs().getPlayerSkillLevel(SkillType.ARCHERY, p) + " §7(§e+" +
                        RPGPlayer.get(p).getPc().getSkillPercentage(SkillType.ARCHERY, RPGPlayer.get(p).getPs().getPlayerSkillLevel(SkillType.ARCHERY, p)) + "% §7" + SkillType.ARCHERY.getDescription() + ")",

                "§7" + SkillType.HEARTS.getName() + ": §6" + RPGPlayer.get(p).getPs().getPlayerSkillLevel(SkillType.HEARTS, p) + " §7(§e+" +
                        RPGPlayer.get(p).getPc().getSkillPercentage(SkillType.HEARTS, RPGPlayer.get(p).getPs().getPlayerSkillLevel(SkillType.HEARTS, p)) + "% §7" + SkillType.HEARTS.getDescription() + ")",

                "§7" + SkillType.DEFENCE.getName() + ": §6" + RPGPlayer.get(p).getPs().getPlayerSkillLevel(SkillType.DEFENCE, p) + " §7(§e+" +
                        RPGPlayer.get(p).getPc().getSkillPercentage(SkillType.DEFENCE, RPGPlayer.get(p).getPs().getPlayerSkillLevel(SkillType.DEFENCE, p)) + "% §7" + SkillType.DEFENCE.getDescription() + ")"

        ));

        head.setItemMeta(headm);

        return head;
    }

    private static ItemStack getPlayerHead(Player p){
        ItemStack head = new ItemStack(Material.SKULL_ITEM);
        head.setDurability((short) 3);
        SkullMeta skull = (SkullMeta) head.getItemMeta();
        skull.setOwner(p.getName());
        head.setItemMeta(skull);
        return head;
    }

    private static ItemStack createAbilityIS(AbilityType abilityType, Player p){

        if(Ability.isUnlocked(p, abilityType)) {
            ItemStack is = new ItemStack(abilityType.getMaterial());
            ItemMeta ism = is.getItemMeta();
            ism.setDisplayName(abilityType.getColor() + abilityType.getName());
            ism.setLore(abilityType.getDescription());
            is.setItemMeta(ism);
            return is;
        }else{
            ItemStack is = new ItemStack(Material.IRON_FENCE);
            ItemMeta ism = is.getItemMeta();
            ism.setDisplayName(abilityType.getColor() + abilityType.getName() + " §c[Unlock at level " + abilityType.getUnlockLevel() + "]");
            ism.setLore(abilityType.getDescription());
            is.setItemMeta(ism);
            return is;
        }
    }

    private static ItemStack createSkillPIs(Player p){

        PlayerSkills ps = RPGPlayer.get(p).getPs();

        int amount = ps.getToSpend();
        ItemStack sp = new ItemStack(Material.BOOK, amount);
        ItemMeta spm = sp.getItemMeta();
        spm.setDisplayName("§6" + amount + " Skill Points");
        spm.setLore(Arrays.asList("§7Use Skill Points to", "§7upgrade your skills"));
        sp.setItemMeta(spm);
        return sp;

    }

    public static void openSkillsMenu(Player p, Player toOpen){

        Inventory skillsInv = Bukkit.createInventory(null, size, "§a§l" + p.getName() + "'s §8§lprofile");

        skillsInv.setItem(8, createHeadIS(p));

        skillsInv.setItem(SkillType.STRENGTH.getSlot(), createUpgradeIS(SkillType.STRENGTH, p));
        skillsInv.setItem(SkillType.ATTACK_SPEED.getSlot(), createUpgradeIS(SkillType.ATTACK_SPEED, p));
        skillsInv.setItem(SkillType.CRITICAL_CHANCE.getSlot(), createUpgradeIS(SkillType.CRITICAL_CHANCE, p));
        skillsInv.setItem(SkillType.ARCHERY.getSlot(), createUpgradeIS(SkillType.ARCHERY, p));
        skillsInv.setItem(SkillType.HEARTS.getSlot(), createUpgradeIS(SkillType.HEARTS, p));
        skillsInv.setItem(SkillType.DEFENCE.getSlot(), createUpgradeIS(SkillType.DEFENCE, p));



        toOpen.openInventory(skillsInv);

    }

    public static void openSkillsMenu(Player p){

        Inventory skillsInv = Bukkit.createInventory(null, size, "§a§l" + p.getName() + "'s §8§lprofile");

        PlayerSkills ps = RPGPlayer.get(p).getPs();

        if (ps.getToSpend() > 0) {
            skillsInv.setItem(0, createSkillPIs(p));
        }

        skillsInv.setItem(8, createHeadIS(p));

        skillsInv.setItem(SkillType.STRENGTH.getSlot(), createUpgradeIS(SkillType.STRENGTH, p));
        skillsInv.setItem(SkillType.ATTACK_SPEED.getSlot(), createUpgradeIS(SkillType.ATTACK_SPEED, p));
        skillsInv.setItem(SkillType.CRITICAL_CHANCE.getSlot(), createUpgradeIS(SkillType.CRITICAL_CHANCE, p));
        skillsInv.setItem(SkillType.ARCHERY.getSlot(), createUpgradeIS(SkillType.ARCHERY, p));
        skillsInv.setItem(SkillType.HEARTS.getSlot(), createUpgradeIS(SkillType.HEARTS, p));
        skillsInv.setItem(SkillType.DEFENCE.getSlot(), createUpgradeIS(SkillType.DEFENCE, p));

        skillsInv.setItem(AbilityType.DECIMATE.getSlot(), createAbilityIS(AbilityType.DECIMATE, p));
        skillsInv.setItem(AbilityType.SPRINTER.getSlot(), createAbilityIS(AbilityType.SPRINTER, p));
        skillsInv.setItem(AbilityType.SUPERCRITICAL.getSlot(), createAbilityIS(AbilityType.SUPERCRITICAL, p));
        skillsInv.setItem(AbilityType.PIERCING_ARROW.getSlot(), createAbilityIS(AbilityType.PIERCING_ARROW, p));
        skillsInv.setItem(AbilityType.ABSORBTION.getSlot(), createAbilityIS(AbilityType.ABSORBTION, p));
        skillsInv.setItem(AbilityType.BLOCKAGE.getSlot(), createAbilityIS(AbilityType.BLOCKAGE, p));

        p.openInventory(skillsInv);

    }

}

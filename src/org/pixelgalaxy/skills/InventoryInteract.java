package org.pixelgalaxy.skills;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.pixelgalaxy.RPG;
import org.pixelgalaxy.RPGPlayer;
import org.pixelgalaxy.abilities.Ability;
import org.pixelgalaxy.abilities.AbilityType;
import org.pixelgalaxy.combat.Sprinter;
import org.pixelgalaxy.equipment.CustomEquipment;
import org.pixelgalaxy.events.onInventoryCrafting;
import org.pixelgalaxy.events.onJoin;
import org.pixelgalaxy.player_objects.PlayerSkills;
import org.pixelgalaxy.utils.refreshTimer;

/**
 * Created by robin on 13/08/2018.
 */
public class InventoryInteract implements Listener {

    @EventHandler
    public void onClick(InventoryClickEvent e){

        Inventory clickedInv = e.getClickedInventory();
        Player p = (Player) e.getWhoClicked();

        if(clickedInv.getTitle() != null) {

            if (clickedInv.getTitle().contains("profile")) {

                if (clickedInv.getTitle().contains(p.getName())) {

                    int slot = e.getSlot();

                    boolean canSpend = false;

                    if (RPGPlayer.get(p).getPs().getToSpend() > 0) {
                        canSpend = true;
                    }

                    PlayerSkills ps = RPGPlayer.get(p).getPs();

                    switch (slot) {

                        case 10:

                            if (ps.getStrengthLevel() < SkillType.STRENGTH.getMax_level()) {

                                if (canSpend) {
                                    ps.addStrengthPoint();
                                } else {
                                    p.sendMessage(RPG.getPrefix() + "§cYou don't have any skill points to spend!");
                                }
                            } else {
                                p.sendMessage(RPG.getPrefix() + "§cYou already have max strength level!");
                            }
                            break;

                        case 11:

                            if (ps.getSpeedLevel() < SkillType.ATTACK_SPEED.getMax_level()) {

                                if (canSpend) {
                                    ps.addSpeedPoint();
                                    if (Ability.isUnlocked(p, AbilityType.SPRINTER)) {
                                        Sprinter.updateWalkSpeed(p);
                                    }

                                } else {
                                    p.sendMessage(RPG.getPrefix() + "§cYou don't have any skill points to spend!");
                                }
                            } else {
                                p.sendMessage(RPG.getPrefix() + "§cYou already have max speed level!");
                            }

                            break;

                        case 12:

                            if (ps.getCritLevel() < SkillType.CRITICAL_CHANCE.getMax_level()) {

                                if (canSpend) {
                                    ps.addCritPoint();
                                } else {
                                    p.sendMessage(RPG.getPrefix() + "§cYou don't have any skill points to spend!");
                                }
                            } else {
                                p.sendMessage(RPG.getPrefix() + "§cYou already have max crit level!");
                            }
                            break;

                        case 14:

                            if (ps.getBowLevel() < SkillType.ARCHERY.getMax_level()) {
                                if (canSpend) {
                                    ps.addBowPoint();
                                } else {
                                    p.sendMessage(RPG.getPrefix() + "§cYou don't have any skill points to spend!");
                                }
                            } else {
                                p.sendMessage(RPG.getPrefix() + "§cYou already have max archer level!");
                            }

                            break;

                        case 15:

                            if (ps.getHealthLevel() < SkillType.HEARTS.getMax_level()) {
                                if (canSpend) {
                                    ps.addHealthoint(p);
                                } else {
                                    p.sendMessage(RPG.getPrefix() + "§cYou don't have any skill points to spend!");
                                }
                            } else {
                                p.sendMessage(RPG.getPrefix() + "§cYou already have max health level!");
                            }
                            break;

                        case 16:

                            if (ps.getDefenseLevel() < SkillType.DEFENCE.getMax_level()) {

                                if (canSpend) {
                                    ps.addDefencePoint();
                                } else {
                                    p.sendMessage(RPG.getPrefix() + "§cYou don't have any skill points to spend!");
                                }

                            } else {
                                p.sendMessage(RPG.getPrefix() + "§cYou already have max defence level!");
                            }

                            break;

                    }

                    p.updateInventory();
                    Skill_Inventory.openSkillsMenu(p);

                    e.setCancelled(true);
                }

                e.setCancelled(true);

            } else if (e.getClickedInventory().getType().equals(InventoryType.CRAFTING)) {

                if (e.getCurrentItem().equals(onInventoryCrafting.getEquipmentItem())) {

                    e.setCancelled(true);
                    if (refreshTimer.getPlayerrefreshTimerMap().containsKey(p)) {
                        refreshTimer.getPlayerrefreshTimerMap().get(p).cancel();
                        refreshTimer.getPlayerrefreshTimerMap().remove(p);
                    }
                    CustomEquipment.openPlayerEquipment(p, p);

                } else if (e.getCurrentItem().equals(onInventoryCrafting.getSkillsItem())) {

                    e.setCancelled(true);
                    if (refreshTimer.getPlayerrefreshTimerMap().containsKey(p)) {
                        refreshTimer.getPlayerrefreshTimerMap().get(p).cancel();
                        refreshTimer.getPlayerrefreshTimerMap().remove(p);
                    }
                    Skill_Inventory.openSkillsMenu(p);

                } else if (e.getCurrentItem().equals(onInventoryCrafting.getCraftItem())) {

                    e.setCancelled(true);
                    if (refreshTimer.getPlayerrefreshTimerMap().containsKey(p)) {
                        refreshTimer.getPlayerrefreshTimerMap().get(p).cancel();
                        refreshTimer.getPlayerrefreshTimerMap().remove(p);
                    }
                    onInventoryCrafting.inWorkBench.add(p);
                    Bukkit.getServer().dispatchCommand(p, "craft");

                }

            }
        }
    }

}

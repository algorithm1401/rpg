package org.pixelgalaxy.insurances;

import net.minecraft.server.v1_12_R1.ItemSaddle;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.pixelgalaxy.RPG;
import org.pixelgalaxy.equipment.Equipment;
import org.pixelgalaxy.events.onInventoryCrafting;
import org.pixelgalaxy.utils.EquipmentUtil;

import java.util.*;

/**
 * Created by robin on 19/09/2018.
 */
public class Insurance implements Listener {

    private static Map<Player, List<ItemStack>> toGiveBackMap = new HashMap<>();

    public static ItemStack getInsuranceStack(int amount) {

        ItemStack insurance = new ItemStack(Material.PAPER);
        insurance.setAmount(amount);
        ItemMeta insurancem = insurance.getItemMeta();
        insurancem.setDisplayName("§6§lItem Insurance");
        insurancem.setLore(Arrays.asList(

                "§7Put this insurance on another",
                "§7item to get an item insurance"

        ));
        insurance.setItemMeta(insurancem);

        return insurance;
    }

    public static ItemStack createItemInsurance(ItemStack toInsure, Player p) {


        ItemStack itemInsurance = new ItemStack(Material.BOOK);
        ItemMeta itemInsurancem = itemInsurance.getItemMeta();
        String display = toInsure.getItemMeta().getDisplayName();
        display = ChatColor.stripColor(display);
        if (Equipment.isArmor(toInsure) || Equipment.isBow(toInsure) || Equipment.isMelee(toInsure)) {
            display = StringUtils.substringBefore(display, " [Lv");
        }
        itemInsurancem.setDisplayName("§6Insured " + display);
        itemInsurancem.setLore(Arrays.asList(

                "§7If you have this insurance in your",
                "§7inventory when you die with the insured item,",
                "§7you will keep the item on death"

        ));
        itemInsurance.setItemMeta(itemInsurancem);

        p.sendMessage(RPG.getPrefix() + "§aYou have succesfully insured: §6" + display);

        return itemInsurance;
    }

    @EventHandler
    public void onItemClick(InventoryClickEvent e) {

        ItemStack current = e.getCurrentItem();
        ItemStack cursor = e.getCursor();
        Inventory inv = e.getClickedInventory();
        Player p = (Player) e.getWhoClicked();

        if (inv.getTitle().equalsIgnoreCase("container.inventory")) {

            if (cursor.equals(getInsuranceStack(1))) {

                e.setCancelled(true);
                e.setCursor(Insurance.createItemInsurance(current, p));

            } else if (cursor.equals(getInsuranceStack(cursor.getAmount()))) {

                e.setCancelled(true);
                p.sendMessage(RPG.getPrefix() + "§cYou can only insure one item at a time!");


            }

        }

    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent e) {

        Player p = e.getPlayer();

        if (toGiveBackMap.containsKey(p)) {

            for (ItemStack itemBack : toGiveBackMap.get(p)) {
                p.getInventory().addItem(itemBack);
                p.sendMessage(RPG.getPrefix() + "§aYou have been given back this item from insurance: " + itemBack.getItemMeta().getDisplayName());
            }

            toGiveBackMap.remove(p);

        }

    }

    @EventHandler
    public void onInvClose(ItemSpawnEvent e) {

        if (onInventoryCrafting.getInventoryShortcuts().contains(e.getEntity().getItemStack())) {
            e.setCancelled(true);
        }

    }

    @EventHandler
    public void onDrop(EntitySpawnEvent e) {
        if (e.getEntity().equals(EntityType.DROPPED_ITEM)) {

            for (ItemStack is : onInventoryCrafting.getInventoryShortcuts()) {

                if (is.getItemMeta().getDisplayName().equalsIgnoreCase(e.getEntity().getCustomName())) ;

            }
        }
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent e) {

        Player p = e.getEntity();

        List<ItemStack> drops = e.getDrops();
        List<ItemStack> booksToRemove = new ArrayList<>();
        List<ItemStack> toGiveBack = new ArrayList<>();

        for (ItemStack drop : drops) {

            if (drop.hasItemMeta()) {

                // If the item in inv is insured item book
                if (drop.getItemMeta().getDisplayName().contains("§6Insured")) {

                    ItemStack toSaveStack = new ItemStack(Material.AIR);

                    for (ItemStack toSave : drops) {

                        if (toSave.hasItemMeta() && !toGiveBack.contains(toSave)) {

                            String insureName = StringUtils.substringAfter(ChatColor.stripColor(drop.getItemMeta().getDisplayName()), "Insured ");

                            if (ChatColor.stripColor(toSave.getItemMeta().getDisplayName()).contains(insureName) && !ChatColor.stripColor(toSave.getItemMeta().getDisplayName()).contains("Insured")) {

                                int lvl = EquipmentUtil.getLevelFromWeapon(toSave);

                                if (lvl > EquipmentUtil.getLevelFromWeapon(toSaveStack)) {
                                    toSaveStack = toSave;
                                }

                            }

                        }

                    }
                    if (toSaveStack.getType() != Material.AIR) {

                        booksToRemove.add(drop);
                        toGiveBack.add(toSaveStack);

                    }

                }

            }

        }

        for (ItemStack book : booksToRemove) {
            drops.remove(book);
        }
        drops.removeAll(toGiveBack);

        if (toGiveBack.size() > 0) {
            toGiveBackMap.put(p, toGiveBack);
        }
    }

}

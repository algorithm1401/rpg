package org.pixelgalaxy.utils;

import org.bukkit.scheduler.BukkitRunnable;
import org.pixelgalaxy.RPG;

/**
 * Created by robin on 1/09/2018.
 */
public class AsyncSQL {

    public static void update(String query) {

        new BukkitRunnable() {

            @Override
            public void run() {

                RPG.SQL.mysql.update(query);

            }
        }.runTaskAsynchronously(RPG.getPlugin());

    }

}

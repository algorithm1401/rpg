package org.pixelgalaxy.utils;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.InventoryView;
import org.bukkit.scheduler.BukkitRunnable;
import org.pixelgalaxy.RPG;
import org.pixelgalaxy.events.onInventoryCrafting;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by robin on 9/09/2018.
 */

public class inventoryOpenChecker extends BukkitRunnable {

    private static Map<Player, InventoryView> playerInventoryViewMap = new HashMap<>();

    public static void resetRefresh(Player p) {
        if (refreshTimer.playerrefreshTimerMap.containsKey(p)) {
            refreshTimer.playerrefreshTimerMap.get(p).cancel();
            refreshTimer.playerrefreshTimerMap.remove(p);
        }
    }

    @Override
    public void run() {

        for (Player p : Bukkit.getServer().getOnlinePlayers()) {

            if (playerInventoryViewMap.containsKey(p)) {

                // If the inventory has changed, update the map
                if (playerInventoryViewMap.get(p) != p.getOpenInventory()) {
                    playerInventoryViewMap.remove(p);
                    playerInventoryViewMap.put(p, p.getOpenInventory());

                    // Reset the update timer
                    resetRefresh(p);

                    refreshTimer rtimer = new refreshTimer(p);
                    rtimer.runTaskTimerAsynchronously(RPG.getPlugin(), 0, 2);
                    refreshTimer.addPlaterRefreshTimer(p, rtimer);

                }

                // Set the items in inventory if inventory is craftingtable or player inventory
                if (playerInventoryViewMap.get(p).getTopInventory().getType().equals(InventoryType.CRAFTING)) {
                    onInventoryCrafting.setItems(p.getOpenInventory().getTopInventory());
                }

            } else {
                playerInventoryViewMap.put(p, p.getOpenInventory());
            }

        }

    }
}

package org.pixelgalaxy.utils;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

/**
 * Created by robin on 25/08/2018.
 */
public class Effects {

    public static void playSphereEffect(double radius, Location midLoc){

        for(double phi=0; phi<=Math.PI; phi+=Math.PI/15) {
            for(double theta=0; theta<=2*Math.PI; theta+=Math.PI/30) {
                double r = radius;
                double x = r*Math.cos(theta)*Math.sin(phi);
                double y = r*Math.cos(phi) + 1.5;
                double z = r*Math.sin(theta)*Math.sin(phi);

                midLoc.add(x,y + 0.3,z);
                midLoc.getWorld().spawnParticle(Particle.FLAME, midLoc, 1, 0F, 0F, 0F, 0.001);
                midLoc.subtract(x, y + 0.3, z);
            }
        }

    }

    public static void playSoundInRadius(Location midLoc, Sound sound){

        midLoc.getWorld().getNearbyEntities(midLoc, 10, 10, 10).forEach(entity -> {
            if(entity instanceof Player){
                Player p = (Player) entity;
                p.playSound(midLoc, sound, 1.0F, 1.0F);
            }
        });

    }

}

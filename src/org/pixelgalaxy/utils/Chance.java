package org.pixelgalaxy.utils;

import org.bukkit.Bukkit;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Created by robin on 13/08/2018.
 */
public class Chance {

    public static boolean chance(int amount){

        Random r = new Random();
        int randomNumber = r.nextInt(100) + 1;

        if(amount > randomNumber){
            return true;
        }
        return false;
    }

    public static int getRandom(int min, int max){
        Random r = new Random();
        int randomNumber = r.nextInt(max) + min;

        return randomNumber;
    }

}

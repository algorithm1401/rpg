package org.pixelgalaxy.utils;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;
import org.pixelgalaxy.RPG;
import org.pixelgalaxy.crystals.Opener;
import org.pixelgalaxy.equipment.Rarity;

/**
 * Created by robin on 25/08/2018.
 */
public class FloatingItem {


    public static Vector getVelocity(Location startLocation, Location endLocation){

        Vector v = new Vector(0, 0, 0);
        v.setX(endLocation.getX() - startLocation.getX());
        v.setY(endLocation.getY() - startLocation.getY());
        v.setZ(endLocation.getZ() - startLocation.getZ());

        v = v.multiply(0.01);

        return v;

    }

    public static int highestLocationNumber(){
        int number = -1;
        for(String s : RPG.getPlugin().getConfig().getKeys(true)){

            if((s.contains("portal_location"))) {
                if ((StringUtils.countMatches(s, ".") == 1)) {
                    s = s.replace("portal_location.", "");
                    number = Integer.valueOf(s);
                }
            }

        }
        return number;
    }

    public static void create(Location loc, ItemStack is, Player p, Rarity r) {

        ArmorStand stand = loc.getWorld().spawn(loc, ArmorStand.class);
        stand.setBasePlate(false);
        stand.setVisible(false);
        stand.setInvulnerable(true);
        stand.setGravity(true);
        stand.setCollidable(false);
        Item item = loc.getWorld().dropItem(loc, is);
        item.setPickupDelay(Integer.MAX_VALUE);
        item.setGlowing(true);
        stand.setPassenger(item);

        int randomNum = Chance.getRandom(1, highestLocationNumber());
        Location endLoc = configSavers.getLocation("portal_location." + randomNum);
        Effects.playSoundInRadius(stand.getLocation(), Sound.ENTITY_PLAYER_LEVELUP);

        endLoc.add(1, 0, 0);

        Long start = System.currentTimeMillis();

        new BukkitRunnable(){

            @Override
            public void run(){

                if ((((int) stand.getLocation().getX()) == endLoc.getX()) || System.currentTimeMillis() - start > 15000) {

                    stand.setVelocity(new Vector(0, 0, 0));
                    stand.setGravity(false);
                    openAnimation openAnimation = new openAnimation(stand, endLoc, p, r);
                    openAnimation.runTaskTimer(RPG.getPlugin(), 1, 0);

                    this.cancel();

                }else {

                    stand.setVelocity(getVelocity(loc, endLoc));

                }

            }

        }.runTaskTimer(RPG.getPlugin(), 0, 1);

    }

}

class openAnimation extends BukkitRunnable {

    ArmorStand stand;
    Location endLoc;
    int count = 0;
    Player p;
    Rarity r;

    public openAnimation(ArmorStand stand, Location endLoc, Player p, Rarity r) {
        this.stand = stand;
        this.endLoc = endLoc;
        this.p = p;
        this.r = r;
    }

    @Override
    public void run() {

        if(count == 20){
            if (stand.getPassengers() != null) {
                stand.getPassenger().remove();
            }
            if (stand != null) {
                stand.remove();
            }
            for (int i = 0; i < 2; i++) {
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        Effects.playSoundInRadius(endLoc, Sound.ENTITY_BLAZE_SHOOT);
                    }
                }.runTaskLater(RPG.getPlugin(), 5);
            }

            Opener.giveRandomReward(p, r);
            Opener.openingCrystal.remove(p);
            this.cancel();
        }

        Effects.playSphereEffect(0.5, endLoc);
        Effects.playSoundInRadius(endLoc, Sound.ENTITY_BLAZE_BURN);

        count++;

    }
}
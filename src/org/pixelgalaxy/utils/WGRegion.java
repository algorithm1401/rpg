package org.pixelgalaxy.utils;

import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.sun.org.apache.regexp.internal.RE;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Created by robin on 22/09/2018.
 */
public class WGRegion {

    public static boolean playerInRegion(Player p, String regionName) {

        for (final ProtectedRegion r : WGBukkit.getRegionManager(p.getWorld())
                .getApplicableRegions(p.getLocation())) {

            if (r.getId().equalsIgnoreCase(regionName)) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

}


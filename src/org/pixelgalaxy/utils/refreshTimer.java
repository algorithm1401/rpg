package org.pixelgalaxy.utils;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.scheduler.BukkitRunnable;
import org.pixelgalaxy.events.onInventoryCrafting;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by robin on 9/09/2018.
 */
public class refreshTimer extends BukkitRunnable {

    public static Map<Player, refreshTimer> playerrefreshTimerMap = new HashMap<>();

    public static void addPlaterRefreshTimer(Player p, refreshTimer rTimer) {
        playerrefreshTimerMap.put(p, rTimer);
    }

    Player p;

    public refreshTimer(Player p) {
        this.p = p;
    }

    public static Map<Player, refreshTimer> getPlayerrefreshTimerMap() {
        return playerrefreshTimerMap;
    }

    public Player getP() {
        return p;
    }

    @Override
    public void run() {

        if (p.getOpenInventory().getTopInventory().getTitle().equalsIgnoreCase("container.crafting") && !onInventoryCrafting.inWorkBench.contains(p)) {
            p.updateInventory();
            onInventoryCrafting.setItems(p.getOpenInventory().getTopInventory());
            playerrefreshTimerMap.remove(p);
            this.cancel();
        } else {
            playerrefreshTimerMap.remove(p);
            this.cancel();
        }

    }
}

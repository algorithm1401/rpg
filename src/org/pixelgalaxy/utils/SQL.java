package org.pixelgalaxy.utils;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginDescriptionFile;
import org.pixelgalaxy.RPG;
import paulek.mysql.Main;
import paulek.mysql.MySQL;

/**
 * Created by robin on 12/08/2018.
 */
public class SQL {

    private PluginDescriptionFile pdf;

    public String HOST;
    public String DATABASE;
    public String USERNAME;
    public String PASS;

    public MySQL mysql;

    public boolean connected;

    public SQL(){
        this.HOST = RPG.getPlugin().getConfig().getString("mysql.host");
        this.DATABASE = RPG.getPlugin().getConfig().getString("mysql.database");
        this.USERNAME = RPG.getPlugin().getConfig().getString("mysql.username");
        this.PASS = RPG.getPlugin().getConfig().getString("mysql.password");
    }

    public void establishConnection(){

        this.pdf = RPG.getPlugin().getDescription();

        mysql = new MySQL((Main) Bukkit.getServer().getPluginManager().getPlugin("MySQL"), pdf.getName());
        connected = mysql.Connect(HOST, DATABASE, USERNAME, PASS);

        if(!this.connected){
            RPG.getPlugin().getLogger().info("Unable to establish a MySQL connection.");
        }

    }

    public static void createTables(){
        RPG.SQL.mysql.update("CREATE TABLE IF NOT EXISTS player_xp(" +
                "uuid VARCHAR(36)," +
                "total_xp INT," +
                "PRIMARY KEY (uuid)" +
                ");");

        RPG.SQL.mysql.update("CREATE TABLE IF NOT EXISTS player_skills(" +
                "uuid VARCHAR(36)," +
                "strength INT," +
                "speed INT," +
                "crit INT," +
                "archery INT," +
                "hearts INT," +
                "defense INT," +
                "tospend INT," +
                "PRIMARY KEY (uuid)" +
                ");");

        RPG.SQL.mysql.update("CREATE TABLE IF NOT EXISTS player_equipment(" +
                "uuid VARCHAR(36)," +
                "combat_boots TEXT," +
                "combat_leggings TEXT," +
                "combat_chestplate TEXT," +
                "combat_helmet TEXT," +
                "ring1 TEXT," +
                "ring2 TEXT," +
                "necklace TEXT," +
                "scroll TEXT," +
                "earring1 TEXT," +
                "earring2 TEXT," +
                "cosmetic_boots TEXT," +
                "cosmetic_leggings TEXT," +
                "cosmetic_chestplate TEXT," +
                "cosmetic_helmet TEXT" +
                ");");

        RPG.SQL.mysql.update("CREATE TABLE IF NOT EXISTS player_bounties(" +
                "uuid VARCHAR(36)," +
                "bounty_types TEXT," +
                "bounty_progress TEXT" +
                ");");

    }

}

package org.pixelgalaxy.utils;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;
import org.pixelgalaxy.equipment.Rarity;

/**
 * Created by robin on 29/08/2018.
 */
public class EquipmentUtil {

    public static String parseDisplayToPath(ItemStack item) {

        String path = "";

        if (item != null) {

            if (item.hasItemMeta()) {

                boolean hasRarity = false;
                for (String s : item.getItemMeta().getLore()) {
                    if (ChatColor.stripColor(s).contains("Rarity")) {
                        hasRarity = true;
                    }
                }

                if (hasRarity) {
                    path = ChatColor.stripColor(item.getItemMeta().getDisplayName());
                    path = StringUtils.substringBefore(path, " [");
                    path = path.replaceAll("\\s+", "_");
                    path = path.replaceAll("-", "_");
                    path = path.toLowerCase();
                    path = path.replaceAll("'", "");
                }
            }
        }

        return path;
    }

    public static Integer getLevelFromWeapon(ItemStack item) {

        int level = 0;

        if (item.getItemMeta() != null && item != null) {

            String display = item.getItemMeta().getDisplayName();
            display = ChatColor.stripColor(display);
            display = StringUtils.substringAfter(display, "[Lv");
            display = StringUtils.substringBefore(display, "]");
            display = display.replaceAll(" ", "");

            level = Integer.valueOf(display);

        }
        return level;
    }

    public static Rarity getItemRarity(ItemStack is) {

        if (is.hasItemMeta()) {

            for (String loreLine : is.getItemMeta().getLore()) {

                loreLine = ChatColor.stripColor(loreLine);

                if (ChatColor.stripColor(loreLine).contains("Rarity")) {

                    Rarity r = Rarity.valueOf(StringUtils.substringAfter(loreLine, "Rarity: ").toUpperCase());
                    return r;

                }

            }

        }
        return null;

    }

    public static double getAttackSpeed(ItemStack item) {

        double attackSpeed = 0;

        for (String s : item.getItemMeta().getLore()) {

            if (ChatColor.stripColor(s).contains("Attack Speed")) {

                attackSpeed = Double.valueOf(StringUtils.substringAfter(ChatColor.stripColor(s), "Speed: ").replaceAll(",", "."));

            }

        }

        return attackSpeed;
    }

}

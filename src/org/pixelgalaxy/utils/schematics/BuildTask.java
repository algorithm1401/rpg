package org.pixelgalaxy.utils.schematics;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.pixelgalaxy.RPG;


/**
 * @author SamB440
 */
public class BuildTask implements Runnable {

    public static Block getTargetBlock(Player player, int range) {
        return player.getTargetBlock((Set<Material>) null, range);
    }

    private HashMap<Player, List<Location>> cache = new HashMap<Player, List<Location>>();

    @Override
    public void run() {

        for (Player player : Bukkit.getOnlinePlayers()) {
            if (RPG.getPlugin().getPlayerManagement().isBuilding(player.getUniqueId())) {
                List<Location> locations = RPG.getPlugin().getPlayerManagement().getBuilding(player.getUniqueId()).pasteSchematic(getTargetBlock(player, 10).getLocation(), player, true);
                Bukkit.getScheduler().scheduleSyncDelayedTask(RPG.getPlugin(), () -> {
                    if (cache.containsKey(player)) {
                        if (!cache.get(player).equals(locations)) {
                            int current = 0;
                            for (Location location : cache.get(player)) {
                                if (location.distance(locations.get(current)) >= 1)
                                    location.getBlock().getState().update(true, false);
                                current++;
                            }
                            cache.remove(player);
                        }
                    }
                    if (!cache.containsKey(player)) cache.put(player, locations);
                }, 20);
            }
        }
    }
}


package org.pixelgalaxy.utils.schematics;

/*
 * BLOCK BY BLOCK VERSION ONLY!
 */

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_12_R1.NBTCompressedStreamTools;
import net.minecraft.server.v1_12_R1.NBTTagCompound;
import org.pixelgalaxy.RPG;

/**
 * @author Jojodmo - Schematic Reader
 * @author SamB440 - Schematic previews, centering and pasting block-by-block
 */
public class SchematicReplacer {

    private File schematic;
    private List<Integer> pastes = new ArrayList<Integer>();
    private int current = 0;
    boolean pasted;

    public SchematicReplacer(File schematic) {
        this.schematic = schematic;
    }

    @SuppressWarnings("deprecation")
    public List<Location> pasteSchematic(Location loc, Player paster, int time) {
        try {

            /*
             * Read the schematic file. Get the width, height, length, blocks, and block data.
             */
            FileInputStream fis = new FileInputStream(schematic);
            NBTTagCompound nbt = NBTCompressedStreamTools.a(fis);

            short width = nbt.getShort("Width");
            short height = nbt.getShort("Height");
            short length = nbt.getShort("Length");

            byte[] blocks = nbt.getByteArray("Blocks");
            byte[] data = nbt.getByteArray("Data");

            fis.close();

            List<Integer> indexes = new ArrayList<Integer>();
            List<Location> locations = new ArrayList<Location>();
            List<Integer> otherindex = new ArrayList<Integer>();
            List<Location> otherloc = new ArrayList<Location>();

            /*
             * Loop through all the blocks within schematic size.
             */
            for (int x = 0; x < width; ++x) {
                for (int y = 0; y < height; ++y) {
                    for (int z = 0; z < length; ++z) {
                        int index = y * width * length + z * width + x;
                        final Location location = new Location(loc.getWorld(), (x + loc.getX()) - (int) width / 2, y + paster.getLocation().getY(), (z + loc.getZ()) - (int) length / 2);

                        /*
                         * Ignore blocks that aren't air. Change this if you want the air to destroy blocks too.
                         * Add items to blacklist if you want them placed last, or if they get broken.
                         * IMPORTANT!
                         * Make the block unsigned, so that blocks with an id over 127, like quartz and emerald, can be pasted.
                         */
                        Material material = Material.getMaterial(blocks[index] & 0xFF);
                        List<Material> blacklist = Arrays.asList(Material.STATIONARY_LAVA,
                                Material.STATIONARY_WATER,
                                Material.GRASS,
                                Material.ARMOR_STAND,
                                Material.LONG_GRASS,
                                Material.BANNER,
                                Material.STANDING_BANNER,
                                Material.WALL_BANNER,
                                Material.CHORUS_FLOWER,
                                Material.CROPS,
                                Material.DOUBLE_PLANT,
                                Material.CHORUS_PLANT,
                                Material.YELLOW_FLOWER,
                                Material.TORCH);
                        if (material != Material.AIR) {
                            if (!blacklist.contains(material)) {
                                indexes.add(index);
                                locations.add(location);
                            } else {
                                otherindex.add(index);
                                otherloc.add(location);
                            }
                        }
                    }
                }
            }

            /*
             * Make sure liquids are placed last.
             */

            for (Integer index : otherindex) {
                indexes.add(index);
            }

            otherindex.clear();

            for (Location location : otherloc) {
                locations.add(location);
            }

            otherloc.clear();

            /*
             * Start pasting each block every tick
             */
            CustomScheduler scheduler = new CustomScheduler();

            scheduler.setTask(Bukkit.getScheduler().scheduleSyncRepeatingTask(RPG.getPlugin(), () -> {

                /*
                 * Get the block, set the type, data, and then update the state.
                 */

                final Block block = locations.get(current).getBlock();
                block.setType(Material.getMaterial(blocks[indexes.get(current)] & 0xFF));
                block.setData(data[indexes.get(current)]);
                block.getState().update(true, false);

                /*
                 * Play block effects
                 */

                block.getLocation().getWorld().spawnParticle(Particle.CLOUD, block.getLocation(), 6);
                block.getLocation().getWorld().playEffect(block.getLocation(), Effect.STEP_SOUND, block.getTypeId());

                current++;

                if (current >= locations.size() || current >= indexes.size()) {
                    scheduler.cancel();
                    pasted = true;
                    current = 0;
                }

            }, 0, time));

            pastes.add(scheduler.getTask());

            return locations;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Location> pasteSchematic(Location loc, Player paster) {
        return pasteSchematic(loc, paster, 3);
    }

    /**
     * Cancels all current instances of pasting tasks for this schematic.
     */
    public void cancel() {
        for (Integer tasks : pastes) {
            Bukkit.getScheduler().cancelTask(tasks);
            pastes.remove(tasks);
        }
    }
}



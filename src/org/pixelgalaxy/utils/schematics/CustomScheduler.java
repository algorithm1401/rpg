package org.pixelgalaxy.utils.schematics;

import org.bukkit.Bukkit;


/**
 * @author SamB440
 */
public class CustomScheduler {

    private int task;

    public void cancel() {
        Bukkit.getScheduler().cancelTask(task);
    }

    public void setTask(int task) {
        this.task = task;
    }

    public int getTask() {
        return this.task;
    }

}

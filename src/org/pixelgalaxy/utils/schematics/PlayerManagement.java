package org.pixelgalaxy.utils.schematics;

import org.pixelgalaxy.RPG;

import java.io.File;
import java.util.HashMap;
import java.util.UUID;

/**
 * @author SamB440
 */
public class PlayerManagement {

    HashMap<UUID, Boolean> building = new HashMap<UUID, Boolean>();
    HashMap<UUID, String> current = new HashMap<UUID, String>();

    public void setBuilding(UUID uuid, boolean val, String name) {
        if (!building.containsKey(uuid)) {
            building.put(uuid, val);
        } else building.replace(uuid, val);

        if (!current.containsKey(uuid)) {
            current.put(uuid, name);
        } else current.replace(uuid, name);
    }

    public boolean isBuilding(UUID uuid) {
        return building.get(uuid);
    }

    public CustomSchematic getBuilding(UUID uuid) {
        return new CustomSchematic(new File(RPG.getPlugin().getDataFolder() + "/volcano.schematic"));
    }
}


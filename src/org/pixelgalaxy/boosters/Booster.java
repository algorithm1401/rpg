package org.pixelgalaxy.boosters;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.pixelgalaxy.RPG;

import java.io.File;

public class Booster {

    public static File file;
    public static FileConfiguration boosterFile;
    private static String boosterPath = "boosters.yml";

    public static FileConfiguration getBoosterFile() {
        if (boosterFile == null) boosterFile = YamlConfiguration.loadConfiguration(getFile());
        return boosterFile;
    }

    private static File getFile() {
        if (file == null) {
            RPG.getPlugin().saveResource(boosterPath, false);
            file = new File(RPG.getPlugin().getDataFolder(), boosterPath);
        }
        return file;
    }

    private static void addBoosterInConfig(Player p, String type, int tier) {
        int currentBoosters = 0;
        if (getBoosterFile().contains("boosters" + p.getUniqueId().toString() + "." + type + "." + tier)) {
            currentBoosters = getBoosterFile().getInt("boosters" + p.getUniqueId().toString() + "." + type + "." + tier);
        }
        currentBoosters++;
        getBoosterFile().set("boosters" + p.getUniqueId().toString() + "." + type + "." + tier, currentBoosters);
    }

    public static void addBooster(Player p, BoosterType boosterType) {

        switch (boosterType) {

            case XP_1:

                addBoosterInConfig(p, "xp", 1);

                break;

            case XP_2:

                addBoosterInConfig(p, "xp", 2);

                break;

            case ITEM_1:

                addBoosterInConfig(p, "item", 1);

                break;

            case ITEM_2:

                addBoosterInConfig(p, "item", 2);

                break;
        }

    }

}


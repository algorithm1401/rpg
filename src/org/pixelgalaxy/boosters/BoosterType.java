package org.pixelgalaxy.boosters;


public enum BoosterType {

    XP_1(1.5, "§6§lx1.5 §a§lXP Booster"),
    XP_2(2.0, "§6§lx2.0 §a§lXP Booster"),
    ITEM_1(1.5, "§6§lx1.5 §c§lLoot Booster"),
    ITEM_2(2.0, "§6§lx2.0 §c§lLoot Booster");

    private double multiplier;
    private String name;

    BoosterType(double multiplier, String name){

        this.multiplier = multiplier;
        this.name = name;
    }

    public double getMultiplier(){
        return this.multiplier;
    }

    public String getName(){
        return this.name;
    }

}
package org.pixelgalaxy.equipment;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.pixelgalaxy.RPG;
import org.pixelgalaxy.RPGPlayer;
import org.pixelgalaxy.events.onInteract;
import org.pixelgalaxy.player_objects.PlayerCombat;
import org.pixelgalaxy.player_objects.PlayerEnchantments;
import org.pixelgalaxy.player_objects.PlayerEquipment;

import java.util.*;

/**
 * Created by robin on 26/08/2018.
 */
public class CustomEquipment implements Listener {

    public static List<Player> playersChangingEquipment = new ArrayList<>();

    public static Map<Integer, String> slotWithItem;

    static {

        slotWithItem = new HashMap<Integer, String>();

        slotWithItem.put(10, "combat_helmet");
        slotWithItem.put(19, "combat_chestplate");
        slotWithItem.put(28, "combat_leggings");
        slotWithItem.put(37, "combat_boots");
        slotWithItem.put(12, "earring1");
        slotWithItem.put(13, "necklace");
        slotWithItem.put(14, "earring2");
        slotWithItem.put(30, "ring1");
        slotWithItem.put(31, "scroll");
        slotWithItem.put(32, "ring2");
        slotWithItem.put(16, "cosmetic_helmet");
        slotWithItem.put(25, "cosmetic_chestplate");
        slotWithItem.put(34, "cosmetic_leggings");
        slotWithItem.put(43, "cosmetic_boots");

    }

    public static ItemStack noSlot() {
        ItemStack is = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 15);
        ItemMeta ism = is.getItemMeta();
        ism.setDisplayName("§8No slot!");
        ism.setLore(Arrays.asList("§cYou can click all you want", "§cbecause I don't do anything"));
        is.setItemMeta(ism);

        return is;
    }

    public static boolean isItemForSlot(ItemStack is, int slot, Player p) {

        PlayerEquipment peq = RPGPlayer.get(p).getPeq();

        switch (slot) {

            case 10:

                if (Equipment.isArmor(is)) {

                    if (is.getType().toString().toLowerCase().contains("helmet")) {
                        peq.setSlotItem(slot, is);
                        return true;
                    } else {
                        p.sendMessage(RPG.getPrefix() + "§cThis slot needs a combat helmet!");
                    }
                } else {
                    p.sendMessage(RPG.getPrefix() + "§cThis slot needs a combat helmet!");
                }
                break;

            case 19:

                if (Equipment.isArmor(is)) {

                    if (is.getType().toString().toLowerCase().contains("chestplate")) {
                        peq.setSlotItem(slot, is);
                        return true;
                    } else {
                        p.sendMessage(RPG.getPrefix() + "§cThis slot needs a combat chestplate!");
                    }
                } else {
                    p.sendMessage(RPG.getPrefix() + "§cThis slot needs a combat chestplate!");
                }
                break;

            case 28:
                if (Equipment.isArmor(is)) {

                    if (is.getType().toString().toLowerCase().contains("leggings")) {
                        peq.setSlotItem(slot, is);
                        return true;
                    } else {
                        p.sendMessage(RPG.getPrefix() + "§cThis slot needs a combat leggings!");
                    }
                } else {
                    p.sendMessage(RPG.getPrefix() + "§cThis slot needs a combat leggings!");
                }

                break;

            case 37:

                if (Equipment.isArmor(is)) {

                    if (is.getType().toString().toLowerCase().contains("boots")) {
                        peq.setSlotItem(slot, is);
                        return true;
                    } else {
                        p.sendMessage(RPG.getPrefix() + "§cThis slot needs a combat boots!");
                    }
                } else {
                    p.sendMessage(RPG.getPrefix() + "§cThis slot needs a combat boots!");
                }

                break;

            case 12:

                break;

            case 13:

                break;

            case 14:

                break;

            case 16:

                List<Material> cosmeticHelmetTypes = Arrays.asList(Material.LEATHER_HELMET, Material.CHAINMAIL_HELMET, Material.GOLD_HELMET, Material.DIAMOND_HELMET, Material.IRON_HELMET);

                if (!Equipment.isArmor(is)) {

                    if (cosmeticHelmetTypes.contains(is.getType())) {
                        peq.setSlotItem(slot, is);
                        return true;

                    } else {
                        p.sendMessage(RPG.getPrefix() + "§cThis slot needs a cosmetic helmet!");
                    }
                } else {
                    p.sendMessage(RPG.getPrefix() + "§cThis slot needs a cosmetic helmet!");
                }
                break;

            case 25:

                List<Material> cosmeticChestplateTypes = Arrays.asList(Material.LEATHER_CHESTPLATE, Material.CHAINMAIL_CHESTPLATE, Material.GOLD_CHESTPLATE, Material.DIAMOND_CHESTPLATE, Material.IRON_CHESTPLATE);

                if (!Equipment.isArmor(is)) {

                    if (cosmeticChestplateTypes.contains(is.getType())) {
                        peq.setSlotItem(slot, is);
                        return true;

                    } else {
                        p.sendMessage(RPG.getPrefix() + "§cThis slot needs a cosmetic chestplate!");
                    }
                } else {
                    p.sendMessage(RPG.getPrefix() + "§cThis slot needs a cosmetic chestplate!");
                }
                break;

            case 34:

                List<Material> cosmeticLeggingsTypes = Arrays.asList(Material.LEATHER_LEGGINGS, Material.CHAINMAIL_LEGGINGS, Material.GOLD_LEGGINGS, Material.DIAMOND_LEGGINGS, Material.IRON_LEGGINGS);

                if (!Equipment.isArmor(is)) {

                    if (cosmeticLeggingsTypes.contains(is.getType())) {
                        peq.setSlotItem(slot, is);
                        return true;

                    } else {
                        p.sendMessage(RPG.getPrefix() + "§cThis slot needs a cosmetic leggings!");
                    }
                } else {
                    p.sendMessage(RPG.getPrefix() + "§cThis slot needs a cosmetic leggings!");
                }
                break;

            case 43:

                List<Material> cosmeticBootsTypes = Arrays.asList(Material.LEATHER_BOOTS, Material.CHAINMAIL_BOOTS, Material.GOLD_BOOTS, Material.DIAMOND_BOOTS, Material.IRON_BOOTS);

                if (!Equipment.isArmor(is)) {

                    if (cosmeticBootsTypes.contains(is.getType())) {
                        peq.setSlotItem(slot, is);
                        return true;

                    } else {
                        p.sendMessage(RPG.getPrefix() + "§cThis slot needs a cosmetic boots!");
                    }
                } else {
                    p.sendMessage(RPG.getPrefix() + "§cThis slot needs a cosmetic boots!");
                }
                break;

            case 30:

                break;

            case 31:

                break;

            case 32:

                break;

        }
        return false;
    }

    public static void openPlayerEquipment(Player p, Player target) {

        Inventory inv = Bukkit.createInventory(null, 54, "§6" + target.getName() + "§6's §aEquipment");
        List<Integer> noSlots = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 15, 17, 18, 20, 21, 22, 23, 24, 26, 27, 29, 33, 35, 36, 38, 39, 40, 41, 42, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53);

        for (Integer slot : noSlots) {

            inv.setItem(slot, noSlot());

        }

        PlayerEquipment peq = RPGPlayer.get(target).getPeq();

        inv.setItem(10, peq.getCombat_helmet());
        inv.setItem(19, peq.getCombat_chestplate());
        inv.setItem(28, peq.getCombat_leggings());
        inv.setItem(37, peq.getCombat_boots());

        inv.setItem(12, peq.getEarring1());
        inv.setItem(13, peq.getNecklace());
        inv.setItem(14, peq.getEarring2());

        inv.setItem(30, peq.getRing1());
        inv.setItem(31, peq.getScroll());
        inv.setItem(32, peq.getRing2());

        inv.setItem(16, peq.getCosmetic_helmet());
        inv.setItem(25, peq.getCosmetic_chestplate());
        inv.setItem(34, peq.getCosmetic_leggings());
        inv.setItem(43, peq.getCosmetic_boots());

        playersChangingEquipment.add(p);

        p.openInventory(inv);
    }

    public static void updatePlayerArmor(Player p) {

        PlayerEquipment peq = RPGPlayer.get(p).getPeq();

        if ((peq.getCombat_boots().getType() != Material.PAPER)) {

            p.getEquipment().setBoots(peq.getCombat_boots());

        } else if (peq.getCosmetic_boots().getType() != Material.PAPER) {

            p.getEquipment().setBoots(peq.getCosmetic_boots());

        } else {

            p.getEquipment().setBoots(null);

        }

        if ((peq.getCombat_leggings().getType() != Material.PAPER)) {

            p.getEquipment().setLeggings(peq.getCombat_leggings());

        } else if (peq.getCosmetic_leggings().getType() != Material.PAPER) {

            p.getEquipment().setLeggings(peq.getCosmetic_leggings());

        } else {
            p.getEquipment().setLeggings(null);
        }

        if ((peq.getCombat_chestplate().getType() != Material.PAPER)) {

            p.getEquipment().setChestplate(peq.getCombat_chestplate());

        } else if (peq.getCosmetic_chestplate().getType() != Material.PAPER) {

            p.getEquipment().setChestplate(peq.getCosmetic_chestplate());

        } else {
            p.getEquipment().setChestplate(null);
        }

        if ((peq.getCombat_helmet().getType() != Material.PAPER)) {

            p.getEquipment().setHelmet(peq.getCombat_helmet());

        } else if (peq.getCosmetic_helmet().getType() != Material.PAPER) {

            p.getEquipment().setHelmet(peq.getCosmetic_helmet());

        } else {
            p.getEquipment().setHelmet(null);
        }

        RPGPlayer.get(p).setPen(new PlayerEnchantments(RPGPlayer.get(p).getPeq()));
        PlayerCombat.updatePlayerMaxHealth(p);

    }

    public static boolean isInventoryFull(Player p) {
        return p.getInventory().firstEmpty() == -1;
    }

    @EventHandler
    public void InventoryInteract(InventoryClickEvent e) {

        Inventory inv = e.getClickedInventory();
        InventoryAction a = e.getAction();
        Player p = (Player) e.getWhoClicked();
        int slot = e.getSlot();

        if (ChatColor.stripColor(inv.getTitle()).contains("Equipment")) {

            // If player picks armor piece out of equipment inventory

            if (a.equals(InventoryAction.PICKUP_ALL) || a.equals(InventoryAction.PICKUP_ONE) || a.equals(InventoryAction.PICKUP_HALF) || a.equals(InventoryAction.PICKUP_SOME) || a.equals(InventoryAction.MOVE_TO_OTHER_INVENTORY)) {

                if (slotWithItem.keySet().contains(slot)) {

                    if (!e.getCurrentItem().getType().equals(Material.PAPER)) {

                        ItemStack is = new ItemStack(Material.PAPER, 1);
                        ItemMeta ism = is.getItemMeta();
                        ism.setDisplayName("§c" + CustomEquipment.slotWithItem.get(slot).replaceAll("_", " ") + " slot");
                        ism.setLore(Arrays.asList(" "));
                        is.setItemMeta(ism);

                        if (!a.equals(InventoryAction.MOVE_TO_OTHER_INVENTORY)) {

                            RPGPlayer.get(p).getPeq().setSlotItem(slot, is);

                            e.setCancelled(true);
                            e.setCursor(e.getCurrentItem());
                            e.setCurrentItem(is);

                        } else {

                            if (!isInventoryFull(p)) {

                                RPGPlayer.get(p).getPeq().setSlotItem(slot, is);

                                e.setCancelled(true);
                                p.getInventory().addItem(e.getCurrentItem());
                                e.setCursor(null);
                                e.setCurrentItem(is);
                            } else {
                                e.setCancelled(true);
                                p.sendMessage(RPG.getPrefix() + "§cYou can't take this out, your inventory is full!");
                            }

                        }

                        updatePlayerArmor(p);

                    } else {
                        e.setCancelled(true);
                    }

                } else {
                    e.setCancelled(true);
                }

                // If player puts in/changes armorpiece in equipment inventory

            } else if ((a.equals(InventoryAction.SWAP_WITH_CURSOR) || a.equals(InventoryAction.PLACE_ONE) || a.equals(InventoryAction.PLACE_ALL) || a.equals(InventoryAction.PLACE_SOME))) {

                if (slotWithItem.keySet().contains(slot)) {

                    ItemStack cursor = e.getCursor();

                    if (cursor.hasItemMeta()) {

                        if (!isItemForSlot(e.getCursor(), slot, p)) {
                            e.setCancelled(true);
                        } else {
                            e.setCancelled(true);

                            if (RPGPlayer.get(p).getPl().getPlayerLevel() >= onInteract.getRequiredLevel(cursor)) {

                                ItemStack is = new ItemStack(Material.PAPER, 1);
                                ItemMeta ism = is.getItemMeta();
                                ism.setDisplayName("§c" + CustomEquipment.slotWithItem.get(slot).replaceAll("_", " ") + " slot");
                                is.setItemMeta(ism);

                                e.setCursor(new ItemStack(Material.AIR));
                                e.setCurrentItem(cursor);
                                updatePlayerArmor(p);
                            } else {
                                p.sendMessage(RPG.getPrefix() + "§cYou need to be atleast level §6" + onInteract.getRequiredLevel(cursor) + " §cto use this item!");
                            }
                        }
                    } else {
                        e.setCancelled(true);
                        p.sendMessage(RPG.getPrefix() + "§cThis is not a valid item to put in this inventory!");
                    }

                } else {
                    e.setCancelled(true);
                }

            } else {
                e.setCancelled(true);
            }

            // If player puts item in inventory armor slots

        }

        /*/
        else if(e.getSlotType().equals(InventoryType.SlotType.ARMOR)){
            if(e.getAction().equals(InventoryAction.PICKUP_SOME) || e.getAction().equals(InventoryAction.PICKUP_HALF) || e.getAction().equals(InventoryAction.PICKUP_ONE) || e.getAction().equals(InventoryAction.PICKUP_ALL)){

                if(Equipment.isArmor(e.getCurrentItem())){

                    PlayerEquipment peq = RPGPlayer.get(p).getPeq();

                    ItemStack current = e.getCurrentItem();

                    ItemStack is = new ItemStack(Material.PAPER, 1);
                    ItemMeta ism = is.getItemMeta();

                    switch(e.getCurrentItem().getType().toString()){

                        case "BOOTS":

                            ism.setDisplayName("§ccombat boots slot");
                            is.setItemMeta(ism);
                            peq.setCombat_boots(current);

                            break;

                        case "LEGGINGS":

                            ism.setDisplayName("§ccombat leggings slot");
                            is.setItemMeta(ism);
                            peq.setCombat_leggings(current);

                            break;

                        case "CHESTPLATE":

                            ism.setDisplayName("§ccombat chestplate slot");
                            is.setItemMeta(ism);
                            peq.setCombat_chestplate(current);

                            break;

                        case "HELMET":

                            ism.setDisplayName("§ccombat helmet slot");
                            is.setItemMeta(ism);
                            peq.setCombat_helmet(current);

                            break;

                    }

                }

            }
        }
         */

    }

}

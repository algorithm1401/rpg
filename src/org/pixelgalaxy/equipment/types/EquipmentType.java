package org.pixelgalaxy.equipment.types;

/**
 * Created by robin on 3/09/2018.
 */
public enum EquipmentType {

    ARMOR(".armor."),
    MELEE(".weapons."),
    BOW(".weapons."),
    ACCESSORY(".accessory");

    private String lorePath;

    EquipmentType(String lorePath) {
        this.lorePath = lorePath;
    }

}
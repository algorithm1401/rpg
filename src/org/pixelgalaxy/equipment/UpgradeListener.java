package org.pixelgalaxy.equipment;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.pixelgalaxy.RPG;

/**
 * Created by robin on 18/08/2018.
 */
public class UpgradeListener implements Listener {

    public static boolean isMaxLevel(ItemStack is) {

        if (is.hasItemMeta()) {

            if (is.getItemMeta().getLore() != null) {

                for (String s : is.getItemMeta().getLore()) {

                    if (ChatColor.stripColor(s).contains("Max Level")) {
                        return true;
                    }

                }

            }

        }

        return false;
    }

    @EventHandler
    public void onUpgrade(InventoryClickEvent e) {

        ItemStack cursor = e.getCursor();
        ItemStack current = e.getCurrentItem();

        if ((cursor != null && current != null)) {
            if (cursor.hasItemMeta() && current.hasItemMeta()) {
                // If the overlapping items are the same
                if (cursor.getType() == current.getType()) {

                    if (StringUtils.substringBefore(cursor.getItemMeta().getDisplayName(), "[") != null && StringUtils.substringBefore(cursor.getItemMeta().getDisplayName(), "[") != null) {
                        String itemNameCursor = StringUtils.substringBefore(cursor.getItemMeta().getDisplayName(), "[");
                        String itemNameCurrent = StringUtils.substringBefore(current.getItemMeta().getDisplayName(), "[");

                        // If the names are the same excluded the level
                        if (itemNameCurrent.equals(itemNameCursor)) {

                            if (!(isMaxLevel(cursor) || isMaxLevel(current))) {

                                // Levels of both items
                                Integer levelCurrent = Integer.valueOf(StringUtils.substringBetween(current.getItemMeta().getDisplayName(), "[Lv ", "]"));
                                Integer levelCursor = Integer.valueOf(StringUtils.substringBetween(cursor.getItemMeta().getDisplayName(), "[Lv ", "]"));

                                // Total amount of items for both items
                                int itemsCurrent = Equipment.getItemsOfLevel(levelCurrent) + Equipment.getCurrentUpgrades(current);
                                int itemsCursor = Equipment.getItemsOfLevel(levelCursor) + Equipment.getCurrentUpgrades(cursor);

                                e.setCancelled(true);
                                e.setCursor(new ItemStack(Material.AIR));
                                e.setCurrentItem(Equipment.upgradeWeapon(itemsCurrent + itemsCursor, current));
                            } else {
                                e.setCancelled(true);
                                e.getWhoClicked().sendMessage(RPG.getPrefix() + "§cOne of these items is already max level!");
                            }
                        }
                    }

                } else if (Dust.isDust(cursor) || Dust.isDust(current)) {

                    if (StringUtils.substringBefore(cursor.getItemMeta().getDisplayName(), "[") != null && Dust.isDust(current)) {

                        int dustAmount = current.getAmount();

                        // When dust is placed on item

                        Rarity dustR = Dust.getRarity(current);

                        Rarity itemR = Equipment.getItemRarity(cursor);

                        if (itemR.equals(dustR)) {

                            if (!(isMaxLevel(cursor))) {

                                Integer level = Integer.valueOf(StringUtils.substringBetween(cursor.getItemMeta().getDisplayName(), "[Lv ", "]"));
                                int itemsCursor = Equipment.getItemsOfLevel(level) + Equipment.getCurrentUpgrades(cursor);

                                ItemStack newWeapon = Equipment.upgradeWeapon(dustAmount + itemsCursor, cursor);
                                e.setCancelled(true);
                                e.setCursor(null);
                                e.setCurrentItem(newWeapon);

                            }
                        }

                    } else if (StringUtils.substringBefore(cursor.getItemMeta().getDisplayName(), "[") != null && Dust.isDust(cursor)) {

                        int dustAmount = cursor.getAmount();

                        if (Equipment.getItemRarity(current).equals(Dust.getRarity(cursor))) {

                            if (!(isMaxLevel(current))) {

                                Integer level = Integer.valueOf(StringUtils.substringBetween(current.getItemMeta().getDisplayName(), "[Lv ", "]"));
                                int itemsCursor = Equipment.getItemsOfLevel(level) + Equipment.getCurrentUpgrades(current);

                                e.setCancelled(true);
                                e.setCursor(null);
                                e.setCurrentItem(Equipment.upgradeWeapon(dustAmount + itemsCursor, current));

                            }

                        }
                    }
                }
            }
        }
    }

}

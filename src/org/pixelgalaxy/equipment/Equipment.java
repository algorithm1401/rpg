package org.pixelgalaxy.equipment;

import net.minecraft.server.v1_12_R1.NBTTagCompound;
import net.minecraft.server.v1_12_R1.NBTTagList;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.pixelgalaxy.RPG;
import org.pixelgalaxy.enchants.Enchant;
import org.pixelgalaxy.equipment.types.EquipmentType;
import org.pixelgalaxy.utils.EquipmentUtil;
import org.pixelgalaxy.utils.ItemStackBase;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Created by robin on 17/08/2018.
 */
public class Equipment {

    public static File file;
    public static FileConfiguration weaponsFile;
    private static String weaponsPath = "items.yml";

    public static List<Material> bowMaterials = Arrays.asList(Material.BOW);
    public static List<Material> meleeMaterials = Arrays.asList(Material.DIAMOND_SWORD, Material.IRON_SWORD, Material.GOLD_SWORD, Material.STONE_SWORD,
            Material.WOOD_SWORD, Material.STONE_HOE, Material.DIAMOND_AXE, Material.GOLD_HOE, Material.BLAZE_ROD, Material.LEVER, Material.STICK, Material.SHEARS,
            Material.BONE, Material.TRIPWIRE_HOOK, Material.END_ROD);
    public static List<Material> armorMaterials = Arrays.asList(Material.LEATHER_BOOTS, Material.LEATHER_LEGGINGS, Material.LEATHER_CHESTPLATE, Material.LEATHER_HELMET,
            Material.IRON_BOOTS, Material.IRON_LEGGINGS, Material.IRON_CHESTPLATE, Material.IRON_HELMET, Material.CHAINMAIL_BOOTS, Material.CHAINMAIL_LEGGINGS, Material.CHAINMAIL_CHESTPLATE,
            Material.CHAINMAIL_HELMET, Material.GOLD_BOOTS, Material.GOLD_LEGGINGS, Material.GOLD_CHESTPLATE, Material.GOLD_HELMET, Material.DIAMOND_BOOTS, Material.DIAMOND_LEGGINGS, Material.DIAMOND_CHESTPLATE,
            Material.DIAMOND_HELMET, Material.SHIELD);

    public static HashMap<Integer, Integer> requiredLevels = new HashMap<Integer, Integer>() {
        {
            put(1, 1);
            put(2, 3);
            put(3, 6);
            put(4, 12);
            put(5, 20);
            put(6, 30);
            put(7, 40);
        }
    };

    public static HashMap<Integer, Integer> forLevelMap = new HashMap<Integer, Integer>() {
        {
            put(1, 0);
            put(2, 2);
            put(3, 4);
            put(4, 10);
            put(5, 20);
            put(6, 50);
            put(7, 100);
            put(8, 100);
        }
    };

    public static HashMap<Integer, Integer> totalItemsForLevelMap = new HashMap<Integer, Integer>() {
        {
            put(1, 1);
            put(2, 3);
            put(3, 7);
            put(4, 17);
            put(5, 37);
            put(6, 87);
            put(7, 187);
            put(8, 187);

        }
    };

    public static double LOW_ATTACK_DAMAGE = RPG.getPlugin().getConfig().getDouble("Threshold.Attack_Damage.Low");
    public static double HIGH_ATTACK_DAMAGE = RPG.getPlugin().getConfig().getDouble("Threshold.Attack_Damage.High");

    public static double LOW_ATTACK_SPEED = RPG.getPlugin().getConfig().getDouble("Threshold.Attack_Speed.Low");
    public static double HIGH_ATTACK_SPEED = RPG.getPlugin().getConfig().getDouble("Threshold.Attack_Speed.Ligh");

    static DecimalFormat df = new DecimalFormat("#.##");
    public static final int MAX_LEVEL = 7;

    private String pathName;
    private String path;
    private String displayName;
    private Map<Enchant, Integer> enchantLevels;
    private EquipmentType equipmentType;
    private Rarity rarity;
    private int totalItems;
    private Material itemType;

    public Equipment(String pathName, int totalItems) {
        this.pathName = pathName;
        this.path = findWeaponPath(pathName);
        this.rarity = getWeaponRarity(path);
        this.displayName = rarity.getColor() + getWeaponName(path) + " §e[Lv " + getLevelForItems(totalItems) + "]";
        this.itemType = getWeaponMaterial(path);
        this.enchantLevels = getEnchantLevels(path, getLevelForItems(totalItems));
        this.equipmentType = getEquipmentType(itemType);
        this.totalItems = totalItems;
    }

    public static HashMap<Integer, Integer> getRequiredLevels() {
        return requiredLevels;
    }

    public String getPathName() {
        return pathName;
    }

    public String getPath() {
        return path;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Map<Enchant, Integer> getEnchantLevels() {
        return enchantLevels;
    }

    public EquipmentType getEquipmentType() {
        return equipmentType;
    }

    public Rarity getRarity() {
        return rarity;
    }

    public int getTotalItems() {
        return totalItems;
    }

    public Material getItemType() {
        return itemType;
    }

    public List<String> buildLore() {

        List<String> lore = new ArrayList<>();
        String line = "§7--------------------";

        lore.add("§7Rarity: " + getRarity().getColor() + getRarity().getName());
        for (Enchant en : getEnchantLevels().keySet()) {
            lore.add("§7" + en.getName() + " " + getEnchantLevels().get(en));
        }
        lore.add(line);

        int level = getLevelForItems(getTotalItems());

        switch (getEquipmentType()) {

            case BOW:

                lore.add(getAttackDamageString(getPath(), level));
                lore.add(line);

                break;

            case MELEE:

                lore.add(getAttackDamageString(getPath(), level));
                lore.add(getAttackSpeedString(getPath(), level));
                lore.add(line);

                break;

            case ARMOR:

                break;

        }

        int currentItems = getTotalItems() - getItemsOfLevel(getLevelForItems(getTotalItems()));
        if (getItemsOfLevel(MAX_LEVEL) <= getTotalItems()) {
            currentItems = 100;
        }
        lore.add(getUpgradePercentageBar(currentItems, level) + " §6" + df.format(getCurrentUpgradePercentage(currentItems, level) * 100) + "§6%");
        if (getItemsOfLevel(MAX_LEVEL) < getTotalItems()) {
            lore.add("§cMax Level");
        } else {
            lore.add("§7Upgrade Progress: " + currentItems + " / " + getItemsForLevel(level + 1));
        }
        lore.add("§eRequires Level " + getRequiredXPLvl(level));

        return lore;
    }

    private ItemStack changeColor(ItemStack is) {

        List<Material> leatherArmor = Arrays.asList(Material.LEATHER_BOOTS, Material.LEATHER_LEGGINGS, Material.LEATHER_CHESTPLATE, Material.LEATHER_HELMET);

        if (leatherArmor.contains(is.getType())) {
            LeatherArmorMeta meta = (LeatherArmorMeta) is.getItemMeta();
            List<Integer> colours = new ArrayList();
            for (String s : getItemsFile().getString(path + ".color").split(",")) {
                s = s.replaceAll(",", "");
                s = s.replaceAll("\\s+", "");
                colours.add(Integer.valueOf(s));
            }
            meta.setColor(Color.fromBGR(((Integer) colours.get(2)).intValue(), ((Integer) colours.get(1)).intValue(), ((Integer) colours.get(0)).intValue()));
            is.setItemMeta(meta);
        }
        return is;
    }

    public ItemStack build() {

        ItemStack is = new ItemStack(getItemType(), 1);
        is = toUnbreakable(is);
        ItemMeta ism = is.getItemMeta();
        ism.setDisplayName(getDisplayName());
        ism.setLore(buildLore());

        ism.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        ism.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        ism.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        ism.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        ism.addItemFlags(ItemFlag.HIDE_DESTROYS);

        is.setItemMeta(ism);

        is = changeColor(is);

        return is;
    }

    public EquipmentType getEquipmentType(Material m) {
        EquipmentType et = null;

        if (bowMaterials.contains(m)) {
            et = EquipmentType.BOW;
        } else if (meleeMaterials.contains(m)) {
            et = EquipmentType.MELEE;
        } else if (armorMaterials.contains(m)) {
            et = EquipmentType.ARMOR;
        }
        return et;
    }

    public static String findWeaponPath(String name) {

        for (String s : getItemsFile().getKeys(true)) {
            if (s.contains(name) && StringUtils.countMatches(s, ".") == 2) {
                return s;
            }
        }

        return null;
    }

    private static FileConfiguration getItemsFile() {
        if (weaponsFile == null) weaponsFile = YamlConfiguration.loadConfiguration(getFile());
        return weaponsFile;
    }

    private static File getFile() {
        if (file == null) {
            RPG.getPlugin().saveResource(weaponsPath, false);
            file = new File(RPG.getPlugin().getDataFolder(), weaponsPath);
        }
        return file;
    }

    private static Rarity getWeaponRarity(String path) {

        String rarityS = new String("");
        if (path != null) {
            if (path.contains("armor")) {
                rarityS = StringUtils.substringBefore(path, ".armor");
            } else if (path.contains("weapons")) {
                rarityS = StringUtils.substringBefore(path, ".weapons");
            }
            Rarity r = Rarity.valueOf(rarityS);

            return r;
        } else {
            return null;
        }
    }

    private static Material getWeaponMaterial(String path) {

        Material m = Material.valueOf(getItemsFile().getString(path + ".material"));

        return m;
    }

    private static String getWeaponName(String path) {

        String s = getItemsFile().getString(path + ".name");

        return s;
    }

    private static Double getWeaponDamage(String path, int level) {

        double damage = -1;

        if (getItemsFile().contains(path + ".damage")) {

            damage = getItemsFile().getDouble(path + ".damage." + level);

        }


        return damage;
    }

    public static EquipmentType getEquipmentType(String pathName) {

        Equipment equipment = new Equipment(pathName, 1);
        EquipmentType equipmentType = equipment.getEquipmentType();

        return equipmentType;

    }

    public static boolean isMelee(ItemStack is) {

        if (EquipmentUtil.parseDisplayToPath(is) != "") {

            String displayPath = EquipmentUtil.parseDisplayToPath(is);

            if (Equipment.getEquipmentType(displayPath).equals(EquipmentType.MELEE)) {

                return true;
            }
        }
        return false;
    }

    public static boolean isBow(ItemStack is) {

        if (EquipmentUtil.parseDisplayToPath(is) != "") {

            if (Equipment.getEquipmentType(EquipmentUtil.parseDisplayToPath(is)).equals(EquipmentType.BOW)) {

                return true;
            }
        }
        return false;
    }

    public static boolean isArmor(ItemStack is) {

        if (EquipmentUtil.parseDisplayToPath(is) != "") {

            if (Equipment.getEquipmentType(EquipmentUtil.parseDisplayToPath(is)).equals(EquipmentType.ARMOR)) {

                return true;
            }
        }
        return false;
    }

    private static Double getWeaponSpeed(String path, int level) {

        double damage = -1;

        if (getItemsFile().contains(path + ".speed")) {

            damage = getItemsFile().getDouble(path + ".speed." + level);

        }

        return damage;
    }

    private static int getRequiredXPLvl(int level) {

        int requiredLevel = requiredLevels.get(level);

        return requiredLevel;
    }

    private static boolean hasWeaponDamage(String path) {

        if (getWeaponDamage(path, 1) != -1) {
            return true;
        }
        return false;
    }

    private static boolean hasWeaponSpeed(String path) {

        if (getWeaponSpeed(path, 1) != -1) {
            return true;
        }
        return false;
    }

    public static String getAttackDamageString(String path, int level) {
        double damage = getWeaponDamage(path, level);
        String s = ("§eAttack Damage: ");

        if (damage > HIGH_ATTACK_DAMAGE) {
            s += "§c" + df.format(damage);
        } else if (damage < LOW_ATTACK_DAMAGE) {
            s += "§6" + df.format(damage);
        } else {
            s += "§a" + df.format(damage);
        }

        return s;
    }

    public static String getAttackSpeedString(String path, int level) {
        double speed = getWeaponSpeed(path, level);
        String s = ("§eAttack Speed: ");

        if (speed > HIGH_ATTACK_SPEED) {
            s += "§a" + df.format(speed);
        } else if (speed < LOW_ATTACK_SPEED) {
            s += "§6" + df.format(speed);
        } else {
            s += "§c" + df.format(speed);
        }

        return s;
    }

    public static ItemStack toUnbreakable(ItemStack is) {

        net.minecraft.server.v1_12_R1.ItemStack stack = CraftItemStack.asNMSCopy(is);
        NBTTagCompound tag = new NBTTagCompound(); //Create the NMS Stack's NBT (item data)
        tag.setBoolean("Unbreakable", true); //Set unbreakable value to true
        stack.setTag(tag); //Apply the tag to the item
        ItemStack is2 = CraftItemStack.asCraftMirror(stack); //Get the bukkit version of the stack

        return is2;
    }

    public static ItemStack addGlow(ItemStack item) {
        net.minecraft.server.v1_12_R1.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);
        NBTTagCompound tag = null;
        if (!nmsStack.hasTag()) {
            tag = new NBTTagCompound();
            nmsStack.setTag(tag);
        }
        if (tag == null) tag = nmsStack.getTag();
        NBTTagList ench = new NBTTagList();
        tag.set("ench", ench);
        nmsStack.setTag(tag);
        return CraftItemStack.asCraftMirror(nmsStack);
    }

    public static Map<Enchant, Integer> getEnchantLevels(String path, int itemLevel) {
        Map<Enchant, Integer> enchantLevelmap = new HashMap<>();

        path = path + ".enchants.";
        for (int i = 1; i < itemLevel + 1; i++) {

            String levelEnchantS = getItemsFile().getString(path + i);
            if (levelEnchantS.contains(",")) {
                for (String s : levelEnchantS.split(", ")) {

                    if (!enchantLevelmap.keySet().contains(Enchant.valueOf(s))) {
                        enchantLevelmap.put(Enchant.valueOf(s), 1);
                    } else {
                        int level = enchantLevelmap.get(Enchant.valueOf(s));
                        enchantLevelmap.remove(Enchant.valueOf(s));
                        enchantLevelmap.put(Enchant.valueOf(s), level + 1);
                    }

                }

            } else if (!enchantLevelmap.keySet().contains(Enchant.valueOf(levelEnchantS))) {
                enchantLevelmap.put(Enchant.valueOf(levelEnchantS), 1);

            } else {

                int level = enchantLevelmap.get(Enchant.valueOf(levelEnchantS));
                enchantLevelmap.remove(Enchant.valueOf(levelEnchantS));
                enchantLevelmap.put(Enchant.valueOf(levelEnchantS), level + 1);

            }

        }

        return enchantLevelmap;
    }

    public static List<String> getWeaponListStringList() {

        List<String> itemsList = new ArrayList<>();

        for (String key : getItemsFile().getKeys(true)) {

            if (StringUtils.countMatches(key, ".") == 2) {

                String weapon_name = new String("");
                if (key.contains("armor")) {
                    weapon_name = StringUtils.substringAfter(key, "armor.");
                } else if (key.contains("weapons")) {
                    weapon_name = StringUtils.substringAfter(key, "weapons.");
                }
                itemsList.add(weapon_name);

            }

        }
        return itemsList;
    }

    public static String getWeaponListString() {

        String list = RPG.getPrefix() + "§7";
        int amount = 0;
        for (String key : getItemsFile().getKeys(true)) {

            if (StringUtils.countMatches(key, ".") == 2) {

                if (amount > 0) {
                    list += "§7, " + Rarity.valueOf(StringUtils.substringBefore(key, ".")).getColor();
                }
                amount++;
                String weapon_name = new String("");
                if (key.contains("armor")) {
                    weapon_name = StringUtils.substringAfter(key, "armor.");
                } else if (key.contains("weapons")) {
                    weapon_name = StringUtils.substringAfter(key, "weapons.");
                }
                list += weapon_name;

            }

        }

        return list;
    }

    public static Rarity getItemRarity(ItemStack is) {

        String pathName = EquipmentUtil.parseDisplayToPath(is);

        if (findWeaponPath(pathName).contains("COMMON")) {
            return Rarity.COMMON;
        } else if (findWeaponPath(pathName).contains("RARE")) {
            return Rarity.RARE;
        } else if (findWeaponPath(pathName).contains("EPIC")) {
            return Rarity.EPIC;
        } else if (findWeaponPath(pathName).contains("LEGENDARY")) {
            return Rarity.LEGENDARY;
        }
        return null;
    }

    // Total level 1 items in another item
    public static int getItemsOfLevel(int level) {

        int items = totalItemsForLevelMap.get(level);

        return items;
    }

    // Get the level for an x amount of items
    public static int getLevelForItems(int items) {

        int level = new Integer(-1);

        for (int i = 1; i < MAX_LEVEL + 2; i++) {

            if (items >= getItemsOfLevel(i)) {
                level = i;
            } else {
                return level;
            }

        }

        if (level == 8) {
            level = 7;
        }

        return level;
    }

    public static int getItemsForLevel(int level) {

        return forLevelMap.get(level);
    }

    public static int getCurrentUpgrades(ItemStack currentItem) {

        for (String s : currentItem.getItemMeta().getLore()) {

            if (ChatColor.stripColor(s).contains("Upgrade Progress")) {

                int currentUpgrades = Integer.valueOf(s.split("\\s+")[2]);

                return currentUpgrades;

            }

        }

        return 0;

    }

    public static double getCurrentUpgradePercentage(int currentUpgrades, int level) {

        double percentage = (double) currentUpgrades / (double) getItemsForLevel(level + 1);

        return percentage;
    }

    // Returns string with a bar that displays how much you have upgraded your weapon to next level
    public static String getUpgradePercentageBar(int currentItems, int level) {
        StringBuilder bar = new StringBuilder("&a");
        int count = (int) (getCurrentUpgradePercentage(currentItems, level) * 100.0);
        int total = 0;

        while (total < 50) {
            if (count == 0) {
                bar.append("&7|");
            } else if (count == 1) {
                bar.append("&2|&7");
            } else {
                bar.append("|");
            }
            count -= 2;
            total++;
        }

        return ChatColor.translateAlternateColorCodes('&', "&8[" + bar.toString() + "&8]");
    }

    public static String convertDisplayToPath(String displayname) {

        String display = StringUtils.substringBefore(displayname, "[");
        display = ChatColor.stripColor(display);
        int count = 0;
        String itemName = new String("");
        for (String s : display.split("\\s+")) {

            if (count != 0) {
                itemName += "_";
            }
            count++;
            itemName += s;
        }

        itemName = itemName.replaceAll("-", "_");
        itemName = itemName.replaceAll("'", "");
        itemName = itemName.toLowerCase();

        return itemName;
    }

    public static ItemStack upgradeWeapon(int newItems, ItemStack current) {

        String display = StringUtils.substringBefore(current.getItemMeta().getDisplayName(), "[");
        display = ChatColor.stripColor(display);
        int count = 0;
        String itemName = new String("");
        for (String s : display.split("\\s+")) {

            if (count != 0) {
                itemName += "_";
            }
            count++;
            itemName += s;
        }

        itemName = itemName.replaceAll("-", "_");
        itemName = itemName.replaceAll("'", "");

        return new Equipment(itemName.toLowerCase(), newItems).build();

    }

    public static void saveItem(Player p, ItemStack item, String saveName) {

        if (item != null && item.hasItemMeta()) {

            if (item.getItemMeta().getLore() != null && item.getItemMeta().getDisplayName() != null) {

                ItemStack[] isArray = new ItemStack[1];
                isArray[0] = item;

                String isArrayString = ItemStackBase.itemStackArrayToBase64(isArray);

                RPG.SQL.mysql.update("UPDATE player_equipment SET " + saveName + " = '" + isArrayString + "' WHERE uuid='" + p.getUniqueId().toString() + "';");


            }
        }
    }

    public static void removeItem(Player p, String saveName) {

        RPG.SQL.mysql.update("UPDATE player_equipment SET " + saveName + "_name = " + "" + " WHERE uuid='" + p.getUniqueId() + "';");
        RPG.SQL.mysql.update("UPDATE player_equipment SET " + saveName + "_lore = " + "" + " WHERE uuid='" + p.getUniqueId() + "';");

    }

    public static ItemStack getItem(Player p, String saveName, int slot) {

        String base64 = new String("");

        ResultSet rs = RPG.SQL.mysql.query("SELECT * FROM `player_equipment` WHERE uuid='" + p.getUniqueId().toString() + "'");

        try {
            while (rs.next()) {
                base64 = rs.getString(saveName);
            }
        } catch (SQLException ex) {
            RPG.getPlugin().getLogger().info("Error executing a query: " + ex.getErrorCode());
        }

        ItemStack is = new ItemStack(Material.PAPER, 1);
        ItemMeta ism = is.getItemMeta();
        ism.setDisplayName("§c" + CustomEquipment.slotWithItem.get(slot).replaceAll("_", " ") + " slot");
        is.setItemMeta(ism);

        if (!(base64.equals(""))) {

            try {
                is = ItemStackBase.itemStackArrayFromBase64(base64)[0];
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        if (is.getType().equals(Material.AIR)) {
            is = new ItemStack(Material.PAPER, 1);
            is.setItemMeta(ism);

        }

        return is;
    }

}

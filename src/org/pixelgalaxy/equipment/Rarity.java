package org.pixelgalaxy.equipment;

/**
 * Created by robin on 17/08/2018.
 */
public enum Rarity {

    COMMON("§7", "Common"),
    RARE("§a", "Rare"),
    EPIC("§5", "Epic"),
    LEGENDARY("§c", "Legendary");

    private String color;
    private String name;

    Rarity(String color, String name) {
        this.color = color;
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public String getName() {
        return getColor() + name;
    }
}

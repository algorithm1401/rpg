package org.pixelgalaxy.equipment;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

/**
 * Created by robin on 12/09/2018.
 */
public class Dust {

    public static ItemStack getDust(Rarity r, int amount) {

        ItemStack dust = new ItemStack(Material.SULPHUR, 1);
        ItemMeta dustm = dust.getItemMeta();
        dustm.setDisplayName(r.getColor() + r.getName() + " Dust");
        dustm.setLore(Arrays.asList(

                "§7Rarity: " + r.getColor() + r.getName(),
                "§6Upgrade any " + ChatColor.stripColor(r.getName()) + " item",
                "§6with a " + ChatColor.stripColor(r.getName()) + " Dust"

        ));
        dust.setItemMeta(dustm);
        dust.setAmount(amount);

        return dust;
    }

    public static boolean isDust(ItemStack item) {

        int amount = item.getAmount();
        if (item.equals(getDust(Rarity.COMMON, amount)) || item.equals(getDust(Rarity.RARE, amount)) || item.equals(getDust(Rarity.EPIC, amount)) || item.equals(getDust(Rarity.LEGENDARY, amount))) {

            return true;

        }
        return false;
    }

    public static Rarity getRarity(ItemStack dust) {

        return Rarity.valueOf(ChatColor.stripColor(StringUtils.substringAfter(dust.getItemMeta().getLore().get(0), "Rarity: ")).toUpperCase());
    }

}

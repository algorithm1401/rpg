package org.pixelgalaxy.npcs;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.pixelgalaxy.RPG;
import org.pixelgalaxy.RPGPlayer;
import org.pixelgalaxy.equipment.CustomEquipment;
import org.pixelgalaxy.equipment.Equipment;
import org.pixelgalaxy.level.XP;
import org.pixelgalaxy.player_objects.PlayerBounties;
import org.pixelgalaxy.utils.Chance;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by robin on 13/09/2018.
 */
public class Bounties implements Listener {

    public static File file;
    public static FileConfiguration bountiesFile;
    private static String bountiesPath = "bounties.yml";

    private static Map<UUID, List<String>> startedBountyIDS = new HashMap<>();
    private static int nextPass;

    private static Hologram hologram;

    private static final List<Integer> ACTIVE_BOUNTY_SLOTS = Arrays.asList(9, 10, 11, 12, 13, 14, 15, 16, 17);

    public static FileConfiguration getBountiesFile() {
        if (bountiesFile == null) bountiesFile = YamlConfiguration.loadConfiguration(getFile());
        return bountiesFile;
    }

    private static File getFile() {
        if (file == null) {
            RPG.getPlugin().saveResource(bountiesPath, false);
            file = new File(RPG.getPlugin().getDataFolder(), bountiesPath);
        }
        return file;
    }

    // Bounty that has already been claimed
    private static ItemStack getStartedBountyStack(int bountyID, int amount) {

        ItemStack bounty = new ItemStack(Material.ITEM_FRAME, 1);
        ItemMeta bountym = bounty.getItemMeta();

        int maxAmount = getBountyAmount(bountyID);
        int currentAmount = amount;

        double progress = currentAmount / maxAmount;
        if (progress >= 1) {
            progress = 1;
            bountym.setDisplayName(ChatColor.translateAlternateColorCodes('&', getBountyName(bountyID)) + " §6[Completed]");
            Equipment.addGlow(bounty);

        } else {

            bountym.setDisplayName(getBountyName(bountyID));

        }

        bountym.setLore(buildBountyLore(bountyID, currentAmount, maxAmount));
        bounty.setItemMeta(bountym);

        return bounty;
    }

    public static Integer getBountyAmount(int bountyID) {

        int bountyAmount = getBountiesFile().getInt("bounties." + bountyID + ".amount");
        return bountyAmount;
    }

    public static String getBountyName(int bountyID) {
        String bountyName = "§a§l" + getBountiesFile().getString("bounties." + bountyID + ".name");

        return bountyName;
    }

    public static List<String> buildBountyLore(int bountyID, int currentAmount, int maxAmount) {

        List<String> lore = new ArrayList<>();

        if (currentAmount > maxAmount) {
            currentAmount = maxAmount;
        }

        String description = getBountiesFile().getString("bounties." + bountyID + ".description");
        description = description.replaceAll("%amount%", "§6" + currentAmount + "§r");
        description = description.replaceAll("%max_amount%", "§6" + maxAmount + "§r");
        description = ChatColor.translateAlternateColorCodes('&', description);

        lore.add(description);

        String progressBar = getUpgradePercentageBar(currentAmount, maxAmount);

        lore.add(progressBar);

        String reward = new String("§aReward: §e" + getBountiesFile().getInt("bounties." + bountyID + ".reward") + " XP");

        lore.add(reward);

        return lore;
    }

    public static String getUpgradePercentageBar(int currentAmount, int maxAmount) {
        StringBuilder bar = new StringBuilder("&a");
        int count = (int) (((double) currentAmount / (double) maxAmount) * 100);
        int total = 0;

        while (total < 50) {
            if (count == 0) {
                bar.append("&7|");
            } else if (count == 1) {
                bar.append("&2|&7");
            } else {
                bar.append("|");
            }
            count -= 2;
            total++;
        }

        return ChatColor.translateAlternateColorCodes('&', "&8[" + bar.toString() + "&8]");
    }

    // Bounties that already have been started
    public static List<ItemStack> getBountyStacks(Player p) {

        List<ItemStack> bountyStacks = new ArrayList<>();

        PlayerBounties pb = RPGPlayer.get(p).getPb();
        if (pb != null) {
            List<Integer> bountyTypes = pb.getBountyIDS();
            List<Integer> bountyProgress = pb.getBountyProgress();

            if (bountyTypes.size() > 0) {
                for (int i = 0; i < bountyTypes.size(); i++) {

                    bountyStacks.add(getStartedBountyStack(bountyTypes.get(i), bountyProgress.get(i)));

                }
            }
            return bountyStacks;
        } else {
            return new ArrayList<>();
        }

    }

    public static ItemStack getClaimableBounty(int bountyID) {

        ItemStack bounty = new ItemStack(Material.PAINTING);
        ItemMeta bountym = bounty.getItemMeta();
        bountym.setDisplayName(ChatColor.translateAlternateColorCodes('&', getBountyName(bountyID)));

        String description = getBountiesFile().getString("bounties." + bountyID + ".description_before");
        description = description.replaceAll("%max_amount%", "§6" + getBountyAmount(bountyID) + "§r");
        description = ChatColor.translateAlternateColorCodes('&', description);

        List<String> lore = new ArrayList<>();
        lore.add(description);

        String reward = new String("§aReward: §e" + getBountiesFile().getInt("bounties." + bountyID + ".reward") + " XP");
        lore.add(reward);

        bountym.setLore(lore);
        bounty.setItemMeta(bountym);

        return bounty;
    }

    public static Map<UUID, List<String>> getStartedBountyIDS() {
        return startedBountyIDS;
    }

    // Bounties that are claimable
    public static List<ItemStack> getAvailableBounties(Player p) {

        List<ItemStack> availableBounties = new ArrayList<>();

        List<String> claimableIds = new ArrayList<>();

        if (getStartedBountyIDS().get(p.getUniqueId()) == null) {
            getStartedBountyIDS().put(p.getUniqueId(), new ArrayList<>());
        }

        for (int bountyID : getBountiesFile().getIntegerList("random")) {

            if (!getStartedBountyIDS().get(p.getUniqueId()).contains(bountyID + "")) {
                claimableIds.add(bountyID + "");
            } else {
                claimableIds.add("-1");
            }

        }

        for (String bountyID : claimableIds) {
            if (bountyID != "-1") {
                availableBounties.add(getClaimableBounty(Integer.valueOf(bountyID)));
            } else {
                availableBounties.add(new ItemStack(Material.AIR));
            }
        }

        return availableBounties;
    }

    public static void addStartedBounty(Player p, int bountyID) {

        List<String> bountyIDS = new ArrayList<>();

        if (getStartedBountyIDS().keySet().contains(p.getUniqueId())) {
            bountyIDS = getStartedBountyIDS().get(p.getUniqueId());
        }

        bountyIDS.add(bountyID + "");
        getStartedBountyIDS().put(p.getUniqueId(), bountyIDS);

    }

    public static void open(Player p) {

        Inventory inv = Bukkit.createInventory(null, 27, "§a§lBounties");

        List<ItemStack> claimableStacks = getAvailableBounties(p);

        for (int i = 0; i < claimableStacks.size(); i++) {
            inv.setItem(i, claimableStacks.get(i));
        }

        List<ItemStack> bountyStacks = getBountyStacks(p);

        if (bountyStacks.size() > 0) {
            for (int i = 0; i < bountyStacks.size(); i++) {

                inv.setItem(ACTIVE_BOUNTY_SLOTS.get(i), bountyStacks.get(i));

            }

            List<ItemStack> buttons = getButtons(p);
            for (int i = 18; i < buttons.size() + 18; i++) {
                inv.setItem(i, buttons.get(i - 18));
            }
        }


        p.openInventory(inv);

    }

    public static void updateActiveBounties(Inventory inv, Player p) {

        List<ItemStack> bountyStacks = getBountyStacks(p);

        if (bountyStacks.size() > 0) {
            for (int i = 0; i < bountyStacks.size(); i++) {

                inv.setItem(ACTIVE_BOUNTY_SLOTS.get(i), bountyStacks.get(i));

            }

            List<ItemStack> buttons = getButtons(p);
            for (int i = 18; i < buttons.size() + 18; i++) {
                inv.setItem(i, buttons.get(i - 18));
            }

        } else {
            for (int i = 9; i < inv.getSize(); i++) {
                inv.setItem(i, null);
            }
        }

    }

    private static ItemStack getTurnInStack() {

        ItemStack turnIN = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 5);
        ItemMeta turnINm = turnIN.getItemMeta();
        turnINm.setDisplayName("§eTurn In");
        turnINm.setLore(Arrays.asList("§7Complete this bounty", "§7and collect the rewards."));
        turnIN.setItemMeta(turnINm);

        return turnIN;
    }

    private static ItemStack getAbandonStack() {

        ItemStack abandon = new ItemStack(Material.BARRIER);
        ItemMeta abandonm = abandon.getItemMeta();
        abandonm.setDisplayName("§cAbandon");
        abandonm.setLore(Arrays.asList("§7This will permanently", "§7discard this bounty."));
        abandon.setItemMeta(abandonm);

        return abandon;
    }

    public static List<ItemStack> getButtons(Player p) {

        PlayerBounties pb = RPGPlayer.get(p).getPb();

        List<ItemStack> buttons = new ArrayList<>();

        for (int i = 0; i < pb.getBountyIDS().size(); i++) {

            int bountyID = pb.getBountyIDS().get(i);
            if (((double) pb.getBountyProgress().get(i) / (double) getBountyAmount(bountyID)) >= 1) {
                buttons.add(getTurnInStack());
            } else {
                buttons.add(getAbandonStack());
            }

        }

        return buttons;

    }

    private static int inventorySpaceFree(Player p) {
        int amount = 0;
        for (ItemStack itemStack : p.getInventory()) {
            if (itemStack == null) {
                amount++;
            }
        }
        return amount;
    }

    private static void givePlayerBountyBottles(Player p, int slot) {

        PlayerBounties pb = RPGPlayer.get(p).getPb();
        int bountyID = pb.getBountyIDS().get(slot - 18);
        int rewardXP = getBountiesFile().getInt("bounties." + bountyID + ".reward");

        int amount25;
        int amount50;
        int amount100;
        int amount250;

        amount250 = (int) Math.floor((double) rewardXP / 250.0);
        rewardXP -= amount250 * 250;
        amount100 = (int) Math.floor((double) rewardXP / 100.0);
        rewardXP -= amount100 * 100;
        amount50 = (int) Math.floor((double) rewardXP / 50.0);
        rewardXP -= amount50 * 50.0;
        amount25 = (int) Math.floor((double) rewardXP / 25.0);
        rewardXP -= amount25 * 25;

        int slotsNeeded = 0;
        if (amount25 > 0) {
            slotsNeeded++;
        }

        if (amount50 > 0) {
            slotsNeeded++;
        }

        if (amount100 > 0) {
            slotsNeeded++;
        }

        if (amount250 > 0) {
            slotsNeeded++;
        }

        if (inventorySpaceFree(p) >= slotsNeeded) {

            ItemStack xp25 = XP.getBottle(25);
            xp25.setAmount(amount25);
            ItemStack xp50 = XP.getBottle(50);
            xp50.setAmount(amount50);
            ItemStack xp100 = XP.getBottle(100);
            xp100.setAmount(amount100);
            ItemStack xp250 = XP.getBottle(250);
            xp250.setAmount(amount250);

            p.getInventory().addItem(xp25);
            p.getInventory().addItem(xp50);
            p.getInventory().addItem(xp100);
            p.getInventory().addItem(xp250);
            p.sendMessage(RPG.getPrefix() + "§aYou have claimed a bounty!");

            pb.removeActiveBounty(p, slot);
            pb.removeProgress(p, slot);
        }

    }

    @EventHandler
    public void onInvClick(InventoryClickEvent e) {

        Inventory inv = e.getClickedInventory();
        int slot = e.getSlot();
        Player p = (Player) e.getWhoClicked();

        if (inv.getTitle().equalsIgnoreCase("§a§lBounties")) {

            ItemStack clicked = e.getCurrentItem();

            switch (clicked.getType()) {

                case PAINTING:

                    int bountyID = getBountiesFile().getIntegerList("random").get(slot);
                    if (RPGPlayer.get(p).getPb().addActiveBounty(p, bountyID)) {

                        addStartedBounty(p, bountyID);
                        e.setCurrentItem(null);
                        updateActiveBounties(inv, p);
                        p.updateInventory();

                    }
                    e.setCancelled(true);
                    break;

                case ITEM_FRAME:
                    e.setCancelled(true);
                    break;

                case BARRIER:

                    e.setCancelled(true);

                    RPGPlayer.get(p).getPb().removeActiveBounty(p, slot);

                    break;

                case STAINED_GLASS_PANE:

                    e.setCancelled(true);
                    if (!CustomEquipment.isInventoryFull(p)) {

                        givePlayerBountyBottles(p, slot);

                    } else {
                        p.sendMessage(RPG.getPrefix() + "§cYou can't turn in this bounty because you don't have any space left in your inventory!");
                    }

                    break;

            }

        }

    }

    private static int getMaxBountyID() {

        int max = 1;
        for (String key : getBountiesFile().getKeys(true)) {

            if (StringUtils.countMatches(key, ".") == 1) {

                max = Integer.valueOf(StringUtils.substringAfter(key, "bounties."));

            }

        }
        return max;
    }

    private static int getNextpass(Date currentDate) {

        int previousPass;
        if (currentDate.getHours() >= 6 && currentDate.getHours() < 12) {
            previousPass = 12;
        } else if (currentDate.getHours() >= 12 && currentDate.getHours() < 18) {
            previousPass = 18;
        } else if (currentDate.getHours() >= 18 && currentDate.getHours() < 24) {
            previousPass = 24;
        } else {
            previousPass = 6;
        }

        return previousPass;
    }

    private static Date getDifferenceFormat(Date currentDate) {

        Date newDate = new Date();

        int seconds = 60 - currentDate.getSeconds();
        int minutes = 60 - currentDate.getMinutes() - 1;

        newDate.setSeconds(seconds);
        newDate.setMinutes(minutes);
        newDate.setHours(nextPass - currentDate.getHours() - 1);

        return newDate;
    }

    public static void setNewBounties() {
        List<Integer> bountyIDS = new ArrayList<>();
        int maxBounty = getMaxBountyID();
        for (int i = 0; i < 9; i++) {
            bountyIDS.add(Chance.getRandom(1, maxBounty));
        }

        // Give new random bounties
        getBountiesFile().set("random", bountyIDS);
        try {
            getBountiesFile().save(getFile());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static String dateToString(Date date) {
        String until = new String(date.getHours() + "h " + date.getMinutes() + "m " + date.getSeconds() + "s");
        return until;
    }

    public static void updateTimeLeft(Date date) {

        hologram.removeLine(2);
        hologram.appendTextLine("§6Refreshes in: §c" + dateToString(getDifferenceFormat(date)));

    }

    public static void update(Date date) {
        startedBountyIDS.clear();
        setNewBounties();

        nextPass = getNextpass(date);

        updateTimeLeft(date);
    }

    public static void startRefreshing() {

        hologram = HologramsAPI.createHologram(RPG.getPlugin(), new Location(Bukkit.getWorld("Warzone"), -4951.338, 72.417, 4451.545));
        hologram.insertTextLine(0, "§6Earn XP bottles by completing");
        hologram.insertTextLine(1, "§6fun and challenging tasks!");
        hologram.insertTextLine(2, "");

        if (getBountiesFile().get("random") == null) {
            setNewBounties();
        }
        nextPass = getNextpass(new Date());

        new BukkitRunnable() {

            @Override
            public void run() {

                DateFormat dateformat = new SimpleDateFormat("HH:mm:ss");
                Date date = new Date();

                if (dateformat.format(date) == "06:00:01" || dateformat.format(date) == "12:00:01" || dateformat.format(date) == "18:00:01" || dateformat.format(date) == "00:00:01") {

                    update(new Date());

                } else {

                    updateTimeLeft(date);

                }

            }
        }.runTaskTimer(RPG.getPlugin(), 0, 20);

    }

}

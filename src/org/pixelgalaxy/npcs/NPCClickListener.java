package org.pixelgalaxy.npcs;

import io.netty.util.internal.StringUtil;
import net.citizensnpcs.api.event.NPCRightClickEvent;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.pixelgalaxy.RPG;
import org.pixelgalaxy.crystals.Opener;
import org.pixelgalaxy.equipment.Equipment;
import org.pixelgalaxy.equipment.Rarity;
import org.pixelgalaxy.level.XP;
import org.pixelgalaxy.utils.EquipmentUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by robin on 10/09/2018.
 */
public class NPCClickListener implements Listener {

    private static final int NPC_ID = RPG.getPlugin().getConfig().getInt("npc.xp_trader");

    private static final List<Integer> itemSlots = Arrays.asList(0, 1, 2, 3, 9, 10, 11, 12, 18, 19, 20, 21, 27, 28, 29, 30, 36, 37, 38, 39, 45, 46, 47, 48);
    private static final List<Integer> xpSlots = Arrays.asList(5, 6, 7, 8, 14, 15, 16, 17, 23, 24, 25, 26, 32, 33, 34, 35, 41, 42, 43, 44, 50, 51, 52, 53);

    private static List<Player> inXpTrader = new ArrayList<>();

    @EventHandler
    public void onClose(InventoryCloseEvent e) {

        Player p = (Player) e.getPlayer();

        if (inXpTrader.contains(p) && ChatColor.stripColor(e.getInventory().getTitle()).equalsIgnoreCase("Trade in items for XP")) {

            for (int slot : itemSlots) {

                if (e.getInventory().getItem(slot) != null) {
                    p.getInventory().addItem(e.getInventory().getItem(slot));
                }

            }

            inXpTrader.remove(p);

        }

    }

    public static void resetItems(Inventory inv, List<Integer> slots, int slotRemoved) {

        inv.setItem(slotRemoved, null);

        int lastSlot = 0;
        for (int currentSlot : slots) {

            if (currentSlot > slotRemoved) {

                if (inv.getItem(currentSlot) != null) {
                    lastSlot = currentSlot;
                }

            }

        }

        if (lastSlot > slotRemoved) {
            inv.setItem(slotRemoved, inv.getItem(lastSlot));
            inv.setItem(lastSlot, null);
        }

    }

    @EventHandler
    public void onInventoryInsert(InventoryClickEvent e) {

        InventoryAction a = e.getAction();
        int slot = e.getSlot();
        Inventory inv = e.getClickedInventory();
        Player p = (Player) e.getWhoClicked();

        // If player clicks on item in inventory
        if (inv.getTitle().equals("container.inventory") && ChatColor.stripColor(e.getInventory().getTitle()).equalsIgnoreCase("Trade in items for XP")) {

            inXpTrader.add(p);

            if (a.equals(InventoryAction.PICKUP_ONE) || a.equals(InventoryAction.PICKUP_ALL) || a.equals(InventoryAction.PICKUP_SOME) || a.equals(InventoryAction.PICKUP_HALF)) {

                ItemStack clicked = e.getCurrentItem();
                ItemStack clickedCopy = clicked.clone();
                clickedCopy.setAmount(1);
                if (Opener.crystalItemstacks.contains(clickedCopy) || Equipment.isMelee(clicked) || Equipment.isArmor(clicked) || Equipment.isBow(clicked)) {

                    for (int slotAvailable : itemSlots) {

                        if (e.getInventory().getItem(slotAvailable) == null) {

                            e.getInventory().setItem(slotAvailable, clicked);
                            e.setCursor(null);
                            e.setCurrentItem(null);
                            break;

                        }

                    }

                    int amount = 0;

                    switch (EquipmentUtil.getItemRarity(clicked)) {

                        case COMMON:
                            amount = 25;
                            break;

                        case RARE:
                            amount = 50;
                            break;

                        case EPIC:
                            amount = 100;
                            break;

                        case LEGENDARY:
                            amount = 250;
                            break;

                    }

                    ItemStack xpBottle = XP.getBottle(amount);

                    for (int slotAvailable : xpSlots) {

                        if (e.getInventory().getItem(slotAvailable) == null) {

                            e.getInventory().setItem(slotAvailable, xpBottle);
                            break;

                        }

                    }

                } else {
                    e.setCancelled(true);
                    p.sendMessage(RPG.getPrefix() + "§cYou can't trade in this item!");
                }

            } else {
                e.setCancelled(true);
            }
        } else if (ChatColor.stripColor(inv.getTitle()).equalsIgnoreCase("Trade in items for XP")) {

            // If clicked slot is xp bottle
            if (xpSlots.contains(slot) && e.getCurrentItem() != null) {

                ItemStack bottle = e.getCurrentItem();

                e.setCancelled(true);

                Rarity rToTake = null;
                int xp = Integer.valueOf(StringUtils.substringBefore(ChatColor.stripColor(bottle.getItemMeta().getDisplayName()), " "));

                switch (xp) {

                    case 25:
                        rToTake = Rarity.COMMON;
                        break;

                    case 50:
                        rToTake = Rarity.RARE;
                        break;

                    case 100:
                        rToTake = Rarity.EPIC;
                        break;

                    case 250:
                        rToTake = Rarity.LEGENDARY;
                        break;

                }

                int itemSlotTaken = -1;

                for (int itemSlot : itemSlots) {

                    if (inv.getItem(itemSlot) != null) {

                        Rarity r = Equipment.getItemRarity(inv.getItem(itemSlot));

                        if (r.equals(rToTake) && (itemSlotTaken == -1)) {

                            itemSlotTaken = itemSlot;
                        }
                    }
                }

                resetItems(inv, itemSlots, itemSlotTaken);
                resetItems(inv, xpSlots, slot);

                p.getInventory().addItem(bottle);

            } else if (itemSlots.contains(slot)) {

                ItemStack xpBottle = new ItemStack(Material.AIR);

                switch (Equipment.getItemRarity(e.getCurrentItem())) {

                    case COMMON:
                        xpBottle = XP.getBottle(25);
                        break;

                    case RARE:
                        xpBottle = XP.getBottle(50);
                        break;

                    case EPIC:
                        xpBottle = XP.getBottle(100);
                        break;

                    case LEGENDARY:
                        xpBottle = XP.getBottle(250);
                        break;

                }

                p.getInventory().addItem(inv.getItem(slot));
                e.setCancelled(true);
                resetItems(inv, itemSlots, slot);

                for (int xpSlot : xpSlots) {

                    if (inv.getItem(xpSlot).equals(xpBottle)) {

                        resetItems(inv, xpSlots, xpSlot);
                        break;

                    }

                }

            } else {
                e.setCancelled(true);
            }

        }
    }

    @EventHandler
    public void onNPCClick(NPCRightClickEvent e) {

        if (e.getNPC().getId() == NPC_ID) {

            XP_Trader.openInventory(e.getClicker());

        }

    }

}

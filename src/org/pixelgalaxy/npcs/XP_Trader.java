package org.pixelgalaxy.npcs;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.List;

/**
 * Created by robin on 10/09/2018.
 */
public class XP_Trader {

    public static void openInventory(Player p){

        Inventory inv = Bukkit.createInventory(null, 54, "§7Trade in items for §aXP");

        List<Integer> paneSlots = Arrays.asList(4, 13, 22, 31, 40, 49);
        List<String> lore = Arrays.asList( "",
                                               "§7<------------",
                                               "§6Put items there",
                                               "",
                                               "§7------------>",
                                               "§aGet XP there");

        ItemStack pane = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 5);
        ItemMeta panem = pane.getItemMeta();
        panem.setDisplayName("§aXP Trader");
        panem.setLore(lore);
        pane.setItemMeta(panem);

        for(int slot : paneSlots){

            inv.setItem(slot, pane);

        }

        p.openInventory(inv);

    }

}

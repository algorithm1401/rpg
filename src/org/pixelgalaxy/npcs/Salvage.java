package org.pixelgalaxy.npcs;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.pixelgalaxy.RPG;
import org.pixelgalaxy.crystals.Crystal;
import org.pixelgalaxy.crystals.Opener;
import org.pixelgalaxy.equipment.Dust;
import org.pixelgalaxy.equipment.Equipment;
import org.pixelgalaxy.equipment.Rarity;

import java.util.*;

/**
 * Created by robin on 12/09/2018.
 */
public class Salvage implements Listener {

    private static final List<Integer> salvageInputSlots = Arrays.asList(11, 12, 13, 14, 15);
    private static Map<Player, List<ItemStack>> playerInputs = new HashMap<>();
    private static Map<Player, Integer> toGive = new HashMap<>();

    public static void updateSalvageResult(Player p, Inventory inv) {

        INPUT_STATE input_state = INPUT_STATE.VALID_EMPTY;
        List<ItemStack> inputs = getPlayerInputs().get(p);

        if (inputs != null && inputs.size() > 0) {

            Rarity r = Equipment.getItemRarity(inputs.get(0));

            int amount = 0;

            for (ItemStack item : inputs) {

                if (Equipment.getItemRarity(item).equals(r)) {

                    amount += item.getAmount();

                } else {
                    input_state = INPUT_STATE.INVALID;
                    break;
                }

            }

            if (input_state != INPUT_STATE.INVALID) {

                int dustAmount = (int) Math.floor(amount / 5);

                if (dustAmount > 0) {

                    input_state = INPUT_STATE.VALID;
                    toGive.put(p, dustAmount);

                }

            }

        } else {
            input_state = INPUT_STATE.VALID_EMPTY;
        }

        ItemStack isGood = new ItemStack(Material.AIR);

        switch (input_state) {
            case VALID:
                isGood = new ItemStack(Material.INK_SACK, 1, (short) 10);
                break;

            case VALID_EMPTY:
                isGood = new ItemStack(Material.INK_SACK, 1, (short) 8);
                break;

            case INVALID:
                isGood = new ItemStack(Material.INK_SACK, 1, (short) 1);
                break;

        }

        inv.setItem(31, isGood);

    }

    public static ItemStack getPaneStack() {

        ItemStack pane = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15);
        ItemMeta panem = pane.getItemMeta();
        panem.setDisplayName("§b§lSalvage");
        panem.setLore(Arrays.asList("§7Put 5 items in the middle", "§7slots of the same rarity", "§7to dust you items!"));
        pane.setItemMeta(panem);

        return pane;
    }

    public static void open(Player p) {

        Inventory salvage = Bukkit.createInventory(null, 45, "§b§lSalvage");

        for (int i = 0; i < salvage.getSize(); i++) {

            if ((!salvageInputSlots.contains(i)) && i != 31) {

                salvage.setItem(i, getPaneStack());

            }

        }

        updateSalvageResult(p, salvage);

        p.openInventory(salvage);

    }

    public static void addItemToMap(Player p, ItemStack item) {

        if (getPlayerInputs().containsKey(p)) {

            List<ItemStack> items = getPlayerInputs().get(p);
            items.add(item);
            getPlayerInputs().remove(p);
            getPlayerInputs().put(p, items);

        }

    }

    public static void removeItemFromMap(Player p, ItemStack item) {

        if (getPlayerInputs().containsKey(p)) {

            List<ItemStack> items = getPlayerInputs().get(p);
            items.remove(item);
            getPlayerInputs().remove(p);
            getPlayerInputs().put(p, items);

        }
    }

    public static Map<Player, List<ItemStack>> getPlayerInputs() {
        return playerInputs;
    }

    public static Map<Player, Integer> getToGive() {
        return toGive;
    }

    @EventHandler
    public void onClose(InventoryCloseEvent e) {

        Player p = (Player) e.getPlayer();

        if (e.getInventory().getTitle().equalsIgnoreCase("§b§lSalvage")) {

            if (playerInputs.containsKey(p)) {

                for (ItemStack item : playerInputs.get(p)) {

                    p.getInventory().addItem(item);

                }

                playerInputs.remove(p);
                toGive.remove(p);

            }

        }

    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {

        Inventory inv = e.getClickedInventory();
        Player p = (Player) e.getWhoClicked();

        int slot = e.getSlot();

        ItemStack clicked = e.getCurrentItem();

        if (clicked != null && clicked.getType() != Material.AIR) {

            // If the clicked inventory is the salvage (take out items)
            if (inv.getTitle().equalsIgnoreCase("§b§lSalvage") && p.getOpenInventory().getTopInventory().getTitle().equalsIgnoreCase("§b§lSalvage")) {

                if (salvageInputSlots.contains(slot)) {

                    removeItemFromMap(p, clicked);
                    p.getInventory().addItem(clicked);
                    e.setCancelled(true);
                    inv.setItem(slot, null);
                    updateSalvageResult(p, inv);
                } else if (slot == 31) {
                    e.setCancelled(true);

                    if (clicked.getDurability() == 8) {
                        p.sendMessage(RPG.getPrefix() + "§cYou have to put in atleast 5 items of the same rarity!");
                    } else if (clicked.getDurability() == 10) {

                        Rarity r = Equipment.getItemRarity(getPlayerInputs().get(p).get(0));
                        ItemStack dust = Dust.getDust(r, 1);
                        dust.setAmount(getToGive().get(p));
                        p.getInventory().addItem(dust);

                        int toRemove = dust.getAmount() * 5;
                        for (int itemSlot : salvageInputSlots) {

                            ItemStack is = p.getOpenInventory().getTopInventory().getItem(itemSlot);
                            int left = -(toRemove - is.getAmount());
                            if (left > 0) {

                                removeItemFromMap(p, is);
                                is.setAmount(left);
                                p.getOpenInventory().getTopInventory().setItem(itemSlot, is);
                                addItemToMap(p, is);

                                break;

                            } else {
                                p.getOpenInventory().getTopInventory().setItem(itemSlot, null);
                                removeItemFromMap(p, is);
                                toRemove -= is.getAmount();
                            }

                        }

                        toGive.remove(p);
                        updateSalvageResult(p, p.getOpenInventory().getTopInventory());

                    } else if (clicked.getDurability() == 1) {
                        p.sendMessage(RPG.getPrefix() + "§cYou have to put in items of the same rarity!");
                    }

                } else {
                    e.setCancelled(true);
                }

            }

            // If the clicked inventory is own (put in items)
            else if (inv.getTitle().equalsIgnoreCase("container.inventory") && p.getOpenInventory().getTopInventory().getTitle().equalsIgnoreCase("§b§lSalvage")) {


                ItemStack clickedCopy = clicked.clone();
                clickedCopy.setAmount(1);
                if (Equipment.isArmor(clicked) || Equipment.isMelee(clicked) || Equipment.isBow(clicked) || Opener.crystalItemstacks.contains(clickedCopy)) {

                    int slotWithPlace = -1;
                    for (int s : salvageInputSlots) {

                        if (p.getOpenInventory().getTopInventory().getItem(s) == null) {
                            slotWithPlace = s;
                            break;
                        }

                    }

                    if (slotWithPlace != -1) {

                        if (playerInputs.containsKey(p)) {

                            addItemToMap(p, clicked);

                        } else {
                            List<ItemStack> initInputs = new ArrayList<>();
                            initInputs.add(clicked);
                            playerInputs.put(p, initInputs);

                        }

                        p.getOpenInventory().getTopInventory().setItem(slotWithPlace, clicked);
                        p.setItemOnCursor(null);
                        e.setCurrentItem(null);
                        updateSalvageResult(p, p.getOpenInventory().getTopInventory());

                    } else {
                        e.setCancelled(true);
                        p.sendMessage(RPG.getPrefix() + "§cThe salvage inventory is full!");
                    }
                } else {
                    e.setCancelled(true);
                }

            }
        }
    }

}

enum INPUT_STATE {

    VALID_EMPTY,
    VALID,
    INVALID

}

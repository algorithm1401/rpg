package org.pixelgalaxy.npcs;

import io.netty.util.internal.StringUtil;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BlockStateMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.pixelgalaxy.RPG;
import org.pixelgalaxy.equipment.CustomEquipment;
import org.pixelgalaxy.equipment.Equipment;

import java.io.File;
import java.util.*;

/**
 * Created by robin on 11/09/2018.
 */
public class Shopkeeper implements Listener {

    private static Map<Player, String> currentyBrowsing = new HashMap<>();
    private static Map<Player, String> lastBrowsed = new HashMap<>();

    public static File file;
    public static FileConfiguration shopFile;
    private static String shopPath = "shops.yml";

    private static FileConfiguration getShopFile() {
        if (shopFile == null) shopFile = YamlConfiguration.loadConfiguration(getFile());
        return shopFile;
    }

    private static File getFile() {
        if (file == null) {
            RPG.getPlugin().saveResource(shopPath, false);
            file = new File(RPG.getPlugin().getDataFolder(), shopPath);
        }
        return file;
    }

    //lore.add("§a\uD83E\uDC46 §9Click to view");

    public static int getAmountInInventory(Player p, ItemStack isToCheck) {

        int amount = 0;

        for (ItemStack is : p.getInventory().getContents()) {

            if (is != null && !is.getItemMeta().hasLore() && !is.getItemMeta().hasDisplayName()) {

                if (is.getType().equals(isToCheck.getType()) && is.getDurability() == isToCheck.getDurability()) {

                    amount += is.getAmount();

                }
            }
        }

        return amount;
    }

    public static ItemStack getPreviousStack() {

        ItemStack previous = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 14);
        ItemMeta previousm = previous.getItemMeta();
        previousm.setDisplayName("§c§lPrevious Page");
        previousm.setLore(Arrays.asList("§a§l← §6Previous page"));
        previous.setItemMeta(previousm);

        return previous;
    }

    public static ItemStack getNextStack() {

        ItemStack previous = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 5);
        ItemMeta previousm = previous.getItemMeta();
        previousm.setDisplayName("§a§lNext Page");
        previousm.setLore(Arrays.asList("§a§l→ §6Next page"));
        previous.setItemMeta(previousm);

        return previous;
    }

    public static ItemStack getExitStack() {

        ItemStack exit = new ItemStack(Material.BARRIER);
        ItemMeta exitm = exit.getItemMeta();
        exitm.setDisplayName("§4Exit menu");
        exit.setItemMeta(exitm);

        return exit;
    }

    public static ItemStack getItemStack(String path, int number, Player p) {

        String newPath = path + "." + number + ".";

        Material m = Material.valueOf(getShopFile().getString(newPath + ".type"));
        int id = 0;

        if (getShopFile().get(newPath + ".id") != null) {
            id = getShopFile().getInt(newPath + ".id");
        }

        ItemStack is = new ItemStack(m, 1, (short) id);
        ItemMeta ism = is.getItemMeta();
        ism.setDisplayName(ChatColor.translateAlternateColorCodes('&', getShopFile().getString(newPath + ".display") + " §7(x" + getShopFile().getInt(newPath + ".amount") + "§7)"));

        List<String> lore = new ArrayList<>();
        lore.add("§9Buy: §e" + getShopFile().get(newPath + ".buy").toString() + " §7(Left click)");
        if (getShopFile().get(newPath + ".sell") != null) {

            lore.add("§9Sell: §e" + getShopFile().get(newPath + ".sell").toString() + " §7(Right click)");
            double sellALl = (((getAmountInInventory(p, is) * getShopFile().getDouble(newPath + ".sell")) / getShopFile().getDouble(newPath + ".amount")));
            if (p.getInventory().getItemInOffHand().getType().equals(is.getType())) {
                sellALl -= getShopFile().getDouble(newPath + ".sell") * p.getInventory().getItemInOffHand().getAmount() / getShopFile().getDouble(newPath + ".amount");
            }
            lore.add("§9Sell All: §e" + sellALl + " §7(Shift + Right click)");

        } else {
            lore.add("§cNot sellable");
        }
        ism.setLore(lore);
        is.setItemMeta(ism);

        return is;
    }

    public static ItemStack getRedirectItemStack(int number) {

        String newPath = "start.items." + number + ".";
        int id = 0;
        if (getShopFile().get(newPath + id) != null) {
            id = getShopFile().getInt(newPath + "id");
        }

        Material m = Material.valueOf(getShopFile().getString(newPath + "type"));

        if (m == null) {
            m = Material.STONE;
        }

        ItemStack redirect = new ItemStack(m, 1, (short) id);
        ItemMeta redirectm = redirect.getItemMeta();
        redirectm.setDisplayName(ChatColor.translateAlternateColorCodes('&', getShopFile().getString(newPath + "display")));
        redirectm.setLore(Arrays.asList("§a➜ §6Click to view"));
        redirect.setItemMeta(redirectm);

        return redirect;
    }

    public static void openStartMenu(Player p) {

        p.sendMessage(RPG.getPrefix() + "§6Opening shop start menu...");

        currentyBrowsing.put(p, "start");

        Inventory inv = Bukkit.createInventory(null, getShopFile().getInt("start.size"), ChatColor.translateAlternateColorCodes('&', getShopFile().getString("start.title")));

        for (String s : getShopFile().getKeys(true)) {

            if (s.contains("start.items.") && StringUtils.countMatches(s, ".") == 2) {

                int itemNumber = Integer.valueOf(StringUtils.substringAfter(s, "start.items."));

                inv.setItem(itemNumber - 1, getRedirectItemStack(itemNumber));

            }

        }

        p.openInventory(inv);

    }

    private static void openNewMenu(Player p, String inv, int page) {

        Inventory newInv = Bukkit.createInventory(null, getShopFile().getInt(inv + ".size"), ChatColor.translateAlternateColorCodes('&', getShopFile().getString(inv + ".title")) + " §7(Page " + page + ")");

        for (String s : getShopFile().getKeys(true)) {

            if (s.contains(inv + ".items.") && StringUtils.countMatches(s, ".") == 2) {

                int itemNumber = Integer.valueOf(StringUtils.substringAfter(s, inv + ".items."));

                if (itemNumber <= (page * 36) && (itemNumber >= ((page - 1) * 36) + 1 || itemNumber == 0)) {

                    newInv.setItem(itemNumber - ((page - 1) * 36) - 1, getItemStack(inv + ".items", itemNumber, p));

                    if (!currentyBrowsing.get(p).contains(inv + page)) {
                        updatePlayerBrowsing(p, inv, page);
                    }

                }
            }
        }

        updatePage(p, page);

        newInv.setItem(48, getPreviousStack());
        newInv.setItem(49, getExitStack());
        newInv.setItem(50, getNextStack());

        p.openInventory(newInv);

    }

    private static boolean isShopInventory(Inventory inv) {

        for (String s : getShopFile().getKeys(true)) {

            if (StringUtils.countMatches(s, ".") == 1 && s.contains("title")) {

                String clickedTitle = StringUtils.substringBefore(inv.getTitle(), " §7(Page");
                String configTitle = ChatColor.translateAlternateColorCodes('&', getShopFile().getString(s));

                if (clickedTitle.equalsIgnoreCase(configTitle)) {
                    return true;
                }

            }

        }

        return false;
    }

    private static String getInventoryString(Inventory inv) {

        String inventoryName = new String("");

        for (String s : getShopFile().getKeys(true)) {

            if (StringUtils.countMatches(s, ".") == 1 && s.contains("title")) {

                String clickedTitle = StringUtils.substringBefore(inv.getTitle(), " (Page");
                if (clickedTitle.equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', getShopFile().getString(s)))) {

                    inventoryName = StringUtils.substringBefore(s, ".title");

                    return inventoryName;

                }

            }

        }
        return inventoryName;
    }

    private static void updatePage(Player p, int newPage) {

        String current = currentyBrowsing.get(p);
        String lastNumber = current.charAt(current.length() - 1) + "";
        current = StringUtils.substringBeforeLast(current, lastNumber + "");
        current += newPage;
        currentyBrowsing.remove(p);
        currentyBrowsing.put(p, current);
    }

    private static void updatePlayerBrowsing(Player p, String inv, int page) {
        if (isBrowsingShop(p)) {
            String current = currentyBrowsing.get(p);
            if (current.contains(inv)) {
                current = StringUtils.substringBeforeLast(current, "." + inv);
            }
            current += "." + inv + page;
            currentyBrowsing.remove(p);
            currentyBrowsing.put(p, current);
            lastBrowsed.remove(p);
            lastBrowsed.put(p, current);
        }
    }

    public static boolean isBrowsingShop(Player p) {
        if (currentyBrowsing.containsKey(p)) {
            return true;
        }
        return false;
    }

    public static boolean canAfford(Player p, int price) {

        if (RPG.getEco().getBalance(p) >= price) {
            return true;
        }
        return false;
    }

    @EventHandler
    public void invOpen(InventoryOpenEvent e) {

        Player p = (Player) e.getPlayer();

        if (isShopInventory(e.getInventory()) && !e.getInventory().getTitle().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', getShopFile().getString("start.title")))) {


            String inventoryName = getInventoryString(e.getInventory());

            if (lastBrowsed.containsKey(p) && lastBrowsed.get(p) != null) {
                String path = lastBrowsed.get(p) + "." + inventoryName;
                if (path.charAt(path.length() - 1) == '.') {

                    path = StringUtils.substringBeforeLast(path, ".");
                }
                currentyBrowsing.put(p, path);

            } else {
                currentyBrowsing.put(p, inventoryName);
            }

        } else {
            if (lastBrowsed.containsKey(p)) {
                lastBrowsed.remove(p);
            }
        }

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void invClose(InventoryCloseEvent e) {

        Player p = (Player) e.getPlayer();
        if (isBrowsingShop(p)) {
            currentyBrowsing.remove(p);
        }

    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onInventoryClick(InventoryClickEvent e) {

        Player p = (Player) e.getWhoClicked();
        InventoryAction a = e.getAction();

        if (ChatColor.stripColor(e.getClickedInventory().getTitle()).equalsIgnoreCase("Shop - Click items to browse")) {
            e.setCancelled(true);
        }

        if (e.getClickedInventory().getTitle() != "container.inventory") {

            int slot = e.getSlot() + 1;

            if (isBrowsingShop(p)) {

                e.setCancelled(true);
                if (currentyBrowsing.get(p).equalsIgnoreCase("start")) {

                    openNewMenu(p, getShopFile().getString("start.items." + (e.getSlot() + 1) + ".redirect"), 1);

                } else if (currentyBrowsing.get(p).contains("start")) {

                    String currentInventory = StringUtils.substringAfterLast(currentyBrowsing.get(p), ".");
                    String lastNumber = currentInventory.charAt(currentInventory.length() - 1) + "";
                    int page = Integer.valueOf(lastNumber);
                    currentInventory = StringUtils.substringBefore(currentInventory, "" + page);

                    int itemNumber = (slot + ((page - 1) * 36));

                    if (a.equals(InventoryAction.PICKUP_ALL) && !e.getClickedInventory().getTitle().equalsIgnoreCase("container.inventory")) {

                        if (slot != 49 && slot != 50 && slot != 51) {
                            int price = (getShopFile().getInt(currentInventory + ".items." + itemNumber + ".buy"));

                            if (canAfford(p, price)) {

                                if (!CustomEquipment.isInventoryFull(p)) {

                                    RPG.getEco().withdrawPlayer(p, price);
                                    ItemStack isToGive = getItemStack(currentInventory + ".items", itemNumber, p);
                                    String display = isToGive.getItemMeta().getDisplayName();
                                    isToGive.setItemMeta(null);
                                    isToGive.setAmount(getShopFile().getInt(currentInventory + ".items." + itemNumber + ".amount"));

                                    if (getShopFile().getString(currentInventory + ".items." + itemNumber + ".spawner") != null) {
                                        BlockStateMeta bsm = (BlockStateMeta) isToGive.getItemMeta();
                                        CreatureSpawner cs = (CreatureSpawner) bsm.getBlockState();

                                        cs.setSpawnedType(EntityType.valueOf(getShopFile().getString(currentInventory + ".items." + itemNumber + ".spawner")));
                                        bsm.setBlockState(cs);
                                        isToGive.setItemMeta(bsm);
                                    }

                                    p.getInventory().addItem(isToGive);
                                    p.sendMessage(RPG.getPrefix() + "§aYou bought: " + display);

                                    openNewMenu(p, currentInventory, page);

                                } else {
                                    p.sendMessage(RPG.getPrefix() + "§cYou don't have enough inventory space to buy this item!");
                                }
                            } else {
                                p.sendMessage(RPG.getPrefix() + "§cYou can't afford this item!");
                            }
                        } else {

                            switch (slot) {

                                case 49:

                                    e.setCancelled(true);
                                    if ((page - 1) > 0) {
                                        openNewMenu(p, currentInventory, page - 1);
                                    }

                                    break;

                                case 50:
                                    e.setCancelled(true);
                                    p.closeInventory();
                                    break;

                                case 51:
                                    e.setCancelled(true);
                                    if (getShopFile().get(currentInventory + ".items." + ((page * 36) + 1)) != null) {
                                        openNewMenu(p, currentInventory, page + 1);
                                    }
                                    break;

                            }

                        }

                    } else if (a.equals(InventoryAction.PICKUP_HALF)) {

                        if (getShopFile().get(currentInventory + ".items." + itemNumber + ".sell") != null) {

                            int toGive = (getShopFile().getInt(currentInventory + ".items." + itemNumber + ".sell"));

                            ItemStack isToTake = getItemStack(currentInventory + ".items", itemNumber, p);
                            String display = isToTake.getItemMeta().getDisplayName();
                            Material m = isToTake.getType();

                            if (!p.getInventory().getItemInOffHand().getType().equals(m)) {

                                if (getAmountInInventory(p, isToTake) >= getShopFile().getInt(currentInventory + ".items." + itemNumber + ".amount")) {

                                    isToTake.setAmount(getShopFile().getInt(currentInventory + ".items." + itemNumber + ".amount"));

                                    isToTake.setItemMeta(null);

                                    p.getInventory().removeItem(isToTake);

                                    RPG.getEco().depositPlayer(p, toGive);

                                    p.sendMessage(RPG.getPrefix() + "§aYou received " + toGive + "$ for selling: " + display + "§a.");

                                    openNewMenu(p, currentInventory, page);

                                } else {

                                    p.sendMessage(RPG.getPrefix() + "§cYou don't have: " + isToTake.getItemMeta().getDisplayName() + "§c!");

                                }

                            } else {
                                p.sendMessage(RPG.getPrefix() + "§cYou can't sell this item if it is in your second hand!");
                            }
                        } else {
                            p.sendMessage(RPG.getPrefix() + "§cThis item is not sellable!");
                        }
                    } else if (a.equals(InventoryAction.MOVE_TO_OTHER_INVENTORY)) {

                        if (getShopFile().get(currentInventory + ".items." + itemNumber + ".sell") != null) {

                            ItemStack isToTake = getItemStack(currentInventory + ".items", itemNumber, p);
                            String display = isToTake.getItemMeta().getDisplayName();
                            Material m = isToTake.getType();

                            int amount = getAmountInInventory(p, isToTake);

                            //int toGive = ((getShopFile().getInt(currentInventory + ".items." + itemNumber + ".sell") * amount) / 64);
                            double sellALl = (((getAmountInInventory(p, isToTake) * getShopFile().getDouble(currentInventory + ".items." + itemNumber + ".sell")) / getShopFile().getDouble(currentInventory + ".items." + itemNumber + ".amount")));

                            if (!p.getInventory().getItemInOffHand().getType().equals(m)) {

                                if (getAmountInInventory(p, isToTake) > 0) {

                                    isToTake.setAmount(amount);
                                    isToTake.setItemMeta(null);

                                    p.getInventory().removeItem(isToTake);
                                    RPG.getEco().depositPlayer(p, sellALl);

                                    p.sendMessage(RPG.getPrefix() + "§aYou received " + sellALl + "$ for selling: " + StringUtils.substringBefore(display, "(") + "§7(x" + amount + "§7)§a.");

                                    openNewMenu(p, currentInventory, page);
                                } else {
                                    p.sendMessage(RPG.getPrefix() + "§cYou don't have any " + StringUtils.substringBefore(isToTake.getItemMeta().getDisplayName(), "(") + "§c!");
                                }
                            } else {
                                p.sendMessage(RPG.getPrefix() + "§cYou can't sell this item if it is in your second hand!");
                            }
                        } else {
                            p.sendMessage(RPG.getPrefix() + "§cThis item is not sellable!");
                        }
                    }
                }
            }
        } else if (isBrowsingShop(p) && e.getClickedInventory().getTitle().equals("container.inventory")) {
            e.setCancelled(true);
        }
    }
}


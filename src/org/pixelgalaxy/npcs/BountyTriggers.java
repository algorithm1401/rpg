package org.pixelgalaxy.npcs;

import io.netty.util.internal.StringUtil;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.pixelgalaxy.RPGPlayer;
import org.pixelgalaxy.combat.melee.PlayerMeleeHit;
import org.pixelgalaxy.disparity.ChestUpdater;
import org.pixelgalaxy.disparity.Disparity;
import org.pixelgalaxy.player_objects.PlayerBounties;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by robin on 15/09/2018.
 */
public class BountyTriggers implements Listener {

    public static Map<Location, List<Player>> lootedChests = new HashMap<>();

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onKill(EntityDeathEvent e) {

        LivingEntity len = e.getEntity();

        Player p = PlayerMeleeHit.getPlayerFromHitEntity(e.getEntity());
        EntityType type = len.getType();

        if (p != null) {
            addProgress(p, BountyTrigger.KILL, type.toString(), 1);
        }

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onDamage(EntityDamageByEntityEvent e) {

        if (e.getEntity() instanceof LivingEntity && e.getDamager() instanceof Player) {
            LivingEntity len = (LivingEntity) e.getEntity();

            Player p = (Player) e.getDamager();
            EntityType type = len.getType();
            addProgress(p, BountyTrigger.DAMAGE, type.toString(), (int) e.getDamage());
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onLootchestOpen(PlayerInteractEvent e) {

        Player p = e.getPlayer();

        if (e.getClickedBlock().getType().equals(Material.CHEST)) {

            for (ChestUpdater chestUpdater : ChestUpdater.getActive()) {

                Location lootChestLoc = chestUpdater.getLoc();

                if (e.getClickedBlock().getLocation().equals(lootChestLoc)) {

                    if (lootedChests.get(lootChestLoc) == null) {

                        BountyTriggers.lootedChests.put(lootChestLoc, new ArrayList<>());

                    }
                    if (!lootedChests.get(lootChestLoc).contains(p)) {

                        List<Player> playersLooted = new ArrayList<>();
                        if (lootedChests.get(lootChestLoc).size() != 0) {
                            playersLooted = lootedChests.get(lootChestLoc);
                        }

                        playersLooted.add(p);
                        lootedChests.remove(lootChestLoc);
                        lootedChests.put(lootChestLoc, playersLooted);
                        addProgress(p, BountyTrigger.OPEN, "CHEST", 1);

                    }
                    break;
                }

            }

        }

    }

    public static void addProgress(Player p, BountyTrigger trigger, String type, int amount) {

        PlayerBounties pb = RPGPlayer.get(p).getPb();

        List<Integer> bountyProgress = pb.getBountyProgress();
        List<Integer> newProgress = new ArrayList<>();

        for (int i = 0; i < pb.getBountyIDS().size(); i++) {

            String triggerS = Bounties.getBountiesFile().getString("bounties." + pb.getBountyIDS().get(i) + ".trigger");
            if (!triggerS.contains(":")) {

                if (BountyTrigger.valueOf(triggerS).equals(trigger) && type.equalsIgnoreCase(Bounties.getBountiesFile().getString("bounties." + pb.getBountyIDS().get(i) + ".type"))) {

                    int progress = bountyProgress.get(i);
                    progress += amount;
                    newProgress.add(progress);

                } else {
                    newProgress.add(bountyProgress.get(i));
                }

            } else {

                String typeS = StringUtils.substringAfter(triggerS, ":");
                triggerS = StringUtils.substringBefore(triggerS, ":");

                if (BountyTrigger.valueOf(triggerS).equals(trigger) && type.equalsIgnoreCase(Bounties.getBountiesFile().getString("bounties." + pb.getBountyIDS().get(i) + ".type"))) {

                    if (typeS != "FIST") {

                        if (p.getInventory().getItemInMainHand().getType().toString().contains(typeS)) {

                            int progress = bountyProgress.get(i);
                            progress += amount;
                            newProgress.add(progress);

                        } else {
                            newProgress.add(bountyProgress.get(i));
                        }

                    } else {

                        if (p.getInventory().getItemInMainHand() == null) {

                            int progress = bountyProgress.get(i);
                            progress += amount;
                            newProgress.add(progress);

                        } else {
                            newProgress.add(bountyProgress.get(i));
                        }

                    }

                } else {
                    newProgress.add(bountyProgress.get(i));
                }

            }
        }

        pb.setBountyProgress(newProgress);

    }

}


enum BountyTrigger {

    KILL,
    DAMAGE,
    OPEN,
    PARTICIPATE;

}
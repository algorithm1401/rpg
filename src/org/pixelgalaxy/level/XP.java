package org.pixelgalaxy.level;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

/**
 * Created by robin on 19/08/2018.
 */
public class XP {

    public static ItemStack getBottle(int amount){
        ItemStack bottle = new ItemStack(Material.EXP_BOTTLE, 1);
        ItemMeta bottleM = bottle.getItemMeta();
        bottleM.setDisplayName("§6" + amount + " Experience");
        bottleM.setLore(Arrays.asList(

                "§7Right click to add",
                "§7" + amount + " experience",
                "§7to your player"

        ));

        bottle.setItemMeta(bottleM);

        return bottle;
    }

}

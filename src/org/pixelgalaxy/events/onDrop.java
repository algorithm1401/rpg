package org.pixelgalaxy.events;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.pixelgalaxy.RPG;

/**
 * Created by robin on 10/09/2018.
 */
public class onDrop implements Listener {

    @EventHandler
    public void onDrop(InventoryClickEvent e) {

        Player p = (Player) e.getWhoClicked();

        if (ChatColor.stripColor(p.getOpenInventory().getTopInventory().getTitle()).contains("Equipment")) {

            if (e.getAction().equals(InventoryAction.DROP_ALL_CURSOR) || e.getAction().equals(InventoryAction.DROP_ALL_SLOT) || e.getAction().equals(InventoryAction.DROP_ONE_CURSOR) || e.getAction().equals(InventoryAction.DROP_ONE_SLOT)) {

                e.setCancelled(true);
                p.sendMessage(RPG.getPrefix() + "§cYou can't drop items while you're in your equipment inventory!");

            }
        }

    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent e) {

        if (e.getItemDrop().equals(onInventoryCrafting.getCraftItem()) || e.getItemDrop().equals(onInventoryCrafting.getSkillsItem()) || e.getItemDrop().equals(onInventoryCrafting.getEquipmentItem())) {
            e.getItemDrop().remove();
        }

    }

}

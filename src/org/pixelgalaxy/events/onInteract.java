package org.pixelgalaxy.events;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.pixelgalaxy.RPG;
import org.pixelgalaxy.RPGPlayer;
import org.pixelgalaxy.equipment.Equipment;

import java.util.Arrays;
import java.util.List;

/**
 * Created by robin on 16/09/2018.
 */
public class onInteract implements Listener {

    public static int getRequiredLevel(ItemStack is) {

        if (is.hasItemMeta() && is.getItemMeta().getLore() != null) {

            for (String s : is.getItemMeta().getLore()) {

                s = ChatColor.stripColor(s);

                if (s.contains("Requires Level")) {

                    int level = Integer.valueOf(StringUtils.substringAfter(s, "Requires Level "));
                    return level;
                }

            }

        }
        return -1;
    }

    private static List<Material> blackListed_blocks = Arrays.asList(Material.ANVIL, Material.WORKBENCH, Material.ENCHANTMENT_TABLE);

    @EventHandler
    public void onInv(BlockPlaceEvent e) {

        if (blackListed_blocks.contains(e.getBlock().getType())) {
            e.setCancelled(true);
            e.getPlayer().sendMessage(RPG.getPrefix() + "§cYou're not allowed to place this block!");
        }

    }

    List<InventoryType> blackListed_inv = Arrays.asList(InventoryType.ANVIL, InventoryType.WORKBENCH, InventoryType.ENCHANTING);

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onRestricedInvOpen(InventoryOpenEvent e) {

        Player p = (Player) e.getPlayer();
        if (blackListed_inv.contains(e.getInventory().getType()) && !onInventoryCrafting.inWorkBench.contains(p)) {
            e.setCancelled(true);
        }

    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {

        Player p = e.getPlayer();

        if (e.getItem() != null) {

            if (e.getItem().getType() != null) {

                if (Equipment.armorMaterials.contains(e.getItem().getType()) || Equipment.meleeMaterials.contains(e.getItem().getType()) || Equipment.bowMaterials.contains(e.getItem().getType())) {

                    if (Equipment.isArmor(e.getItem()) || Equipment.isMelee(e.getItem()) || Equipment.isBow(e.getItem())) {

                        if (RPGPlayer.get(p).getPl().getPlayerLevel() < getRequiredLevel(e.getItem())) {
                            e.setCancelled(true);
                            p.sendMessage(RPG.getPrefix() + "§cYou need to be atleast level §6" + getRequiredLevel(e.getItem()) + " §cto use this item!");
                        }

                    }
                }
            }
        }
    }

}

package org.pixelgalaxy.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLevelChangeEvent;
import org.pixelgalaxy.RPGPlayer;
import org.pixelgalaxy.player_objects.PlayerLevel;
import org.pixelgalaxy.tags.Tags;

/**
 * Created by robin on 13/08/2018.
 */
public class onLevelUp implements Listener {

    @EventHandler
    public void onLevelUP(PlayerLevelChangeEvent e){

        Player p = e.getPlayer();
        RPGPlayer rpgPlayer = RPGPlayer.get(p);

        if (e.getNewLevel() <= PlayerLevel.MAX_LEVEL) {

            if (e.getNewLevel() > e.getOldLevel()) {

                int difference = e.getNewLevel() - e.getOldLevel();

                for (int i = 0; i < difference; i++) {

                    rpgPlayer.getPs().addSkillPoints(2);
                    Tags.update(p);

                }

            }
        }else{
            PlayerLevel pl = rpgPlayer.getPl();
            pl.setExperience(pl.getXpOfLevel(PlayerLevel.MAX_LEVEL + 1) - 1);
            PlayerLevel.setPlayerXPBar(p);
        }
    }

}

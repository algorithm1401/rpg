package org.pixelgalaxy.events;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.inventory.ItemStack;
import org.pixelgalaxy.RPGPlayer;
import org.pixelgalaxy.equipment.CustomEquipment;
import org.pixelgalaxy.equipment.Equipment;

/**
 * Created by robin on 3/10/2018.
 */
public class onShieldEquip implements Listener {


    private void checkForShield(ItemStack offhand, Player p) {
        if (offhand.getType().equals(Material.SHIELD)) {

            if (Equipment.isArmor(offhand)) {

                RPGPlayer.get(p).getPeq().setShield(offhand);
                CustomEquipment.updatePlayerArmor(p);

            } else {
                RPGPlayer.get(p).getPeq().setShield(new ItemStack(Material.AIR));
                CustomEquipment.updatePlayerArmor(p);
            }

        } else {
            RPGPlayer.get(p).getPeq().setShield(new ItemStack(Material.AIR));
            CustomEquipment.updatePlayerArmor(p);
        }
    }

    @EventHandler
    public void onShieldPutInOffhand(InventoryClickEvent e) {

        Player p = (Player) e.getWhoClicked();

        if (e.getSlot() == 40) {

            if (e.getAction().equals(InventoryAction.PICKUP_ALL) || e.getAction().equals(InventoryAction.PICKUP_ONE) || e.getAction().equals(InventoryAction.PICKUP_SOME) || e.getAction().equals(InventoryAction.PICKUP_HALF)) {
                RPGPlayer.get(p).getPeq().setShield(new ItemStack(Material.AIR));
                CustomEquipment.updatePlayerArmor(p);
            } else {

                checkForShield(e.getCursor(), p);

            }
        }

    }

    @EventHandler
    public void onShieldHold(PlayerSwapHandItemsEvent e) {

        Player p = e.getPlayer();
        ItemStack offhand = e.getOffHandItem();

        checkForShield(offhand, p);

    }

}

package org.pixelgalaxy.events;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.pixelgalaxy.RPG;
import org.pixelgalaxy.RPGPlayer;
import org.pixelgalaxy.armor.ArmorEquipEvent;
import org.pixelgalaxy.equipment.CustomEquipment;
import org.pixelgalaxy.equipment.Equipment;
import org.pixelgalaxy.player_objects.PlayerCombat;
import org.pixelgalaxy.player_objects.PlayerEnchantments;
import org.pixelgalaxy.player_objects.PlayerEquipment;
import org.pixelgalaxy.utils.EquipmentUtil;

import java.util.Arrays;

/**
 * Created by robin on 6/09/2018.
 */
public class onArmorEquip implements Listener {

    @EventHandler
    public void onWear(ArmorEquipEvent e) {

        ItemStack newArmor = new ItemStack(Material.AIR);
        if (e.getNewArmorPiece() != null) {
            newArmor = e.getNewArmorPiece();
        }
        Player p = e.getPlayer();

        PlayerEquipment peq = RPGPlayer.get(p).getPeq();

        Material m = newArmor.getType();

        if (Equipment.armorMaterials.contains(m)) {

            int level = EquipmentUtil.getLevelFromWeapon(newArmor);
            int items = Equipment.getItemsOfLevel(level);
            items += Equipment.getCurrentUpgrades(newArmor);
            ItemStack toSet = new Equipment(Equipment.convertDisplayToPath(newArmor.getItemMeta().getDisplayName()), items).build();

            if (RPGPlayer.get(p).getPl().getPlayerLevel() >= onInteract.getRequiredLevel(toSet)) {

                String type = StringUtils.substringAfter(m.toString(), "_");

                switch (type.toUpperCase()) {

                    case "BOOTS":

                        peq.setSlotItem(37, toSet);
                        break;

                    case "LEGGINGS":

                        peq.setSlotItem(28, toSet);
                        break;

                    case "CHESTPLATE":

                        peq.setSlotItem(19, toSet);
                        break;

                    case "HELMET":

                        peq.setSlotItem(10, toSet);
                        break;

                }

            } else {
                e.setCancelled(true);
                p.sendMessage(RPG.getPrefix() + "§cYou need to be atleast level §6" + onInteract.getRequiredLevel(toSet) + " §cto use this item!");
            }

        } else {
            ItemStack is = new ItemStack(Material.PAPER, 1);
            ItemMeta ism = is.getItemMeta();
            int slot = e.getType().getSlot();

            switch (slot) {
                case 8:
                    slot = 37;
                    break;

                case 7:
                    slot = 28;
                    break;

                case 6:
                    slot = 19;
                    break;

                case 5:
                    slot = 10;
                    break;
            }
            ism.setDisplayName("§c" + CustomEquipment.slotWithItem.get(slot).replaceAll("_", " ") + " slot");
            ism.setLore(Arrays.asList(" "));
            is.setItemMeta(ism);

            peq.setSlotItem(slot, is);
        }

        RPGPlayer.get(p).setPen(new PlayerEnchantments(RPGPlayer.get(p).getPeq()));
        PlayerCombat.updatePlayerMaxHealth(p);

    }

}

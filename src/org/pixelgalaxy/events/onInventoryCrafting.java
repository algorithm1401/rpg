package org.pixelgalaxy.events;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.pixelgalaxy.utils.refreshTimer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by robin on 9/09/2018.
 */
public class onInventoryCrafting implements Listener {

    private static List<String> lore = Arrays.asList("§7Click this item to open", "§7one of your player menu's");
    private static final List<ItemStack> inventoryShortcuts = Arrays.asList(getEquipmentItem(), getCraftItem(), getSkillsItem());
    public static List<Player> inWorkBench = new ArrayList<>();

    public static List<ItemStack> getInventoryShortcuts() {
        return inventoryShortcuts;
    }

    public static ItemStack getEquipmentItem() {
        ItemStack equipment = new ItemStack(Material.CHEST);
        ItemMeta equipmentm = equipment.getItemMeta();
        equipmentm.setDisplayName("§6Player Equipment");
        equipmentm.setLore(lore);
        equipment.setItemMeta(equipmentm);
        return equipment;
    }

    public static ItemStack getSkillsItem() {
        ItemStack skills = new ItemStack(Material.BOOK, 1);
        ItemMeta skillsm = skills.getItemMeta();
        skillsm.setDisplayName("§9Player Skills");
        skillsm.setLore(lore);
        skills.setItemMeta(skillsm);
        return skills;
    }

    public static ItemStack getCraftItem() {
        ItemStack craft = new ItemStack(Material.WORKBENCH, 1);
        ItemMeta craftm = craft.getItemMeta();
        craftm.setDisplayName("§2Craft");
        craftm.setLore(lore);
        craft.setItemMeta(craftm);
        return craft;
    }

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        if (refreshTimer.getPlayerrefreshTimerMap().containsKey(e.getWhoClicked())) {
            refreshTimer.getPlayerrefreshTimerMap().get(e.getWhoClicked()).cancel();
            refreshTimer.getPlayerrefreshTimerMap().remove(e.getWhoClicked());
        }

        if (!(e.getWhoClicked() instanceof Player))
            return;

        if (e.getClickedInventory() == null)
            return;

        Player p = (Player) e.getWhoClicked();

        if (p.getGameMode() == GameMode.CREATIVE || p.getGameMode() == GameMode.SPECTATOR)
            return;

        Inventory inv = p.getOpenInventory().getTopInventory();

        if (inv == null)
            return;

        if (inv.getType() != InventoryType.CRAFTING)
            return;

        setItems(inv);
    }

    @EventHandler
    public void onInvClose(InventoryCloseEvent e) {

        Player p = (Player) e.getPlayer();

        if (p.getOpenInventory().getTopInventory().getTitle().equalsIgnoreCase("container.crafting") && inWorkBench.contains(p)) {
            inWorkBench.remove(p);

        }

        if (refreshTimer.getPlayerrefreshTimerMap().containsKey(e.getPlayer())) {
            refreshTimer.getPlayerrefreshTimerMap().get(e.getPlayer()).cancel();
            refreshTimer.getPlayerrefreshTimerMap().remove(e.getPlayer());
        }
        if (e.getInventory().getType() != InventoryType.CRAFTING)
            return;

        if (p.getGameMode() == GameMode.CREATIVE || p.getGameMode() == GameMode.SPECTATOR)
            return;

        removeItems(e.getInventory());

    }

    @EventHandler
    public void onInvOpen(InventoryOpenEvent e) {

        if (!e.getInventory().getTitle().equalsIgnoreCase("container.inventory")) {

            if (refreshTimer.getPlayerrefreshTimerMap().containsKey(e.getPlayer())) {
                refreshTimer.getPlayerrefreshTimerMap().get(e.getPlayer()).cancel();
                refreshTimer.getPlayerrefreshTimerMap().remove(e.getPlayer());
            }

        }

    }

    public static void setItems(Inventory inv) {
        if (inv.getItem(1) != null)
            return;

        inv.setItem(1, getSkillsItem());
        inv.setItem(2, getEquipmentItem());
        inv.setItem(3, getCraftItem());
    }

    public static void removeItems(Inventory inv) {
        inv.setItem(0, null);

        inv.setItem(1, null);
        inv.setItem(2, null);
        inv.setItem(3, null);
        inv.setItem(4, null);
    }

}

package org.pixelgalaxy.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.pixelgalaxy.RPG;
import org.pixelgalaxy.RPGPlayer;

/**
 * Created by robin on 3/09/2018.
 */
public class onLeave implements Listener {

    @EventHandler
    public void onLeave(PlayerQuitEvent e) {

        Player p = e.getPlayer();

        p.getInventory().setExtraContents(null);

        RPGPlayer.saveRPGPlayerToDB(p);
        RPG.getPlayerManagement().setBuilding(p.getUniqueId(), false, "null");

    }

}

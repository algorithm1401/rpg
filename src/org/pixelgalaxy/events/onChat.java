package org.pixelgalaxy.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.pixelgalaxy.RPGPlayer;
import org.pixelgalaxy.player_objects.PlayerLevel;

/**
 * Created by robin on 4/09/2018.
 */
public class onChat implements Listener {

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {

        Player p = e.getPlayer();
        PlayerLevel pl = RPGPlayer.get(p).getPl();
        e.setFormat("§a[" + pl.getPlayerLevel() + "]" + e.getFormat());

    }

}

package org.pixelgalaxy.events;

import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.pixelgalaxy.RPG;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by robin on 10/09/2018.
 */
public class onPlayerDeath implements Listener {

    public static Map<Player, Location> dpLocations = new HashMap<>();
    public static Map<Player, Long> playerDeathTime = new HashMap<>();

    @EventHandler

    public void onPlayerDeath(PlayerDeathEvent e) {

        e.getDrops().remove(onInventoryCrafting.getSkillsItem());
        e.getDrops().remove(onInventoryCrafting.getCraftItem());
        e.getDrops().remove(onInventoryCrafting.getEquipmentItem());

        Player p = e.getEntity();

        for (ProtectedRegion r : WGBukkit.getRegionManager(p.getWorld()).getApplicableRegions(p.getLocation())) {
            if (r.getId().equalsIgnoreCase("Warzone")) {

                dpLocations.put(p, p.getLocation());
                playerDeathTime.put(p, System.currentTimeMillis());

            }

        }
    }

}

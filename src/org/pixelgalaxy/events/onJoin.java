package org.pixelgalaxy.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.pixelgalaxy.RPG;
import org.pixelgalaxy.RPGPlayer;
import org.pixelgalaxy.combat.Speed;
import org.pixelgalaxy.combat.Sprinter;
import org.pixelgalaxy.equipment.CustomEquipment;
import org.pixelgalaxy.player_objects.PlayerCombat;
import org.pixelgalaxy.player_objects.PlayerLevel;
import org.pixelgalaxy.tags.Tags;

/**
 * Created by robin on 3/09/2018.
 */
public class onJoin implements Listener {

    @EventHandler(priority = EventPriority.LOWEST)
    public void onJoin(PlayerJoinEvent e) {

        Player p = e.getPlayer();

        // Don't do anything before these two methods
        RPGPlayer.createDefaultStats(p);
        RPGPlayer.loadRPGPlayer(p);

        if (!p.hasPlayedBefore()) {

            RPGPlayer rpgPlayer = RPGPlayer.get(p);
            rpgPlayer.getPs().defaultSkills();
            rpgPlayer.getPl().setLevel(1);
            PlayerLevel.setPlayerXPBar(p);
            CustomEquipment.updatePlayerArmor(p);
        }

        PlayerCombat.updatePlayerMaxHealth(p);
        Sprinter.updateWalkSpeed(p);
        PlayerLevel.setPlayerXPBar(p);

        Speed.startCharge(p.getInventory().getItemInMainHand(), p);
        RPG.getPlayerManagement().setBuilding(p.getUniqueId(), false, "null");
    }

}

package org.pixelgalaxy.events;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.pixelgalaxy.RPGPlayer;
import org.pixelgalaxy.level.XP;
import org.pixelgalaxy.player_objects.PlayerLevel;

/**
 * Created by robin on 19/08/2018.
 */
public class onXPBottleUse implements Listener {

    @EventHandler
    public void onUse(PlayerInteractEvent e){

        Player p = e.getPlayer();
        RPGPlayer rpgPlayer = RPGPlayer.get(p);

        if(e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)){

            ItemStack interacted = e.getItem();

            if (interacted != null) {

                if (interacted.getType().equals(Material.EXP_BOTTLE)) {

                    if (Integer.valueOf(ChatColor.stripColor(interacted.getItemMeta().getDisplayName()).split("\\s+")[0]) != null) {
                        int amount = Integer.valueOf(ChatColor.stripColor(interacted.getItemMeta().getDisplayName()).split("\\s+")[0]);
                        ItemStack bottle = XP.getBottle(amount);
                        bottle.setAmount(interacted.getAmount());
                        if (interacted.equals(bottle)) {

                            e.setCancelled(true);

                            bottle.setAmount(1);
                            p.getInventory().removeItem(bottle);

                            rpgPlayer.getPl().addExperience(amount);
                            PlayerLevel.setPlayerXPBar(p);
                            p.sendMessage("§aYou gained §6§l" + amount + " XP §r§a!");

                        }
                    }
                }
            }
        }

    }

    @EventHandler
    public void removedefaultXP(EntitySpawnEvent e){

        if (e.getEntity().getType().equals(EntityType.THROWN_EXP_BOTTLE) || e.getEntity().getType().equals(EntityType.EXPERIENCE_ORB)) {
            e.getEntity().remove();
        }

    }

}

package org.pixelgalaxy.events;

import net.minecraft.server.v1_12_R1.ItemStack;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemDamageEvent;
import org.pixelgalaxy.equipment.CustomEquipment;
import org.pixelgalaxy.equipment.Equipment;

import java.util.Arrays;
import java.util.List;

/**
 * Created by robin on 17/08/2018.
 */
public class onItemDamage implements Listener {

    @EventHandler
    public void onBreak(PlayerItemDamageEvent e) {

        if (Equipment.armorMaterials.contains(e.getItem().getType()) || Equipment.meleeMaterials.contains(e.getItem().getType()) || Equipment.bowMaterials.contains(e.getItem().getType())) {

            if (Equipment.isArmor(e.getItem()) || Equipment.isMelee(e.getItem()) || Equipment.isBow(e.getItem())) {
                e.setDamage(0);
            }

        }
    }

}

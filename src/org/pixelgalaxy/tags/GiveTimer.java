package org.pixelgalaxy.tags;

import com.nametagedit.plugin.NametagEdit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.pixelgalaxy.RPGPlayer;

/**
 * Created by robin on 23/08/2018.
 */
public class GiveTimer extends BukkitRunnable {

    Player p;

    public GiveTimer(Player p){
        this.p = p;
    }

    @Override
    public void run() {

        NametagEdit.getApi().setPrefix(p, "§a[" + RPGPlayer.get(p).getPl().getPlayerLevel() + "] §7");
        this.cancel();

    }
}

package org.pixelgalaxy.tags;

import com.nametagedit.plugin.NametagEdit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.pixelgalaxy.RPG;
import org.pixelgalaxy.RPGPlayer;

/**
 * Created by robin on 23/08/2018.
 */
public class Tags {

    public static void update(Player p) {
        GiveTimer giveTimer = new GiveTimer(p);
        giveTimer.runTaskLater(RPG.getPlugin(), 20);
    }
}

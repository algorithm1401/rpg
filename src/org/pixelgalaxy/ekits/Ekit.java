package org.pixelgalaxy.ekits;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.pixelgalaxy.RPG;
import org.pixelgalaxy.equipment.Equipment;
import org.pixelgalaxy.equipment.Rarity;
import org.pixelgalaxy.utils.Chance;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by robin on 17/09/2018.
 */
public class Ekit {

    private static final List<String> bootsList = new ArrayList<>();
    private static final List<String> leggingsList = new ArrayList<>();
    private static final List<String> chestList = new ArrayList<>();
    private static final List<String> helmetList = new ArrayList<>();
    private static final List<String> bowList = new ArrayList<>();
    private static final List<String> meleeList = new ArrayList<>();
    private static final List<String> shieldList = new ArrayList<>();

    public static void initializeLists() {

        for (String s : Equipment.getWeaponListStringList()) {

            ItemStack item = new Equipment(s, 1).build();
            Bukkit.getLogger().info(item.toString());
            Rarity itemRarity = Equipment.getItemRarity(item);
            Bukkit.getLogger().info("Item rarity: " + itemRarity.toString());

            if (!itemRarity.equals(Rarity.COMMON)) {

                if (s.contains("boots")) {
                    bootsList.add(s);
                } else if (s.contains("leggings")) {
                    leggingsList.add(s);
                } else if (s.contains("chest")) {
                    chestList.add(s);
                } else if (s.contains("helmet")) {
                    helmetList.add(s);
                } else if (s.contains("bow")) {
                    bowList.add(s);
                } else if (s.contains("shield")) {
                    shieldList.add(s);
                } else {
                    meleeList.add(s);
                }
            }
        }

    }

    public static void giveRandom(Player p, EKitType type) {

        List<String> toGive = new ArrayList<>();

        switch (type) {

            case ARMOR:

                toGive.add(bootsList.get(Chance.getRandom(0, bootsList.size())));
                toGive.add(leggingsList.get(Chance.getRandom(0, leggingsList.size())));
                toGive.add(chestList.get(Chance.getRandom(0, chestList.size())));
                toGive.add(helmetList.get(Chance.getRandom(0, helmetList.size())));
                toGive.add(meleeList.get(Chance.getRandom(0, meleeList.size())));

                break;

            case ARCHER:

                toGive.add(shieldList.get(Chance.getRandom(0, shieldList.size())));
                toGive.add(bowList.get(Chance.getRandom(0, bowList.size())));

                break;

        }


        for (String itemName : toGive) {

            int randomLvl = Chance.getRandom(1, 10);
            if (randomLvl >= 1 && randomLvl <= 3) {
                randomLvl = 1;
            } else if (randomLvl > 3 && randomLvl <= 7) {
                randomLvl = 2;
            } else if (randomLvl > 7 && randomLvl <= 9) {
                randomLvl = 3;
            } else {
                randomLvl = 4;
            }
            p.getInventory().addItem(new Equipment(itemName, Equipment.getItemsOfLevel(randomLvl)).build());

        }
        p.sendMessage(RPG.getPrefix() + "§aYou have succesfully claimed your ekit!");
    }
}

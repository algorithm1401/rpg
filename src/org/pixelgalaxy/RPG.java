package org.pixelgalaxy;/**
 * Created by robin on 29/06/2018.
 */

import com.sk89q.worldguard.LocalPlayer;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.managers.RegionManager;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.pixelgalaxy.armor.ArmorListener;
import org.pixelgalaxy.combat.Absorbtion;
import org.pixelgalaxy.combat.Armor;
import org.pixelgalaxy.combat.Speed;
import org.pixelgalaxy.combat.Sprinter;
import org.pixelgalaxy.combat.bow.BowDamage;
import org.pixelgalaxy.combat.bow.PiercingArrow;
import org.pixelgalaxy.combat.melee.Decimate;
import org.pixelgalaxy.combat.melee.PlayerMeleeHit;
import org.pixelgalaxy.commands.EquipmentCommand;
import org.pixelgalaxy.commands.RPGCommand;
import org.pixelgalaxy.crystals.Opener;
import org.pixelgalaxy.disparity.Disparity;
import org.pixelgalaxy.ekits.Ekit;
import org.pixelgalaxy.enchants.Enchant;
import org.pixelgalaxy.enchants.InventoryList;
import org.pixelgalaxy.enchants.listeners.HitListener;
import org.pixelgalaxy.equipment.CustomEquipment;
import org.pixelgalaxy.equipment.UpgradeListener;
import org.pixelgalaxy.events.*;
import org.pixelgalaxy.insurances.Insurance;
import org.pixelgalaxy.npcs.*;
import org.pixelgalaxy.skills.InventoryInteract;
import org.pixelgalaxy.tags.Tags;
import org.pixelgalaxy.utils.SQL;
import org.pixelgalaxy.utils.inventoryOpenChecker;
import org.pixelgalaxy.utils.schematics.PlayerManagement;
import org.pixelgalaxy.volcano.Volcano;
import org.pixelgalaxy.volcano.shootItems;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class RPG extends JavaPlugin {

    private static RPG plugin;
    private static final String PREFIX = "§8[§6RPG§8] §7";
    private static Economy eco;
    private static PlayerManagement management;

    public static final double KnockBack = 0.3;

    public static SQL SQL;

    @Override
    public void onEnable() {
        plugin = this;

        if (!setupEconomy()) {
            this.getLogger().severe("Disabled due to no Vault dependency found!");
            Bukkit.getPluginManager().disablePlugin(this);
            return;
        }

        createYMLS();

        management = new PlayerManagement();

        Enchant.initializeValues();

        SQL = new SQL();

        // Establish mysql connection and create tables
        SQL.establishConnection();
        SQL.createTables();

        // register command and listeners
        registerCommands(Arrays.asList(

                "rpg",
                "equipment"

        ), Arrays.asList(

                new RPGCommand(),
                new EquipmentCommand()

        ));

        registerListeners(Arrays.asList(

                new InventoryInteract(),
                new Speed(),
                new PlayerMeleeHit(),
                new BowDamage(),
                new Absorbtion(),
                new Decimate(),
                new Sprinter(),
                new PiercingArrow(),
                new ArmorListener(Arrays.asList("")),
                new Armor(),
                new UpgradeListener(),
                new HitListener(),
                new Opener(),
                new CustomEquipment(),
                new onXPBottleUse(),
                new onChat(),
                new onItemDamage(),
                new onLevelUp(),
                new onLeave(),
                new onJoin(),
                new onArmorEquip(),
                new onInventoryCrafting(),
                new Armor(),
                new onDrop(),
                new onPlayerDeath(),
                new NPCClickListener(),
                new Shopkeeper(),
                new InventoryList(),
                new Salvage(),
                new Bounties(),
                new BountyTriggers(),
                new onInteract(),
                new Insurance(),
                new Disparity(),
                new onShieldEquip(),
                new Volcano()

        ));

        inventoryOpenChecker inventoryOpenChecker = new inventoryOpenChecker();
        inventoryOpenChecker.runTaskTimerAsynchronously(RPG.getPlugin(), 10, 0);

        Bounties.startRefreshing();
        Ekit.initializeLists();
        Opener.initMaxValues();
        Disparity.initRewardChances();
        shootItems.initMaxValues();

        new BukkitRunnable() {

            @Override
            public void run() {
                for (Player p : RPGPlayer.all().keySet()) {

                    Tags.update(p);

                }
            }
        }.runTaskTimerAsynchronously(this, 0, 20);

    }

    private void createYMLS() {
        // Create config if not exist
        File configFile = new File(getDataFolder() + "/config.yml");
        if (!configFile.exists()) {
            this.saveDefaultConfig();
        }

        File weaponsFile = new File(getDataFolder() + "/items.yml");
        if (!weaponsFile.exists()) {
            saveResource("items.yml", false);
        }

        File enchantFile = new File(getDataFolder() + "/enchants.yml");
        if (!enchantFile.exists()) {
            saveResource("enchants.yml", false);
        }

        File shopFile = new File(getDataFolder() + "/shops.yml");
        if (!shopFile.exists()) {
            saveResource("shops.yml", false);
        }

        File rewardFile = new File(getDataFolder() + "/rewards.yml");
        if (!rewardFile.exists()) {
            saveResource("rewards.yml", false);
        }

        File bountiesFile = new File(getDataFolder() + "/bounties.yml");
        if (!bountiesFile.exists()) {
            saveResource("bounties.yml", false);
        }

        File cooldownsFile = new File(getDataFolder() + "/cooldowns/");
        if (!cooldownsFile.exists()) {
            cooldownsFile.mkdirs();
        }

        File chestsFile = new File(getDataFolder() + "/chests.yml");
        if (!chestsFile.exists()) {
            saveResource("chests.yml", false);
        }

        File boosterFile = new File(getDataFolder() + "/boosters.yml");
        if (!boosterFile.exists()) {
            saveResource("boosters.yml", false);
        }

    }

    @Override
    public void onDisable() {

        RPGPlayer.saveAllRPGPlayerToDB();

        plugin = null;
    }

    private boolean setupEconomy() {
        if (Bukkit.getPluginManager().getPlugin("Vault") == null) {
            return false;
        }

        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        eco = rsp.getProvider();
        return eco != null;
    }

    public static PlayerManagement getPlayerManagement() {
        return management;
    }

    public static Economy getEco() {
        return eco;
    }

    public static boolean isPvPAllowed(Player p){

        WorldGuardPlugin worldGuard = getWorldGuard();
        LocalPlayer localPlayer = worldGuard.wrapPlayer(p);

        RegionManager regionManager = worldGuard.getRegionManager(p.getWorld());
        ApplicableRegionSet arset = regionManager.getApplicableRegions(p.getLocation());
        boolean canPvp = arset.allows(DefaultFlag.PVP, localPlayer);

        if (!canPvp) {
            p.sendMessage(RPG.getPrefix() + "§cPvP is not allowed in this region!");
        }

        return arset.allows(DefaultFlag.PVP, localPlayer);//returns true if can pvp, else false
    }

    public static WorldGuardPlugin getWorldGuard() {
        Plugin plugin = RPG.getPlugin().getServer().getPluginManager().getPlugin("WorldGuard");

        // WorldGuard may not be loaded
        if (plugin == null || !(plugin instanceof WorldGuardPlugin)) {
            return null; // Maybe you want throw an exception instead
        }

        return (WorldGuardPlugin) plugin;
    }

    public static RPG getPlugin() {
        return plugin;
    }

    public static String getPrefix() {
        return PREFIX;
    }

    public void registerListeners(List<Listener> listeners) {

        listeners.forEach(listener -> plugin.getServer().getPluginManager().registerEvents(listener, plugin));

    }

    public void registerCommands(List<String> commands, List<CommandExecutor> commandExecutors) {

        for (int i = 0; i < commands.size(); i++) {
            Bukkit.getLogger().info(commands.get(i));
            Bukkit.getLogger().info(commandExecutors.get(i).toString());

            String command = commands.get(i);
            CommandExecutor ce = commandExecutors.get(i);

            getCommand(command).setExecutor(ce);
        }

    }

    public static boolean checkValidCommand(Command cmd, String commandString, String permission, Player p) {
        boolean isValid = false;
        if (p.hasPermission(permission)) {
            if (cmd.getName().equalsIgnoreCase(commandString)) {
                isValid = true;
            } else {
                p.sendMessage("§c/" + cmd.getName() + " is not a valid command!");
            }
        } else {
            p.sendMessage("§cYou do not have permission to execute this command!");
        }
        return isValid;
    }

}

package org.pixelgalaxy.player_objects;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.pixelgalaxy.RPG;
import org.pixelgalaxy.RPGPlayer;

/**
 * Created by robin on 3/09/2018.
 */
public class PlayerLevel {

    private int experience;

    public PlayerLevel(int experience) {
        this.experience = experience;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    private static double increment_low = RPG.getPlugin().getConfig().getDouble("experience_algorithm.increment.low");
    private static double increment_mid = RPG.getPlugin().getConfig().getDouble("experience_algorithm.increment.mid");
    private static double increment_high = RPG.getPlugin().getConfig().getDouble("experience_algorithm.increment.high");
    private static final int LOW_LEVEL_THRESHOLD = 15;
    private static final int MID_LEVEL_THRESHOLD = 50;

    public static final int MAX_LEVEL = RPG.getPlugin().getConfig().getInt("max_level");

    public int getPlayerLevel() {

        int level;
        double divided = ((double) getExperience() / 43.0);

        level = (int) Math.round((Math.log10(divided)) / (Math.log10(1 + increment_low)));
        if (level >= LOW_LEVEL_THRESHOLD) {

            level = LOW_LEVEL_THRESHOLD;

            divided = ((double) getExperience() / (double) getXpOfLevel(LOW_LEVEL_THRESHOLD));

            level += (int) Math.round((Math.log10(divided)) / (Math.log10(1 + increment_mid)));

            if (level >= MID_LEVEL_THRESHOLD) {

                level = MID_LEVEL_THRESHOLD;
                divided = ((double) getExperience() / (double) getXpOfLevel(MID_LEVEL_THRESHOLD));

                level += (int) Math.round((Math.log10(divided)) / (Math.log10(1 + increment_high)));

            }

        }

        if (getXpOfLevel(level) > getExperience()) {
            level--;
        }

        return level;
    }

    public void addExperience(int amount) {
        setExperience(getExperience() + amount);
    }

    public int getXpToNextLevel() {

        int xpNeedednext = getXpOfLevel(getPlayerLevel() + 1);

        int xp = xpNeedednext - experience;

        return xp;
    }

    public static int getXpOfLevel(int l) {

        int xpOfLevel = 0;
        if (l <= LOW_LEVEL_THRESHOLD) {
            xpOfLevel = (int) (43 * Math.pow((1 + increment_low), l));

        } else if (l <= MID_LEVEL_THRESHOLD) {

            xpOfLevel += (43 * Math.pow((1 + increment_low), LOW_LEVEL_THRESHOLD));

            xpOfLevel = (int) (xpOfLevel * Math.pow((1 + increment_mid), (l - LOW_LEVEL_THRESHOLD)));

        } else {

            xpOfLevel += (43 * Math.pow((1 + increment_low), LOW_LEVEL_THRESHOLD));

            xpOfLevel += (xpOfLevel * Math.pow((1 + increment_mid), MID_LEVEL_THRESHOLD - LOW_LEVEL_THRESHOLD));

            xpOfLevel = (int) (xpOfLevel * Math.pow((1 + increment_high), l - MID_LEVEL_THRESHOLD));

        }

        return xpOfLevel;
    }

    public void setLevel(int level) {
        setExperience(getXpOfLevel(level));
    }

    public static int getTotalXPForLevel(int level) {

        Integer totalXpCurrent = getXpOfLevel(level);
        Integer totalXpNext = getXpOfLevel(level + 1);

        Integer totalXP = totalXpNext - totalXpCurrent;

        return totalXP;
    }

    public double getXPPercentage() {

        int level = getPlayerLevel();

        double xpPercentage = (double) (getTotalXPForLevel(level) - getXpToNextLevel()) / getTotalXPForLevel(level);

        return xpPercentage;
    }

    public String getXPPercentageBar() {
        StringBuilder bar = new StringBuilder("&a");
        int count = (int) (getXPPercentage() * 100.0);
        int total = 0;

        while (total < 50) {
            if (count == 0) {
                bar.append("&7|");
            } else if (count == 1) {
                bar.append("&2|&7");
            } else {
                bar.append("|");
            }
            count -= 2;
            total++;
        }

        return ChatColor.translateAlternateColorCodes('&', "&8[" + bar.toString() + "&8]");
    }

    public static void setPlayerXPBar(Player p) {

        p.setLevel(0);
        p.setTotalExperience(0);
        p.setExp(0.0F);

        PlayerLevel pl = RPGPlayer.get(p).getPl();

        int level = pl.getPlayerLevel();

        p.setLevel(level);
        p.setExp((float) pl.getXPPercentage());


    }

}

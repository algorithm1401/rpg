package org.pixelgalaxy.player_objects;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.pixelgalaxy.RPG;
import org.pixelgalaxy.RPGPlayer;
import org.pixelgalaxy.npcs.Bounties;

import java.util.List;

/**
 * Created by robin on 14/09/2018.
 */
public class PlayerBounties {

    private List<Integer> bountyIDS;
    private List<Integer> bountyProgress;

    public PlayerBounties(List<Integer> bountyIDS, List<Integer> bountyProgress) {
        this.bountyIDS = bountyIDS;
        this.bountyProgress = bountyProgress;
    }

    public List<Integer> getBountyIDS() {
        return bountyIDS;
    }

    public List<Integer> getBountyProgress() {
        return bountyProgress;
    }

    public void setBountyIDS(List<Integer> bountyIDS) {
        this.bountyIDS = bountyIDS;
    }

    public void removeActiveBounty(Player p, int slot) {

        if (getBountyIDS().size() >= (slot - 17)) {
            List<Integer> newBountyIDS = getBountyIDS();
            newBountyIDS.remove(getBountyIDS().get(slot - 18));
            RPGPlayer.get(p).getPb().setBountyIDS(newBountyIDS);
            p.sendMessage(RPG.getPrefix() + "§cYou have abandoned a bounty!");
            Bounties.updateActiveBounties(p.getOpenInventory().getTopInventory(), p);
            p.closeInventory();
            Bounties.open(p);
        } else {
            p.sendMessage(RPG.getPrefix() + "§cUnable to abandon bounty!");
        }

    }

    public void removeProgress(Player p, int slot) {

        if (getBountyProgress().size() >= (slot - 17)) {
            List<Integer> newProgess = getBountyProgress();
            newProgess.remove(getBountyProgress().get(slot - 18));
            RPGPlayer.get(p).getPb().setBountyProgress(newProgess);
            Bounties.updateActiveBounties(p.getOpenInventory().getTopInventory(), p);
            p.closeInventory();
            Bounties.open(p);
        }

    }

    public boolean addActiveBounty(Player p, int bountyID) {

        if (getBountyIDS().size() <= 9) {
            getBountyIDS().add(bountyID);
            getBountyProgress().add(0);
            return true;
        } else {
            p.sendMessage(RPG.getPrefix() + "§cYou can't add this bounty because you already have 9 bounties active!");
            return false;
        }

    }

    public void setBountyProgress(List<Integer> bountyProgress) {
        this.bountyProgress = bountyProgress;
    }
}

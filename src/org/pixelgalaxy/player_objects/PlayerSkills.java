package org.pixelgalaxy.player_objects;

import org.bukkit.entity.Player;
import org.pixelgalaxy.skills.SkillType;

/**
 * Created by robin on 11/08/2018.
 */
public class PlayerSkills {

    private int strengthLevel;
    private int speedLevel;
    private int critLevel;
    private int bowLevel;
    private int healthLevel;
    private int defenseLevel;

    private int toSpend;

    public PlayerSkills(int strengthLevel, int speedLevel, int critLevel, int bowLevel, int healthLevel, int defenseLevel, int toSpend) {
        this.strengthLevel = strengthLevel;
        this.speedLevel = speedLevel;
        this.critLevel = critLevel;
        this.bowLevel = bowLevel;
        this.healthLevel = healthLevel;
        this.defenseLevel = defenseLevel;
        this.toSpend = toSpend;
    }

    public int getToSpend() {
        return toSpend;
    }

    public void setToSpend(int toSpend) {
        this.toSpend = toSpend;
    }

    public void addStrengthPoint() {
        setStrengthLevel(getStrengthLevel() + 1);
        removeSkillPoint();
    }

    public void addSpeedPoint() {
        setSpeedLevel(getSpeedLevel() + 1);
        removeSkillPoint();
    }

    public void addCritPoint() {
        setCritLevel(getCritLevel() + 1);
        removeSkillPoint();
    }

    public void addBowPoint() {
        setBowLevel(getBowLevel() + 1);
        removeSkillPoint();
    }

    public void addHealthoint(Player p) {
        setHealthLevel(getHealthLevel() + 1);
        removeSkillPoint();
        PlayerCombat.updatePlayerMaxHealth(p);
    }

    public void addDefencePoint() {
        setDefenseLevel(getDefenseLevel() + 1);
        removeSkillPoint();
    }

    private void setStrengthLevel(int strengthLevel) {
        this.strengthLevel = strengthLevel;
    }

    private void setSpeedLevel(int speedLevel) {
        this.speedLevel = speedLevel;
    }

    private void setCritLevel(int critLevel) {
        this.critLevel = critLevel;
    }

    private void setBowLevel(int bowLevel) {
        this.bowLevel = bowLevel;
    }

    private void setHealthLevel(int healthLevel) {
        this.healthLevel = healthLevel;
    }

    private void setDefenseLevel(int defenseLevel) {
        this.defenseLevel = defenseLevel;
    }

    public void defaultSkills() {
        setStrengthLevel(1);
        setSpeedLevel(1);
        setCritLevel(1);
        setBowLevel(1);
        setHealthLevel(1);
        setDefenseLevel(1);
        setToSpend(0);
    }

    public int getStrengthLevel() {
        return strengthLevel;
    }

    public int getSpeedLevel() {
        return speedLevel;
    }

    public int getCritLevel() {
        return critLevel;
    }

    public int getBowLevel() {
        return bowLevel;
    }

    public int getHealthLevel() {
        return healthLevel;
    }

    public int getDefenseLevel() {
        return defenseLevel;
    }

    public int getPlayerSkillLevel(SkillType skillType, Player p) {

        int skillLevel = new Integer(1);

        switch (skillType) {

            case STRENGTH:

                return this.strengthLevel;

            case ATTACK_SPEED:

                return this.speedLevel;

            case CRITICAL_CHANCE:

                return this.critLevel;

            case ARCHERY:

                return this.bowLevel;

            case HEARTS:

                return this.healthLevel;

            case DEFENCE:

                return this.defenseLevel;
        }

        return skillLevel;
    }

    private void removeSkillPoint() {
        setToSpend(getToSpend() - 1);
    }

    public void addSkillPoints(int amount) {

        setToSpend(getToSpend() + amount);

    }

}

package org.pixelgalaxy.player_objects;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.pixelgalaxy.RPGPlayer;
import org.pixelgalaxy.enchants.Enchant;
import org.pixelgalaxy.equipment.Equipment;
import org.pixelgalaxy.player_objects.PlayerEquipment;
import org.pixelgalaxy.utils.EquipmentUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by robin on 2/09/2018.
 */
public class PlayerEnchantments {

    private Map<Enchant, Integer> enchantLevels = new HashMap<>();

    private double protectionReduction = 0;
    private double nutritionMultiplier = 0;
    private double lightweightMultiplier = 0;
    private double reinforcedMultiplier = 0;

    private double kadabraChance = 0;
    private double kadabraIncrease = 0;

    private double igniteChance = 0;
    private double igniteIncrease = 0;

    private double baneChance = 0;

    private double cactusChance = 0;
    private double cactusIncrease = 0;

    private double lastStandChance = 0;
    private double lastStandIncrease = 0;

    private double hardenChance = 0;
    private double hardenIncrease = 0;

    private double snareChance = 0;
    private double snareIncrease = 0;

    private double slowChance = 0;
    private double slowIncrease = 0;

    private double selfDestructChance = 0;
    private double blastOffChance = 0;
    private double escapeMultiplier = 0;

    private double saviorChance = 0;
    private double saviorIncrease = 0;

    private double smokeScreenChance = 0;
    private double smokeScreenIncrease = 0;

    private double tankChance = 0;
    private double tankIncrease = 0;

    private double witheringChance = 0;
    private double witheringIncrease = 0;

    private double absorbingChance = 0;
    private double absorbingIncrease = 0;

    private double saturationChance = 0;
    private double saturationIncrease = 0;

    private double hastenedChance = 0;

    private double ghostlyChance = 0;
    private double ghostlyIncrease = 0;

    private double snowstormChance = 0;
    private double snowstormIncrease = 0;

    public PlayerEnchantments(PlayerEquipment peq) {

        for (ItemStack is : peq.getCombatArmor()) {

            if (is.hasItemMeta() && is.getItemMeta().hasLore()) {

                String weaponPath = Equipment.findWeaponPath(EquipmentUtil.parseDisplayToPath(is));
                int level = EquipmentUtil.getLevelFromWeapon(is);

                Map<Enchant, Integer> enchantFromLoreMap = Equipment.getEnchantLevels(weaponPath, level);

                for (Enchant enchant : enchantFromLoreMap.keySet()) {

                    if (!enchantLevels.containsKey(enchant)) {

                        enchantLevels.put(enchant, enchantFromLoreMap.get(enchant));

                    } else {

                        int levelAlready = enchantLevels.get(enchant);
                        levelAlready += enchantFromLoreMap.get(enchant);
                        enchantLevels.remove(enchant);
                        enchantLevels.put(enchant, levelAlready);

                    }

                }

            }

        }

        for (Enchant enchant : enchantLevels.keySet()) {

            int level = getEnchantLevels().get(enchant);

            switch (enchant) {

                case REINFORCED:

                    reinforcedMultiplier = (0.1 * level);

                    break;

                case PROTECTION:

                    protectionReduction = 0.0125 * level;

                    break;

                case VALOR:

                    protectionReduction = 0.0125 * level;

                    break;


                case ENLIGHTENED:

                    protectionReduction = 0.0125 * level;

                    break;

                case NUTRITION:

                    nutritionMultiplier = 1 + (0.25 * level);

                    break;

                case KADABRA:

                    kadabraChance = (enchant.getChance() * level);
                    kadabraIncrease = (enchant.getIncrease() * level);

                    break;

                case IGNITE:

                    igniteChance = (enchant.getChance() * level);
                    igniteIncrease = (enchant.getIncrease() * level);

                    break;

                case BANE:

                    baneChance = (enchant.getChance() * level);

                    break;

                case CACTUS:

                    cactusChance = (enchant.getChance() * level);
                    cactusIncrease = (enchant.getIncrease() * level);

                    break;

                case LAST_STAND:

                    lastStandChance = (enchant.getChance() * level);
                    lastStandIncrease = (enchant.getIncrease() * level);

                    break;

                case HARDEN:

                    if (level <= 4) {
                        hardenChance = (enchant.getChance() * level);
                    } else {
                        hardenChance = 100;
                    }

                    hardenIncrease = (enchant.getIncrease() * level);

                    break;

                case SNARE_ARMOR:

                    snareChance = (enchant.getChance() * level);
                    snareIncrease = (enchant.getIncrease() * level);

                    break;

                case SLOW_ARMOR:

                    slowChance = (enchant.getChance() * level);
                    slowIncrease = (enchant.getIncrease() * level);

                    break;

                case SELF_DESTRUCT:

                    selfDestructChance = (enchant.getChance() * level);

                    break;

                case BLAST_OFF:

                    blastOffChance = (enchant.getChance() * level);

                    break;

                case ESCAPE:

                    escapeMultiplier = (enchant.getIncrease() * level);

                    break;

                case SAVIOR:

                    saviorChance = (enchant.getChance() * level);
                    saviorIncrease = (enchant.getIncrease() * level);

                    break;

                case SMOKE_SCREEN:

                    smokeScreenChance = (enchant.getChance() * level);
                    smokeScreenIncrease = (enchant.getIncrease() * level);

                    break;

                case TANK:

                    tankChance = (enchant.getChance() * level);
                    tankIncrease = (enchant.getIncrease() * level);

                    break;

                case WITHERING:

                    witheringChance = (enchant.getChance() * level);
                    witheringIncrease = (enchant.getIncrease() * level);

                    break;

                case ABSORBING:

                    absorbingChance = (enchant.getChance() * level);
                    absorbingIncrease = (enchant.getIncrease() * level);

                    break;

                case SATURATION:

                    saturationChance = (enchant.getChance() * level);
                    saturationIncrease = (enchant.getIncrease() * level);

                    break;

                case HASTENED:

                    hastenedChance = (enchant.getChance() * level);

                    break;

                case GHOSTLY:

                    ghostlyChance = (enchant.getChance() * level);
                    ghostlyIncrease = (enchant.getIncrease() * level);

                    break;

                case SNOWSTORM:

                    snowstormChance = (enchant.getChance() * level);
                    snowstormIncrease = (enchant.getIncrease() * level);

                    break;

                case LIGHTWEIGHT:

                    lightweightMultiplier = 1 + (0.125 * level);

                    break;

            }

        }

    }

    public double getReinforcedMultiplier() {
        return reinforcedMultiplier;
    }

    public Map<Enchant, Integer> getEnchantLevels() {
        return enchantLevels;
    }

    public double getProtectionReduction() {
        return protectionReduction;
    }

    public double getNutritionMultiplier() {
        return nutritionMultiplier;
    }

    public double getLightweightMultiplier() {
        return lightweightMultiplier;
    }

    public double getKadabraChance() {
        return kadabraChance;
    }

    public double getIgniteChance() {
        return igniteChance;
    }

    public double getBaneChance() {
        return baneChance;
    }

    public double getCactusChance() {
        return cactusChance;
    }

    public double getLastStandChance() {
        return lastStandChance;
    }

    public double getHardenChance() {
        return hardenChance;
    }

    public double getSnareChance() {
        return snareChance;
    }

    public double getSlowChance() {
        return slowChance;
    }

    public double getSelfDestructChance() {
        return selfDestructChance;
    }

    public double getBlastOffChance() {
        return blastOffChance;
    }

    public double getKadabraIncrease() {
        return kadabraIncrease;
    }

    public double getEscapeMultiplier() {
        return escapeMultiplier;
    }

    public double getSaviorChance() {
        return saviorChance;
    }

    public double getSmokeScreenChance() {
        return smokeScreenChance;
    }

    public double getTankChance() {
        return tankChance;
    }

    public double getWitheringChance() {
        return witheringChance;
    }

    public double getAbsorbingChance() {
        return absorbingChance;
    }

    public double getSaturationChance() {
        return saturationChance;
    }

    public double getHastenedChance() {
        return hastenedChance;
    }

    public double getGhostlyChance() {
        return ghostlyChance;
    }

    public double getSnowstormChance() {
        return snowstormChance;
    }

    public double getIgniteIncrease() {
        return igniteIncrease;
    }

    public double getCactusIncrease() {
        return cactusIncrease;
    }

    public double getLastStandIncrease() {
        return lastStandIncrease;
    }

    public double getHardenIncrease() {
        return hardenIncrease;
    }

    public double getSnareIncrease() {
        return snareIncrease;
    }

    public double getSlowIncrease() {
        return slowIncrease;
    }

    public double getSaviorIncrease() {
        return saviorIncrease;
    }

    public double getSmokeScreenIncrease() {
        return smokeScreenIncrease;
    }

    public double getTankIncrease() {
        return tankIncrease;
    }

    public double getWitheringIncrease() {
        return witheringIncrease;
    }

    public double getAbsorbingIncrease() {
        return absorbingIncrease;
    }

    public double getSaturationIncrease() {
        return saturationIncrease;
    }

    public double getGhostlyIncrease() {
        return ghostlyIncrease;
    }

    public double getSnowstormIncrease() {
        return snowstormIncrease;
    }
}

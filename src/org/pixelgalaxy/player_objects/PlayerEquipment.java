package org.pixelgalaxy.player_objects;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.pixelgalaxy.equipment.Equipment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by robin on 27/08/2018.
 */
public class PlayerEquipment {

    private ItemStack combat_boots;
    private ItemStack combat_leggings;
    private ItemStack combat_chestplate;
    private ItemStack combat_helmet;

    private ItemStack earring1;
    private ItemStack necklace;
    private ItemStack earring2;

    private ItemStack ring1;
    private ItemStack ring2;
    private ItemStack scroll;

    private ItemStack cosmetic_boots;
    private ItemStack cosmetic_leggings;
    private ItemStack cosmetic_chestplate;
    private ItemStack cosmetic_helmet;

    private ItemStack shield;

    public PlayerEquipment(ItemStack combat_boots, ItemStack combat_leggings, ItemStack combat_chestplate, ItemStack combat_helmet, ItemStack earring1, ItemStack necklace, ItemStack earring2, ItemStack ring1, ItemStack ring2, ItemStack scroll, ItemStack cosmetic_boots, ItemStack cosmetic_leggings, ItemStack cosmetic_chestplate, ItemStack cosmetic_helmet, ItemStack shield) {
        this.combat_boots = combat_boots;
        this.combat_leggings = combat_leggings;
        this.combat_chestplate = combat_chestplate;
        this.combat_helmet = combat_helmet;
        this.earring1 = earring1;
        this.necklace = necklace;
        this.earring2 = earring2;
        this.ring1 = ring1;
        this.ring2 = ring2;
        this.scroll = scroll;
        this.cosmetic_boots = cosmetic_boots;
        this.cosmetic_leggings = cosmetic_leggings;
        this.cosmetic_chestplate = cosmetic_chestplate;
        this.cosmetic_helmet = cosmetic_helmet;
        this.shield = shield;
    }

    public List<ItemStack> getCombatArmor() {

        List<ItemStack> combatWearing = new ArrayList<>();

        if (!getCombat_boots().getType().equals(Material.PAPER)) {

            combatWearing.add(getCombat_boots());

        }
        if (!getCombat_leggings().getType().equals(Material.PAPER)) {

            combatWearing.add(getCombat_leggings());

        }

        if (!getCombat_chestplate().getType().equals(Material.PAPER)) {

            combatWearing.add(getCombat_chestplate());

        }

        if (!getCombat_helmet().getType().equals(Material.PAPER)) {

            combatWearing.add(getCombat_helmet());

        }

        if (shield != null) {
            combatWearing.add(shield);
        }

        return combatWearing;
    }

    public void setSlotItem(int slot, ItemStack is) {

        switch (slot) {

            case 10:
                setCombat_helmet(is);
                break;

            case 19:
                setCombat_chestplate(is);
                break;

            case 28:
                setCombat_leggings(is);
                break;

            case 37:
                setCombat_boots(is);
                break;

            case 12:
                setEarring1(is);
                break;

            case 13:
                setNecklace(is);
                break;

            case 14:
                setEarring2(is);
                break;

            case 16:
                setCosmetic_helmet(is);
                break;

            case 25:
                setCosmetic_chestplate(is);
                break;

            case 30:
                setRing1(is);
                break;

            case 31:
                setScroll(is);
                break;

            case 32:
                setRing2(is);
                break;

            case 34:
                setCosmetic_leggings(is);
                break;

            case 43:
                setCosmetic_boots(is);
                break;

        }

    }

    public void setShield(ItemStack shield) {
        this.shield = shield;
    }

    public ItemStack getShield() {
        return this.shield;
    }

    public void setCombat_boots(ItemStack combat_boots) {
        this.combat_boots = combat_boots;
    }

    public void setCombat_leggings(ItemStack combat_leggings) {
        this.combat_leggings = combat_leggings;
    }

    public void setCombat_chestplate(ItemStack combat_chestplate) {
        this.combat_chestplate = combat_chestplate;
    }

    public void setCombat_helmet(ItemStack combat_helmet) {
        this.combat_helmet = combat_helmet;
    }

    public void setEarring1(ItemStack earring1) {
        this.earring1 = earring1;
    }

    public void setNecklace(ItemStack necklace) {
        this.necklace = necklace;
    }

    public void setEarring2(ItemStack earring2) {
        this.earring2 = earring2;
    }

    public void setRing1(ItemStack ring1) {
        this.ring1 = ring1;
    }

    public void setRing2(ItemStack ring2) {
        this.ring2 = ring2;
    }

    public void setScroll(ItemStack scroll) {
        this.scroll = scroll;
    }

    public void setCosmetic_boots(ItemStack cosmetic_boots) {
        this.cosmetic_boots = cosmetic_boots;
    }

    public void setCosmetic_leggings(ItemStack cosmetic_leggings) {
        this.cosmetic_leggings = cosmetic_leggings;
    }

    public void setCosmetic_chestplate(ItemStack cosmetic_chestplate) {
        this.cosmetic_chestplate = cosmetic_chestplate;
    }

    public void setCosmetic_helmet(ItemStack cosmetic_helmet) {
        this.cosmetic_helmet = cosmetic_helmet;
    }

    public ItemStack getCombat_boots() {
        return combat_boots;
    }

    public ItemStack getCombat_leggings() {
        return combat_leggings;
    }

    public ItemStack getCombat_chestplate() {
        return combat_chestplate;
    }

    public ItemStack getCombat_helmet() {
        return combat_helmet;
    }

    public ItemStack getEarring1() {
        return earring1;
    }

    public ItemStack getNecklace() {
        return necklace;
    }

    public ItemStack getEarring2() {
        return earring2;
    }

    public ItemStack getRing1() {
        return ring1;
    }

    public ItemStack getRing2() {
        return ring2;
    }

    public ItemStack getScroll() {
        return scroll;
    }

    public ItemStack getCosmetic_boots() {
        return cosmetic_boots;
    }

    public ItemStack getCosmetic_leggings() {
        return cosmetic_leggings;
    }

    public ItemStack getCosmetic_chestplate() {
        return cosmetic_chestplate;
    }

    public ItemStack getCosmetic_helmet() {
        return cosmetic_helmet;
    }
}

package org.pixelgalaxy.player_objects;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;
import org.pixelgalaxy.RPG;
import org.pixelgalaxy.RPGPlayer;
import org.pixelgalaxy.abilities.Ability;
import org.pixelgalaxy.abilities.AbilityType;
import org.pixelgalaxy.combat.ChargeDelayDisplayer;
import org.pixelgalaxy.combat.Speed;
import org.pixelgalaxy.combat.bow.BowDamage;
import org.pixelgalaxy.skills.SkillType;

/**
 * Created by robin on 3/09/2018.
 */
public class PlayerCombat {

    private boolean isDecimating;
    private boolean isPiercing;
    private Long decimateCooldown;
    private Long piercingCooldown;

    public PlayerCombat(boolean isDecimating, boolean isPiercing) {
        this.isDecimating = isDecimating;
        this.isPiercing = isPiercing;
        this.decimateCooldown = System.currentTimeMillis() - 20000;
        this.piercingCooldown = System.currentTimeMillis() - 20000;
    }

    public static double getTotalBaseHealth(Player p) {
        double health = 20.0;
        PlayerSkills ps = RPGPlayer.get(p).getPs();

        health += (health * ((double) getSkillPercentage(SkillType.HEARTS, ps.getHealthLevel()) / 100));

        return health;
    }

    public static void updatePlayerMaxHealth(Player p) {
        p.setMaxHealth(getTotalBaseHealth(p) * (1 + RPGPlayer.get(p).getPen().getReinforcedMultiplier()));
    }

    public static double getTotalArmorReduction(Player p) {

        double reduction = 0.0;

        int defensePercentage = getSkillPercentage(SkillType.DEFENCE, RPGPlayer.get(p).getPs().getDefenseLevel());
        int maxDefensePercentage = getSkillPercentage(SkillType.DEFENCE, SkillType.DEFENCE.getMax_level());

        // Apply reduction from skills
        reduction += (0.3 * ((double) defensePercentage / (double) maxDefensePercentage));

        return reduction;
    }

    public static double getTotalRawMeleeDMG(Player p, ItemStack is, LivingEntity target) {

        PlayerSkills ps = RPGPlayer.get(p).getPs();
        PlayerCombat pc = RPGPlayer.get(p).getPc();

        // Get base damage from item
        double damage = getBaseDMGFromItem(is);

        // Apply extra damage for strength skills
        double strengthSkillModifier = 1 + ((getSkillPercentage(SkillType.STRENGTH, ps.getStrengthLevel()) / 100));
        damage *= strengthSkillModifier;

        // Apply extra damage for decimate
        if (pc.isDecimating()) {

            damage *= 1.5;
            pc.setDecimating(false);
            pc.setDecimateCooldown(System.currentTimeMillis());
            if (target instanceof Player) {
                Player targetP = (Player) target;
                p.sendMessage(AbilityType.DECIMATE.getMessageOnUse() + "§e" + targetP.getName());
            } else {

                p.sendMessage(AbilityType.DECIMATE.getMessageOnUse() + "§e" + target.getType().toString().toLowerCase());

            }
        }

        // Change the damage based on charge
        damage *= Speed.getPercentCharged(p);

        return damage;
    }

    public static double getTotalRawBowDMG(Player p, ItemStack is) {

        PlayerSkills ps = RPGPlayer.get(p).getPs();
        PlayerCombat pc = RPGPlayer.get(p).getPc();

        // Get base damage from item
        double damage = getBaseDMGFromItem(is);

        // Apply damage from bow skill
        double bowSkillModifier = 1 + (getSkillPercentage(SkillType.ARCHERY, ps.getBowLevel()) / 100);
        damage *= bowSkillModifier;

        // Apply piercing damage
        if (pc.isPiercing()) {

            damage *= 2.0;
            pc.setPiercing(false);
            pc.setPiercingCooldown(System.currentTimeMillis());
            p.sendMessage(AbilityType.PIERCING_ARROW.getMessageOnUse());
        }

        damage *= BowDamage.getDrawedPercent(p);

        return damage;
    }

    public static void applyKnockBack(Player hitter, LivingEntity target, double percent) {
        Vector knockbackVector = hitter.getLocation().getDirection().multiply(RPG.KnockBack * 1.5).setY(RPG.KnockBack + 0.05);
        target.setVelocity(knockbackVector.multiply(percent));
    }

    public void setDecimating(boolean decimating) {
        isDecimating = decimating;
    }

    public void setPiercing(boolean piercing) {
        isPiercing = piercing;
    }

    public Long getDecimateCooldown() {
        return decimateCooldown;
    }

    public Long getPiercingCooldown() {
        return piercingCooldown;
    }

    public void setDecimateCooldown(Long decimateCooldown) {
        this.decimateCooldown = decimateCooldown;
    }

    public void setPiercingCooldown(Long piercingCooldown) {
        this.piercingCooldown = piercingCooldown;
    }

    public boolean isPiercing() {
        return this.isPiercing;
    }

    public boolean isDecimating() {
        return this.isDecimating;
    }

    public boolean hasDecimateCooldown() {

        if ((System.currentTimeMillis() - getDecimateCooldown()) > 20000) {
            return false;
        }
        return true;
    }

    private static double getBaseDMGFromItem(ItemStack is) {

        double damage = 0.0;

        if (is.getItemMeta().getLore() != null) {

            for (String s : is.getItemMeta().getLore()) {

                s = ChatColor.stripColor(s);
                if (s.contains("Attack Damage:")) {

                    s = s.replaceAll(",", ".");

                    if (Double.valueOf(s.split("\\s+")[2]) != null) {
                        damage = Double.valueOf(s.split("\\s+")[2]);
                    }

                }

            }

        }
        return damage;
    }

    public static int getSkillPercentage(SkillType type, int skillLevel) {

        switch (type) {

            case STRENGTH:

                return (skillLevel * 5 - 5);

            case ATTACK_SPEED:

                return (skillLevel * 3 - 3);

            case CRITICAL_CHANCE:

                return (skillLevel * 1 - 1);

            case ARCHERY:

                return (skillLevel * 5 - 5);

            case HEARTS:

                return (skillLevel * 8 - 8);

            case DEFENCE:

                return (skillLevel * 18 - 18);

        }

        return -1;
    }

}
